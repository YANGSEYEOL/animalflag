﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HPBar : MonoBehaviour
{
    public UISprite[] hpMarks;

    // 스킬사용이나 상태를 나타내는 아이콘(동물들의 머리위에뜸)
    public List<UISprite> statusIconList;
    readonly int ICON_SIZE = 30;
    int statusCount = 0; // 상태아이콘 개수
    int statusIndex = 0;

    public UISprite rack1;
    public UISprite rack2;
    public UIManager uiManager;
    public UIFollowTarget uiFollowTarget;

    private int hpMarkCount = 0;
    private int curHealth = 0;
    private int anchorOffsetHorizontal = 3;
    private int anchorOffsetVertical = 3;
    readonly private int markWidth = 25;
    readonly private int markHeight = 25;

    private int rackOffset = 0;
    private int rack1Width = 30;
    private int rack2Width;
    private int rack1Height = 40;
    private int rack2Height = 8;

    Animal ownAnimal;
    readonly int hpClusterNum = 5;
    
    public void Init(Animal animal)
    {
        statusIconList = new List<UISprite>();

        uiManager = UIManager.instance;
        ownAnimal = animal;
        hpMarkCount = ownAnimal.curHealth;
        curHealth = hpMarkCount;
        hpMarks = new UISprite[hpMarkCount];

        uiFollowTarget = GetComponent<UIFollowTarget>();
        uiFollowTarget.target = ownAnimal.UIPositionTransform.transform;
        uiFollowTarget.gameCamera = Camera.main;
        uiFollowTarget.uiCamera = uiManager._uiCamera;

        // hp바의 rack.
        rack1 = gameObject.AddChild<UISprite>();
        rack2 = gameObject.AddChild<UISprite>();
        rack1.pivot = UIWidget.Pivot.BottomLeft;
        rack1.transform.position = Vector3.zero;

        rack1.atlas = uiManager.atlasForHpBar.GetComponent<UIAtlas>();
        rack2.atlas = uiManager.atlasForHpBar.GetComponent<UIAtlas>();
        if (hpMarkCount < 5)
        {
            rack2Width = (markWidth + anchorOffsetVertical) * (hpMarkCount % hpClusterNum) + rackOffset;
        }
        else
        {
            rack2Width = (markWidth + anchorOffsetVertical) * 5 + rackOffset;
        }  

        rack1.SetDimensions(rack1Width, rack1Height);

        rack1.alpha = 0.8f;
        rack2.alpha = 0.8f;

        if (ownAnimal.ownerUserId == PhotonNetwork.player.UserId)
        {
            rack1.spriteName = "rack_ally1";
            rack2.spriteName = "rack_ally2";
        }
        else
        {
            rack1.spriteName = "rack_enemy1";
            rack2.spriteName = "rack_enemy2";
        }

        rack2.SetAnchor(rack1.gameObject.transform);
        rack2.leftAnchor.absolute = rack1Width - 1;
        rack2.rightAnchor.absolute = rack2Width;
        rack2.topAnchor.target = null;
        rack2.bottomAnchor.absolute = rack1Height - rack2Height;

        // hp마크들
        for (int i = 0; i < hpMarkCount; i++)
        {
            hpMarks[i] = gameObject.AddChild<UISprite>();
            hpMarks[i].atlas = uiManager.atlasForHpBar.GetComponent<UIAtlas>();           

            if (ownAnimal.ownerUserId == PhotonNetwork.player.UserId)
            {
                hpMarks[i].spriteName = "hpMark_heart_ally";
            }
            else
            {
                hpMarks[i].spriteName = "hpMark_heart_enemy";
            }

            hpMarks[i].SetDimensions(markWidth, markHeight);
            //hpMarks[i].alpha = 0.8f;
            if (i == 0)
            {
                hpMarks[i].SetAnchor(rack2.gameObject.transform);
                hpMarks[i].rightAnchor.absolute = -rack2Width + rackOffset + markWidth;
                hpMarks[i].bottomAnchor.absolute = rack2Height;
                hpMarks[i].topAnchor.absolute = markHeight;
                continue;
            }

            if (i % hpClusterNum != 0)
            {
                hpMarks[i].SetAnchor(hpMarks[i - 1].gameObject.transform);
                hpMarks[i].leftAnchor.absolute = anchorOffsetHorizontal + markWidth;
                hpMarks[i].rightAnchor.absolute = anchorOffsetHorizontal + markWidth;
            }
            else
            {
                hpMarks[i].SetAnchor(hpMarks[i - hpClusterNum].gameObject.transform);
                hpMarks[i].topAnchor.absolute = anchorOffsetVertical + markWidth;
                hpMarks[i].bottomAnchor.absolute = anchorOffsetVertical + markHeight;
            }            
        }
        if(ownAnimal.ownedTile!=null)
            SetActive(ownAnimal.ownedTile.isVisible);
    }

    // 누구 순서인지에 따라서 hp바 색깔변경
    public void ChangeHPBarColor()
    {
        if (ownAnimal.ownerUserId == PhotonNetwork.player.UserId)
        {
            rack1.spriteName = "rack_ally1";
            rack2.spriteName = "rack_ally2";
        }
        else
        {
            rack1.spriteName = "rack_enemy1";
            rack2.spriteName = "rack_enemy2";
        }

        for (int i = 0; i < hpMarkCount; i++)
        {
            if (ownAnimal.ownerUserId == PhotonNetwork.player.UserId)
            {
                hpMarks[i].spriteName = "hpMark_heart_ally";
            }
            else
            {
                hpMarks[i].spriteName = "hpMark_heart_enemy";
            }

            hpMarks[i].SetDimensions(markWidth, markHeight);
            //hpMarks[i].alpha = 0.8f;
            if (i == 0)
            {
                hpMarks[i].SetAnchor(rack2.gameObject.transform);
                hpMarks[i].rightAnchor.absolute = -rack2Width + rackOffset + markWidth;
                hpMarks[i].bottomAnchor.absolute = rack2Height;
                hpMarks[i].topAnchor.absolute = markHeight;
                continue;
            }

            if (i % hpClusterNum != 0)
            {
                hpMarks[i].SetAnchor(hpMarks[i - 1].gameObject.transform);
                hpMarks[i].leftAnchor.absolute = anchorOffsetHorizontal + markWidth;
                hpMarks[i].rightAnchor.absolute = anchorOffsetHorizontal + markWidth;
            }
            else
            {
                hpMarks[i].SetAnchor(hpMarks[i - hpClusterNum].gameObject.transform);
                hpMarks[i].topAnchor.absolute = anchorOffsetVertical + markWidth;
                hpMarks[i].bottomAnchor.absolute = anchorOffsetVertical + markHeight;
            }
        }
    }

    public void SetActive(bool visible)
    {
        gameObject.SetActive(visible);
    }

    public void GainCurHp(int amount)
    {
        for (int i = 1; i <= amount; i++)
        {
            if (curHealth + i - 1 < hpMarkCount)
            {
                hpMarks[i + curHealth - 1].spriteName = "hpMark_heart_ally";
            }
        }

        curHealth += amount;

        SetActive(curHealth > 0);
    }

    public IEnumerator ReduceCurHp(int amount, float heartDisappearInterval)
    {
        SetActive(true);
        yield return new WaitForSeconds(heartDisappearInterval);
        for (int i = 1; i <= amount; i++)
        {
            if(curHealth - i >= 0)
            {
                // 기존에 하트였다면 빈 하트로 바꾼다음에 잠깐 쉬기
                if (hpMarks[curHealth - i].spriteName == "hpMark_heart_ally")
                {
                    hpMarks[curHealth - i].spriteName = "hpMark_heart_empty_ally";
                }
                else
                {
                    hpMarks[curHealth - i].spriteName = "hpMark_heart_empty_enemy";
                }                   
            }            
        }
        //ownAnimal.audioSource.PlayOneShot(GameManager.instance.hpReducingSound);

        curHealth -= amount;
        yield return null;
    }

    public void DestroyHpMarkAnimaltion()
    {

    }

    public void CreateHpMarkAnimaltion()
    {

    }

    public void AddStatus(string status)
    {
        // 기존에 같은 이름이 있었다면 새로운 스프라이트를 추가하지않고 리턴한다.
        for (int i = 0; i < statusIconList.Count; i++)
        {
            if (statusIconList[i].spriteName == status)
            {
                return;
            }
        }

        // 새로운 아아이콘을 만들어서 리스트에추가한다.
        UISprite sprite = null;
        /// statusCount 는 현재 hp바에 뜬 상태아이콘개수
        /// statusIcon은 아이콘풀에 저장된 아이콘의 개수
        /// 아이콘풀이 꽉찼으면 아이콘을 새로만든다.
        if (statusIconList.Count == statusCount)
        {
            sprite = gameObject.AddChild<UISprite>();
            statusIconList.Add(sprite);
            sprite.atlas = UIManager.instance.atlasForHpBar.GetComponent<UIAtlas>();
        }
        /// 아니면 기존에있던 아이콘을 재사용한다.
        else if(statusIconList.Count > statusCount)
        {
            sprite = statusIconList[statusCount];
        }

        // 아이콘 개수를 하나 증가시켜줌
        statusCount++;
        sprite.gameObject.SetActive(true);
        sprite.spriteName = status;
        
        
        // 상태아이콘 위치조정, 첫번째일때는 rack2에 맞춰 anchor를 설정함
        if (statusCount == 1)
        {
            sprite.SetAnchor(rack2.gameObject.transform);
            sprite.topAnchor.absolute = -rack2Height - anchorOffsetVertical;
            sprite.bottomAnchor.absolute = -ICON_SIZE - anchorOffsetVertical;
            sprite.leftAnchor.absolute = 0;
            sprite.rightAnchor.absolute = -rack2Width + ICON_SIZE;
        }
        // 두번째아이콘부터 바로 전 아이콘을 앵커로써 정렬한다.
        else
        {

        }
    }

    ///  
    
    public void RemoveStatus(string status)
    {
        // 기존에 같은 이름이 있었다면 새로운 스프라이트를 추가하지않고 리턴한다.
        UISprite spriteToRemove = null;
        for (int i = 0; i < statusIconList.Count; i++)
        {
            if (statusIconList[i].spriteName == status)
            {
                spriteToRemove = statusIconList[i];
                break;
            }
        }
        if (spriteToRemove != null)
        {
            statusCount--;
            spriteToRemove.gameObject.SetActive(false);
            spriteToRemove.spriteName = null;
        }
    }
}
