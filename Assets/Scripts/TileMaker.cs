﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Photon;
using Hashtable = ExitGames.Client.Photon.Hashtable;

// 알고리즘 개요
#region
// <알고리즘의 목적>
// 프로토타입에서는 20 * 20의 맵에 타일들을 그럴듯하게 배치하는 알고리즘이다. 현재 구상한 타일은 아래 5가지이다. 이 타일들이 나름의 원칙대로
// 모여있어야 한다. 예를 들어 사막 지역은 5*5정도의 규모로 몰려있어야하고 그 지형의 특색을 나타낼 수 있어야한다. 사막과 초원이 규칙적으로 섞여있다면
// 어색할 것이다. 또 예를들어 강은 나름대로 이어져 흐름이 생성되어야한다. 
// <알고리즘 컨셉>
// 대충 5*5 정도의 크기의 클러스터를 만든다. 이 클러스터는 사막타일, 숲타일, 평지타일 등등으로 나름의 테마를 가지고있다. 
// 이 클러스터는 5*5 직사각형의 크기가 아니고 둥그스름하게 자연스러운 형태를 유지한다. 이때 발생하는 문제가 클러스터와 클러스터 사이에 발생하는
// 어색한 빈칸인데 이것은 이 맵의 basictile로 선정된 타일이 채워넣게 되어 경계를 나름 깔끔하게 채우도록한다.
// 강 같은 경우는 그 흐름을 유지하기 위해 일자 혹은 zigzag의 형태를 유지하고 어떤 클러스터 중심을 가로지른다.
// 모든 배치는 정확하지않고 약간의 오차를 두어 맵의 배치가 자연스럽도록 연출한다.
#endregion


public class TileMaker : PunBehaviour
{
    ///  enum 순서에 의존하는 코드가 있으니 수정하지 말 것 
    public enum TileType : int { NA_TILE = 0, FLAT, GRASS, RIVER, DESSERT, FOREST};

    public static TileMaker instance;

    [System.NonSerialized]
    public int[] CreatedTileTypeArray = new int[6];

    [System.NonSerialized]
    int[] directionX = new int[8] { -1,0,1,-1,1,-1,0,1};
    [System.NonSerialized]
    int[] directionY = new int[8] { 1,1,1,0,0,-1,-1,-1};

    bool desertExsting = false;

    struct tileStr
    {
        public int cost;
        public int CoreX;
        public int CoreY;
        public int probability;
    };

    [SerializeField]
    TileType basicTile;   // basicTile이란 맵의 클러스터에 포함되지않은 공간에 채워넣을 배경이 되는 타일이다.
    [System.NonSerialized]
    public TileType[,] tilesArray;

    [SerializeField]
    public int tileScale = 50;
    [SerializeField]
    public int tileSize = 40;

    [SerializeField]
    int clusterNum = 40; // 게임에 이용할 클러스터의 개수이다.

    [SerializeField]
    int clusterSize = 5; // 클러스터의 대략적인 대각선 크기이다.

    [SerializeField]
    int[] tileTypeChances;  // 타일 타입들의 결정되는 확률을 담는 변수

    [SerializeField]
    int creatingRockChance; // 바위가 생성될 확률

    public static readonly string TILES_PROP_KEY = "Tiles";
    public static readonly string TILES_SIZE_WIDTH_PROP_KEY = "TilesSizeWidth";
    public static readonly string TILES_SIZE_HEIGHT_PROP_KEY = "TilesSizeHeight";

    // 타일의 설명
    #region
    // 평지 : FLAT = 0;  아무런 타입이 없는 타일이다.
    // 초원 : GRASS = 1; 풀이 있어 이동에 제약이없고 아마도 대부분의 동물들이 좋아하는 타일이다.
    // 강 : RIVER = 2, 하마 물소가 좋아하는 타일이다. 아마도 이동의 제약조건이있다. 또한 맵에서 차지하는 비중이 비교적 작고 강은 그럴듯하게 일자로 이어지거나 꺾여야한다.
    // 사막 : DESSERT = 3 이동에 제약이 심한 타일이다. 그러나 단봉낙타 등 사막을 좋아하는 유닛이 있다. 사막타일은 서로 뭉쳐있을 확률이 높다. 사막타일중간에 초원이나 평지가 있을 확률은 극히 드물다. 
    //또한 사막에 강이 이어질 수 있다. 사막은 다른 타일과는 경계가 심각하게 존재하여 서로 공존하기 힘들다. 사막지형이 따로 길게 있거나 동그랗게 있다.   
    // 숲 : FORREST = 4 원숭이, 고릴라 등이 좋아하는 타일이다. 이동에 제약이 있고 비교적 소규모로 뭉쳐 존재한다.  


    // Use this for initialization
    #endregion

    private void Awake()
    {
        instance = this;
    }

    void Start ()
    {
        tilesArray = new TileType[tileSize, tileSize];
        Init();
        AddClustersOnMap();
    }

    // 타일 어레이를 -1로 초기화한다. NA_TILE(-1)은 아직 타일이 할당되지 않음을 의미한다.
    void Init()
    {
        for (int i = 0; i < tileSize; i++)
        {
            for(int j = 0; j < tileSize; j++)
            {
                tilesArray[i, j] = basicTile;
            }
        }

        for (int i = 0; i < 6; i++)
        {
            CreatedTileTypeArray[i] = 0;
        }
        CreatedTileTypeArray[(int)basicTile] = 1;
    }

    bool CheckDuplicateTile(TileType tile)
    {
        if (CreatedTileTypeArray[(int)tile] == 2)
        {
          //  Debug.Log(tile + " has come out! Do it again.");
            return true;
        }

      //  Debug.Log(tile + " has come out! lego.");
        return false;
    }

    //클러스터의 중심부의 위치를 결정하고 그 위치에 랜덤하게 타일을 할당해주어 해당 타일의 클러스터를 생성한다.
    void AddClustersOnMap()
    {
        for (int i = 0; i < clusterNum; i++)
        {
            int a = Random.Range(0, tileSize);
            int b = Random.Range(0, tileSize);

            CreateCluster(a, b);
        }
    }

    void CreateCluster(int a, int b)
    {
        TileType ChosenTile;
        
        ChosenTile = RollForDecisionTileType();
        CreatedTileTypeArray[(int)ChosenTile]++;

   
        Stack<tileStr> stack = new Stack<tileStr>();
        bool[,] visited = new bool[clusterSize * 2, clusterSize * 2];
        int[,] probChecker = new int[clusterSize * 2, clusterSize * 2];
        visited[clusterSize, clusterSize] = true;
        tileStr startTileStr;
        startTileStr.cost = 0;
        startTileStr.CoreX = a;
        startTileStr.CoreY = b;
        startTileStr.probability = 95;

        stack.Push(startTileStr);

        int offsetX = clusterSize - a;
        int offsetY = clusterSize - b;

        while(stack.Count != 0)
        {
            tileStr Pop_TileStr = stack.Pop();                                          // 스택에서 타일 하나를 꺼낸다. 
            visited[Pop_TileStr.CoreX + offsetX, Pop_TileStr.CoreY + offsetY] = true;   // 이 타일은 이제 탐색을 했다.
            int ModifiedProbability = 0;
            int diceRes = Random.Range(0, 100);
            if (Pop_TileStr.probability >= diceRes)
            {
                tilesArray[Pop_TileStr.CoreX, Pop_TileStr.CoreY] = ChosenTile;
                ModifiedProbability = Pop_TileStr.probability - 5;
                if (ModifiedProbability <= 0) ModifiedProbability = 0;           
            }
            else
            {
                tilesArray[Pop_TileStr.CoreX, Pop_TileStr.CoreY] = basicTile;
                ModifiedProbability = Pop_TileStr.probability - 30;
                if (ModifiedProbability <= 0) ModifiedProbability = 0;
            }

            for (int i = 0; i < 8; i++)
            {
                // 아직 옆 근처 타일을 방문하지 않았다면 이 타일을 스택에 집어넣고 visit을 true로 체크해준다.
                if (Pop_TileStr.CoreX + directionX[i] >= 0 && Pop_TileStr.CoreX + directionX[i] < tileSize &&
                    Pop_TileStr.CoreY + directionY[i] >= 0 && Pop_TileStr.CoreY + directionY[i] < tileSize)
                {
                    if (Pop_TileStr.CoreX + directionX[i] + offsetX >= 0 && Pop_TileStr.CoreX + directionX[i] + offsetX < clusterSize * 2 &&
                        Pop_TileStr.CoreY + directionY[i] + offsetY >= 0 && Pop_TileStr.CoreY + directionY[i] + offsetY < clusterSize * 2)
                    {
                        if (!visited[Pop_TileStr.CoreX + directionX[i] + offsetX, Pop_TileStr.CoreY + directionY[i] + offsetY])
                        {
                            tileStr tempTileStr;
                            tempTileStr.cost = 0;
                            tempTileStr.CoreX = Pop_TileStr.CoreX + directionX[i];
                            tempTileStr.CoreY = Pop_TileStr.CoreY + directionY[i];
                            tempTileStr.probability = ModifiedProbability;
                            stack.Push(tempTileStr);
                            visited[Pop_TileStr.CoreX + directionX[i] + offsetX, Pop_TileStr.CoreY + directionY[i] + offsetY] = true;
                            probChecker[Pop_TileStr.CoreX + directionX[i] + offsetX, Pop_TileStr.CoreY + directionY[i] + offsetY] = ModifiedProbability;
                        }
                    }
                }
            }
        }

        return;
    }
    
    bool CheckDistanceOfCluster(ref int a, ref int b)
    {
        return false;
    }


    public void GenerateTileObjects()
    {
        Hashtable tilesProp = new Hashtable();
        
        Hashtable tiles = new Hashtable();

        int nexusTileIndex_player0 = GameManager.instance.GetTileIndexOfPlayerNexus(0);
        int nexusTileIndex_player1 = GameManager.instance.GetTileIndexOfPlayerNexus(1);

        for (int i = 0; i < tileSize; i++)
            for(int j = 0; j < tileSize; j++)
            {
                int tilePhotonViewId = PhotonNetwork.AllocateSceneViewID();

                int tileIndex = i + j * tileSize;

                TileType tileType = tilesArray[i, j];

                /// 넥서스 타일이면 FLAT 타입으로 고정 
                if (tileIndex == nexusTileIndex_player0 || tileIndex == nexusTileIndex_player1)
                {
                    tileType = TileType.FLAT;
                }
                /// 테스트
                //if (tileIndex == nexusTileIndex_player0) tileType = TileType.FOREST;
                //else if (tileIndex == nexusTileIndex_player1) tileType = TileType.GRASS;

                Hashtable prop = new Hashtable();
                prop[Tile.TILE_INDEX_PROP_KEY] = tileIndex;
                prop[Tile.TILE_TYPE_PROP_KEY] = (int)tileType;
                prop[Tile.TILE_OWNED_ANIMAL_VIEW_ID_PROP_KEY] = -1;

                /// 바위 타일 결정
                bool tileisRock = false;
                if (tileIndex != nexusTileIndex_player0 && tileIndex != nexusTileIndex_player1)
                {                    
                    /// 확률적으로 바위 배치
                    int diceForRock = UnityEngine.Random.Range(0, 100);
                    tileisRock = diceForRock < creatingRockChance && tileType != TileType.RIVER ? true : false;
                }
                prop[Tile.TILE_IS_WALL_PROP_KEY] = tileisRock;

                tiles[tileIndex] = prop;
            }

        tiles[TILES_SIZE_WIDTH_PROP_KEY] = tileSize;
        tiles[TILES_SIZE_HEIGHT_PROP_KEY] = tileSize;

        tilesProp[TILES_PROP_KEY] = tiles;
        PhotonNetwork.room.SetCustomProperties(tilesProp);
    }


    TileType RollForDecisionTileType()
    {
        TileType decidedType = TileType.FOREST;

        int sum_possibility = 0;
        for(int i = 0; i < tileTypeChances.Length; i++)
        {
            sum_possibility += tileTypeChances[i];
        }

        int randRoll = Random.Range(0, sum_possibility);

        int current = 0;
        for (int i = 1; i < (int)TileType.FOREST; ++i)     //  If we don't match a result before miss, then it's a miss.
        {
            current += tileTypeChances[i];

            if (randRoll < current && randRoll > (current - tileTypeChances[i]))
            {
                decidedType = (TileType)i;

                break;
            }
        }

        return decidedType;
    }
}
