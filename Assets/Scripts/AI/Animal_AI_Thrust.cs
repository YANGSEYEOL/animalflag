﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Animal_AI_Thrust : Animal_AI
{
    bool canThrust { get { return animal.canAction && animal.curRemainSkillCoolTime <= 0; } }

    protected override IEnumerator DoAICore()
    {
        // 운송중인 동물이 본인인 경우
        if (GameManager.instance.flags[0].carryingAnimal == animal)
        {
            /// 깃발을 가지고 있다면, 선 공격이 아닌 선 이동을 택한다 
            yield return MoveToOurFlagPosition();

            /// 스킬 쓰거나 공격하거나
            yield return DoSkillOrAttack();
        }
        else 
        { 
            /// 스킬 쓰거나 공격하거나
            yield return DoSkillOrAttack();

            /// 시야 내에 있는 적을 찾아내고, 접근한다 
            /// 시야 내에 없으면 적 기지로 이동시킨다 
            yield return SearchEnemyAndApproach();

            /// 다시 한번, 스킬쓸 수 있는지 체크 
            yield return DoSkillOrAttack();
        }

        /// 경계 가능하다면 경계
        yield return Overwatch();
    }


    IEnumerator DoSkillOrAttack()
    {
        // 스킬을 쓸 수 있는 타일을 불러온다. 가장 적을 많이, 아군을 적게 공격할 수 있는 타일들이 리스트에 담긴다.
        ThrustCommand.instance.ownAnimal = animal;
        ThrustCommand.instance.GetRangeTiles(animal);

        // 평타 범위 내에 들어오는 동물 리스트를 갱신한다
        AttackCommand.instance.GetAllAttackableTargetInfoList(animal, ref attackableAnimals);

        if (canThrust && ThrustCommand.instance.currentThrustLineDirection != ThrustCommand.ThrustLineDirection.NONE)
        {
            yield return Thrust();
        }    
        // thrust할 수 없는 상황이라면 평타 사거리 내에 있는 동물이 있다면 평타를 친다.
        else if (attackableAnimals.Count > 0)
        {
            yield return DirectingAttackInMyRange();
        }
    }


    IEnumerator Thrust()
    {
        List<Tile> currentThrustLineTileList = ThrustCommand.instance.thrustLineTileList[(int)ThrustCommand.instance.currentThrustLineDirection];
        ThrustCommand.instance.tileToThrust = currentThrustLineTileList[currentThrustLineTileList.Count - 1];
        ThrustCommand.instance.RefreshThrustPath(animal.ownedTile._tileIndex);
        
        animal.canAction = false;
        animal.curRemainSkillCoolTime = ThrustCommand.instance.skillCoolTime;

        yield return ThrustCommand.instance.MoveStaight(animal);

        yield return new WaitForSeconds(1.0f);
    }
}
