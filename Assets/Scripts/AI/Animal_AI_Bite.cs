﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Animal_AI_Bite : Animal_AI
{
    List<Animal> targetAnimals;

    protected override IEnumerator DoAICore()
    {
        if (BiteCommand.instance.IsBiting(animal))
        {
            yield break;
        }
        else
        {
            // 운송중인 동물이 본인인 경우
            if (GameManager.instance.flags[0].carryingAnimal == animal)
            {
                /// 깃발을 가지고 있다면, 선 공격이 아닌 선 이동을 택한다 
                yield return MoveToOurFlagPosition();

                /// 공격하거나 경계하거나
                yield return AttackOrOverwatch();
            }
            else
            {
                // trace가 가능한 타겟들을 불러온다.
                BiteCommand.instance.GetTargets(animal, ref targetAnimals);
                // 평타 범위 내에 들어오는 동물 리스트를 갱신한다
                AttackCommand.instance.GetAllAttackableTargetInfoList(animal, ref attackableAnimals);

                // bite할 타겟이 있다면 bite한다
                if (targetAnimals.Count > 0)
                {
                    int targetIndex = Random.Range(0, targetAnimals.Count);
                    BiteCommand.instance.AddBiteRelation(animal, targetAnimals[targetIndex]);
                    
                    var biteCoroutine = BiteCommand.instance.Bite(animal, targetAnimals[targetIndex]);
                    while (biteCoroutine.MoveNext()) yield return biteCoroutine.Current;
                }
                // 공격할 타겠이있다면 평타친다.
                else if (attackableAnimals.Count > 0)
                {
                    var attackCoroutine = DirectingAttackInMyRange();
                    while (attackCoroutine.MoveNext()) yield return attackCoroutine.Current;
                }
                // 평타 사거리에 동물이 없다면 이동한다.
                else
                {               
                    /// 다시 한번 공격가능한 동물이 있는지 서칭해서 공격 시도해본다 
                    if (animal.curHealth > 0)
                    {
                        /// 시야 내에 있는 적을 찾아내고, 접근한다 
                        /// 시야 내에 없으면 적 기지로 이동시킨다 
                        yield return SearchEnemyAndApproach();
                        
                        // trace가 가능한 타겟들을 불러온다.
                        BiteCommand.instance.GetTargets(animal, ref targetAnimals);
                        if (targetAnimals.Count > 0)
                        {
                            int targetIndex = Random.Range(0, targetAnimals.Count);
                            BiteCommand.instance.AddBiteRelation(animal, targetAnimals[targetIndex]);
                            var biteCoroutine = BiteCommand.instance.Bite(animal, targetAnimals[targetIndex]);
                            while (biteCoroutine.MoveNext()) yield return biteCoroutine.Current;
                        }
                        else
                        {
                            /// 공격하거나 경계하거나
                            yield return AttackOrOverwatch();
                        }
                    }
                }
            }
        }
    }
}