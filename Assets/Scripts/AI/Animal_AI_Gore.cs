﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Animal_AI_Gore : Animal_AI
{
    Tile[] goalTiles;
    List<Animal> targetAnimals;

    int GetTargetIndexForHighestDamage(Animal animal, List<Animal> targetAnimals)
    {
        int bestIndex = 0;
        int bestDistance = 0;
        for(int i = 0; i < targetAnimals.Count;i++)
        {
            int distance = GoreCommand.instance.GetDistance(animal.ownedTile._tileIndex, targetAnimals[i].ownedTile._tileIndex);
            if (bestDistance <= distance)
            {
                bestDistance = distance;
                bestIndex = i;
            }            
        }
        return bestIndex;
    }

    protected override IEnumerator DoAICore()
    {
        // 운송중인 동물이 본인인 경우
        if (GameManager.instance.flags[0].carryingAnimal == animal)
        {
            /// 깃발을 가지고 있다면, 선 공격이 아닌 선 이동을 택한다 
            yield return MoveToOurFlagPosition();

            /// 공격하거나 경계하거나
            yield return AttackOrOverwatch();
        }
        else
        {
            // Gore스킬을 쓸 수 있는 타일, 타겟동물의 목록을 불러온다. 
            GoreCommand.instance.GetTargets(animal, ref targetAnimals, ref goalTiles);
            // 평타 범위 내에 들어오는 동물 리스트를 갱신한다
            AttackCommand.instance.GetAllAttackableTargetInfoList(animal, ref attackableAnimals);

            if (targetAnimals.Count > 0)
            {
                int targetIndex = GetTargetIndexForHighestDamage(animal, targetAnimals);
                var goreCoroutine = GoreCommand.instance.Gore(animal, targetAnimals[targetIndex], goalTiles[targetIndex]);
                while (goreCoroutine.MoveNext()) yield return goreCoroutine.Current;
            }
            else if (attackableAnimals.Count > 0)
            {
                var attackCoroutine = DirectingAttackInMyRange();
                while (attackCoroutine.MoveNext()) yield return attackCoroutine.Current;
            }
            // 평타 사거리에 동물이 없다면 이동한다.
            else
            {
                /// 다시 한번 공격가능한 동물이 있는지 서칭해서 공격 시도해본다 
                if (animal.curHealth > 0)
                {
                    /// 시야 내에 있는 적을 찾아내고, 접근한다 
                    /// 시야 내에 없으면 적 기지로 이동시킨다 
                    yield return SearchEnemyAndApproach();

                    // Gore스킬을 쓸 수 있는 타일, 타겟동물의 목록을 불러온다. 
                    GoreCommand.instance.GetTargets(animal, ref targetAnimals, ref goalTiles);

                    if (targetAnimals.Count > 0)
                    {
                        int targetIndex = GetTargetIndexForHighestDamage(animal, targetAnimals);
                        var goreCoroutine = GoreCommand.instance.Gore(animal, targetAnimals[targetIndex], goalTiles[targetIndex]);
                        while (goreCoroutine.MoveNext()) yield return goreCoroutine.Current;
                    }
                    else
                    { 
                        /// 공격하거나 경계하거나
                        yield return AttackOrOverwatch();
                    }
                }                
            }
        }
    }
}
