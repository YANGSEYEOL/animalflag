﻿using UnityEngine;
using System.Collections;

public class Scorpion : MonoBehaviour {

    bool turning = false;
    int speed = 20;
    int rotationSpeed = 10;
    Vector3 direction;
    // Use this for initialization
    void OnEnable () {
        StartCoroutine(DoIdleMovement());
        StartCoroutine(RotateScorpion());
        StartCoroutine(Dig());
	}

    MeshRenderer meshRenderer;
    bool isVisible
    {
        get
        {
            return meshRenderer.enabled;
        }

        set
        {
            meshRenderer.enabled = value;
        }
    }

    void Start()
    {
        meshRenderer = GetComponentInChildren<MeshRenderer>();
    }

	// Update is called once per frame
	void Update () {
        // 안개에 위치하면 렌더러를꺼줌
        if (TileChunk.instance.IsValidTilePos(transform.position.x, transform.position.z))
        {
            bool tileVisible = TileChunk.instance.GetTileWithPosition(transform.position).isVisible;
            if (isVisible != tileVisible) isVisible = tileVisible;
        }

        if (isIdling || isDigging)
            return;

        if (transform.position.x <= 0 || transform.position.z >= 0 ||
             transform.position.x >= Tile.TILE_WORLD_WIDTH * TileChunk.instance.tileSizeWidth ||
             transform.position.z <= -Tile.TILE_WORLD_HEIGHT * TileChunk.instance.tileSizeHeight ||
             TileChunk.instance.GetTileWithPosition(transform.position).type != TileMaker.TileType.DESSERT ||
             TileChunk.instance.IsValidTilePos(transform.position.x, transform.position.z))
        {
            turning = true;
        }
        else
            turning = false;

        // 벽에 부딪혔을때
        if (turning)
        {
            transform.Translate(0, 0, -3 * Time.deltaTime * speed);
            direction = TileChunk.instance.GetTileWithPositionForFish(transform.position).position - transform.position;
            direction.y = 0;
            transform.rotation = Quaternion.LookRotation(direction);
            speed = Random.Range(10, 20);
        }
        // 벽에 부딪히지않고있을 경우
        else
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);
            speed = Random.Range(10, 20);
        }
        
        transform.Translate(0, 0, Time.deltaTime * speed);
    }

    WaitForSeconds rotateInterval = new WaitForSeconds(1);
    IEnumerator RotateScorpion()
    {
        while (gameObject.GetActive())
        {
            direction += Vector3.forward * Random.Range(-10, 11) + Vector3.left * Random.Range(-10, 11); 
            yield return rotateInterval;
        }
    }

    bool isIdling = false;
    WaitForSeconds idleInterval = new WaitForSeconds(1);
    IEnumerator DoIdleMovement()
    {
        while (gameObject.GetActive())
        {
            if (Random.Range(0, 3) < 1)
                isIdling = true;
            else
                isIdling = false;           
            
            yield return idleInterval;
        }
    }

    bool isDigging = false;
    WaitForSeconds digInterval = new WaitForSeconds(5);
    IEnumerator Dig()
    {
        while (true)
        {
            yield return digInterval;
            if (isDigging)
                continue;
            isDigging = true;
            GetComponent<Animator>().SetBool("dig", true);
            StartCoroutine(DigOut());
        }
    }

    public void CreateDigSmokeAndRipple()
    {
        ScorpionManager.instance.digSmoke[ScorpionManager.instance.smokeIndex].transform.position = transform.position;
        ScorpionManager.instance.digSmoke[ScorpionManager.instance.smokeIndex].Play();
        ScorpionManager.instance.sandRipple[ScorpionManager.instance.smokeIndex].transform.position = transform.position + Vector3.up * 0.2f + transform.forward * 3;
        ScorpionManager.instance.sandRipple[ScorpionManager.instance.smokeIndex].Play();

        ScorpionManager.instance.smokeIndex++;
        if(ScorpionManager.instance.smokeIndex >= ScorpionManager.instance.digSmoke.Length)
        {
            ScorpionManager.instance.smokeIndex = 0;
        }
    }
   
    WaitForSeconds digOutInterval = new WaitForSeconds(5);
    IEnumerator DigOut()
    {
        yield return digOutInterval;
        
        transform.position = ScorpionManager.instance.GetRandomSandTile().position_AdjustedTileYFactor;
       // Camera.main.MoveTo(transform.position, true);
        GetComponent<Animator>().SetBool("dig", false);
    }

    public void StopDigging()
    {
        isDigging = false;
    }
}
