﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vectrosity;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public delegate void OnChangedTileVisibleDelegate(Tile tile, bool visible);
public delegate void OnChangedOwningAnimalDelegate(Animal animal);

/// <summary>
/// MonoBehavior를 상속받는 순간 몇백개의 타일을 생성할 때, 렉이 발생한다.
/// 일반적인 C# 클래스 형태로 사용해야 한다.
/// </summary>
public class Tile //: Photon.PunBehaviour
{
    public TileChunk tileChunk;
    public enum SeasonEnum : int { NORMAL = 0, RAINY, DRAUGHT };
    public Tile.SeasonEnum tileSeason = 0;
    public int _tileIndex;
    /*
    public RainySeasonProcessor rainySeasonProcessor;
    public DraughtSeasonProcessor draughtSeasonProcessor;
    public NormalSeasonProcessor normalSeasonProcessor;
    SeasonProcessor tileSeasonProcessor;
    */
    static GameObject parentGrass;
    GameObject grassObject;

    public TileMaker.TileType type;
    public const float TILE_WORLD_WIDTH = 35.0f;
    public const float TILE_WORLD_HEIGHT = 35.0f;
    /*
    private int _water;
    public int water
    {
        get
        {
            return _water;
        }
        set
        {
            if (value <= 0)
            {
                if (_water > 0)
                {
                    tileChunk.ChangeTileTypeAndTexture(this, TileMaker.TileType.DESSERT);
                    _water = 0;
                }
            }
            else
            {
                if(type == TileMaker.TileType.DESSERT)
                {
                    tileChunk.ChangeTileTypeAndTextureFromDessert(this);
                }
                _water = value;
            }
        }
    }
    private int _grass;
    public int grass
    {
        get { return _grass; }
        set
        {
            if (value <= 0)
            {
                if (_grass > 0)
                {
                    tileChunk.ChangeTileTypeAndTexture(this, TileMaker.TileType.DESSERT);
                    _grass = 0;
                }
            }
            else
            {
                if (type == TileMaker.TileType.DESSERT)
                {
                    tileChunk.ChangeTileTypeAndTextureFromDessert(this);
                }
                _grass = value;
            }
        }
    }
    private int _meat;
    public int meat
    {
        get { return _meat; }
        set
        {
            if (value <= 0)
            {
              // 고기가 바닥남  tileChunk.ChangeTileToDessert(this);
                _meat = 0;
            }
            else
            {
                _meat = value;
            }
        }
    }
    private bool hasTurnedGrey;
    */
    Animal _owningAnimal;
    public Animal owningAnimal
    {
        get { return _owningAnimal; }
        set { _owningAnimal = value;
            OnChangedOwningAnimal(value);

            if(value == null)
            {
                tileChunk.alphaTex.SetPixel(TileIndex_X, TileMaker.instance.tileSize - TileIndex_Y - 1, new Color(1, 1, 1));
            }
            tileChunk.alphaTex.Apply();
        }
    }

    /// 인접타일에 의해 변경되는 텍스처링을 위한 변수. 텍스쳐의 SpriteIndex를 들고 있다 
    public int[] subTileIndexes = new int[4];

    /// 시야 관련 변수 
    bool _isRock = false;
    public bool isRock
    {
        get { return _isRock; }
        set
        {
            _isRock = value;
        }
    }
    public bool isExplored = false;

    // 투명유닛이 보이는 타일
    public Dictionary<string, bool> isDetected = new Dictionary<string, bool>();

    bool _isVisible = false;
    public bool isVisible
    {
        get { return _isVisible; } 
        set
        { 
            _isVisible = value;

            if (value)
            {
                isExplored = true;
            }

            OnChangedTileVisible(this, value);
        }
    }
    public bool isOriginFlagTile = false;

    /// <summary>
    ///  이 타일이 깃발 시작 타일일 때, 누구의 깃발 지역인지 명시
    /// </summary>
    public string originFlagTileOwnerUserId;     

    public OnChangedTileVisibleDelegate OnChangedTileVisible = delegate { };
    public OnChangedOwningAnimalDelegate OnChangedOwningAnimal = delegate { };

    public int TileIndex_X { get{ return _tileIndex % TileMaker.instance.tileSize; } }
    public int TileIndex_Y { get{ return _tileIndex / TileMaker.instance.tileSize; } }

    public static readonly string TILE_INDEX_PROP_KEY = "tileIndex";
    public static readonly string TILE_TYPE_PROP_KEY = "tileType";
    public static readonly string TILE_OWNED_ANIMAL_VIEW_ID_PROP_KEY = "tileOwnedAnimalViewIdIndex";
    public static readonly string TILE_SEASON_PROP_KEY = "tileSeason";
    public static readonly string TILE_IS_WALL_PROP_KEY = "tileisRock";


    public Tile()
    {
        //tileSeasonProcessor = normalSeasonProcessor;
        
    }
  

    public enum TileVertex
    { 
        LEFT_TOP,
        LEFT_BOTTOM,
        RIGHT_TOP,
        RIGHT_BOTTOM
    }


    public Vector3 position
    {
        get
        {
            int subTileX = TileIndex_X * 2;
            int subTileY = TileIndex_Y * 2;

         
            /// 현재 반환되는 타일의 위치를 볼 때 0,0은 좌상단이고, 끝타일은 우하단에 위치한다
            float px = (subTileX + 1) * (TILE_WORLD_WIDTH * 0.5f);
            float py = -(subTileY + 1) * (TILE_WORLD_HEIGHT * 0.5f);

            return tileChunk.transform.position + (new Vector3(px, 0, py));
        }
    }


    public Vector3 position_AdjustedTileYFactor
    {
        get
        {
            Vector3 pos = position;

			/// 1을 빼주는 이유는 타일 위에 Vectrosity Line을 그리기 위해서다 
            pos.y += -1f + TileChunk.instance.yFactorOfTileType[(int)type];

            return pos;
        }
    }

    void MakeForestObjects()
    {
        Vector3 cloakObjectPosition = position_AdjustedTileYFactor;
        TileChunk.instance.LoadAndPushMeshResource("Forest/Forest_" +  Random.Range(1, 4).ToString("D1"), cloakObjectPosition);      
    }

    void MakeRockObjects()
    {
        Vector3 cloakObjectPosition = position_AdjustedTileYFactor;
        TileChunk.instance.LoadAndPushMeshResource("Rocks/Rock_" + Random.Range(1, 4).ToString("D1"), cloakObjectPosition);
    }

    void MakeDesertObjects()
    {        
        if (Random.Range(0, 10) < 8)
        {
            return;
        }
        Vector3 cactusPosition = position_AdjustedTileYFactor;
        float y = cactusPosition.y;

        cactusPosition.x -= 15;
        cactusPosition.z -= 15;
        for (int i = 0; i < Random.Range(1, 3); i++)
        {
            cactusPosition.x += Random.Range(0, 5);
            cactusPosition.z += Random.Range(0, 5);
            int cactus = Random.Range(1, 3);            
            TileChunk.instance.LoadAndPushMeshResource("Desert/cactus" + cactus.ToString("D1"), cactusPosition);
        }
    }

    void MakeGrassObjects()
    {
        //GameObject grassObject = (GameObject)Resources.Load("Grass/Grass");
        //Bounds meshBounds = grassObject.GetComponentInChildren<MeshFilter>().sharedMesh.bounds;
        //Vector3 meshSize = Vector3.Scale(meshBounds.size, grassObject.transform.localScale);
        //Vector3 cloakObjectPosition = position_AdjustedTileYFactor;
        //int plantCount = 4;
        //float marginFromSide = 2.5f;

        //float sizeWidth = (TILE_WORLD_WIDTH - (marginFromSide * 2) - (meshSize.x)) / (float)(plantCount - 1);
        //float sizeHeight = (TILE_WORLD_HEIGHT - (marginFromSide * 2) - (meshSize.z)) / (float)(plantCount - 1);

        //for (int x = 0; x < plantCount; ++x)
        //{
        //    for (int y = 0; y < plantCount; ++y)
        //    {
        //        Vector3 grassPos = GetVertexPositionByScale(TileVertex.LEFT_TOP);
        //        grassPos.x += x * sizeWidth + (meshSize.x / 2) + marginFromSide;
        //        grassPos.y += -1f + TileChunk.instance.yFactorOfTileType[(int)type];
        //        grassPos.z -= y * sizeHeight + (meshSize.z / 2) + marginFromSide;

        //        TileChunk.instance.LoadAndPushMeshResource("Grass/Grass", grassPos);
        //    }
        //}
        Vector3 cloakObjectPosition = position_AdjustedTileYFactor;
        TileChunk.instance.LoadAndPushMeshResource("Grass/Grass_" + Random.Range(1, 2).ToString("D1"), cloakObjectPosition);
    }

    void MakeFlatObjects()
    {
        if(Random.Range(0,3) < 1)
        {
            return;
        }
        Vector3 flowerPosition = position_AdjustedTileYFactor;
        float y = flowerPosition.y;

        flowerPosition.x -= 10;
        flowerPosition.z -= 10;
        for (int i = 0; i < Random.Range(0, 5); i++)
        {
            flowerPosition.x += Random.Range(0, 5);
            flowerPosition.z += Random.Range(0, 5);
            int flower = Random.Range(1, 4);
            if (Random.Range(0, 2) < 1)
            {
                switch (flower)
                {
                    case 1:
                        flowerPosition.y = 10.53f;
                        break;
                    case 2:
                        flowerPosition.y = 0.5f;
                        break;
                    case 3:
                        flowerPosition.y = 1.65f;
                        break;                    
                }
                ButterflyManager.instance.flowersPos.Add(flowerPosition);

                flowerPosition.y = y;
            }
            TileChunk.instance.LoadAndPushMeshResource("Flats/Flower_" +flower.ToString("D1"), flowerPosition);
        }
    }

    public void Init()
    {
        GameManager.instance.OnChangedPlayerTurn += OnChangedPlayerTurn;

        if (isRock)
        {
            MakeRockObjects();
        }
        else
        {
            if (type == TileMaker.TileType.FOREST)
            {
                MakeForestObjects();
            }
            else if (type == TileMaker.TileType.GRASS)
            {
                MakeGrassObjects();
            }
            else if (type == TileMaker.TileType.FLAT)
            {
                MakeFlatObjects();
            }
            else if (type == TileMaker.TileType.DESSERT)
            {
                TileChunk.instance.desertExisting = true;
                MakeDesertObjects();
            }
        }
    }

    VectorLine line;
    void OnChangedPlayerTurn(string turnUserId)
    {
        /// 딱 한번만 한다
        /// init 하는게 가장 심플한데, init에서 하니 Vectrosity 초기화 타이밍이랑 안 맞아서 그런지 라인 스케일이 제대로 반영 안되는 문제 발생 
        if (PhotonNetwork.offlineMode && isOriginFlagTile && line == null)
        {
            line = new VectorLine("FlagTile_" + _tileIndex, new List<Vector3>(), TileChunk.instance.lineMaterial, TileChunk.instance.intensityOfGlowLine, LineType.Discrete, Joins.Weld);

            line.Resize(0);

            TileChunk.instance.AddVerticesIntoLinePointsByScale(this, 0.25f, line);

            line.Draw3D();
            line.color = originFlagTileOwnerUserId == GameManager.instance.userIDForSimulation_origin ? Color.white : Color.yellow;
        }
    }


    public bool CheckPointIsInTile(Vector3 point)
    {
        Vector3 leftTop = GetVertexPositionByScale(TileVertex.LEFT_TOP, 1.0f);

        Vector3 rightBottom = GetVertexPositionByScale(TileVertex.RIGHT_BOTTOM, 1.0f);

   
        if (leftTop.x <= point.x && point.x <= rightBottom.x &&
            leftTop.y <= point.y && point.y <= rightBottom.y)
        {
        
            return true;
        }
       
        return false;
    }


    public Vector3 GetVertexPositionByScale(TileVertex vertex, float scale = 1f)
    {

        Vector3 vertexPosition = position;
        switch (vertex)
        {
            case TileVertex.LEFT_TOP:
                {
                    vertexPosition.x -= TILE_WORLD_WIDTH * 0.5f * scale;
                    vertexPosition.z += TILE_WORLD_HEIGHT * 0.5f * scale;
                }
                break;
            case TileVertex.LEFT_BOTTOM:
                {
                    vertexPosition.x -= TILE_WORLD_WIDTH * 0.5f * scale;
                    vertexPosition.z -= TILE_WORLD_HEIGHT * 0.5f * scale;
                }
                break;
            case TileVertex.RIGHT_TOP:
                {
                    vertexPosition.x += TILE_WORLD_WIDTH * 0.5f * scale;
                    vertexPosition.z += TILE_WORLD_HEIGHT * 0.5f * scale;
                }
                break;
            case TileVertex.RIGHT_BOTTOM:
                {
                    vertexPosition.x += TILE_WORLD_WIDTH * 0.5f * scale;
                    vertexPosition.z -= TILE_WORLD_HEIGHT * 0.5f * scale;
                }
                break;
        }

        return vertexPosition;
    }

    /*
    public void SetTileSeason(Tile.SeasonEnum season)
    {
        tileSeason = season;
    }
    

    public void SetToRainySeason()
    {
        SetTileSeason(SeasonEnum.RAINY);
        tileSeasonProcessor = rainySeasonProcessor;
    }
    

    public void SetToDraughtSeason()
    {
        SetTileSeason(SeasonEnum.DRAUGHT);
        tileSeasonProcessor = draughtSeasonProcessor;
    }

    public void SetToNormalSeason()
    {
        SetTileSeason(SeasonEnum.NORMAL);
        tileSeasonProcessor = normalSeasonProcessor;
    }
    

    public void ProcessSeasonEffect()
    {
       tileSeasonProcessor.SeasonProcess(this);
    }

    public void SetInitialResourcesByType()
    {
        if (type == TileMaker.TileType.RIVER)
        {
            _meat = 0;
            _grass = 0;
            _water = 1;
        }
        else if (type != TileMaker.TileType.DESSERT)
        {
            _grass = 5;
            _water = 0;
        }
    }*/
}