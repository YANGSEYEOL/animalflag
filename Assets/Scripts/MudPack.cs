﻿using UnityEngine;
using System.Collections;

public class MudPack : Skill
{
    public override void Init(Animal _ownAnimal)
    {
        ownAnimal = _ownAnimal;
        InitSkillDescription();
        InitSkillName();
    }

    public override void InitSkillDescription()
    {
        description = "This is MudPack";
    }

    public override void InitSkillName()
    {
        skillName = "MudPack";
    }

    public override void InitIsActive()
    {
        isActive = false;
    }

    public override void Do()
    {
        Debug.Log(ownAnimal.name + "used a MudPack");
        if(ownAnimal.ownedTile.type == TileMaker.TileType.RIVER)
        {           
            ownAnimal.curHealth++;
            //ownAnimal.SkillUseEffect(skillName);
        }
    }
}
