﻿using UnityEngine;
using System.Collections;


// 이제 하위 hp바들은 HPBarGrid에서 관리한다. 
// 최대 hp가 증가하거나 감소했을 경우, hp바를 새로 생성하거나 제거해주어야 한다. 물론 배열에서도 등록 제거해주는 작업이 필요하다.
//
public class HPBarGrid : MonoBehaviour {

    //public Animal _OwnerAnimal;
    //public GameObject[] _HPBar;
    //public GameObject _HpBarPrefab;
    //// 체력이 수정되어야 할 index
    //int _curHpBarIndex;

    //int _curHp;
    //int _oriHp;

    ///*
    //   해야할 일
    //   grid가 체력에 따라 hpbar개수를 알아서 생성하고 지워야 한다.
    //   그래서 hp바 생성을 hpbargrid에서 담당해주고 hpbargrid만 uimanager에서 관리를 해주어야 한다고 생각한다.
    // */
    //private void Start()
    //{
    //    // 우선 하나의 hpGrid당 hpBar들을 10개까지 담을 수 있다고 치자. 그러면 동물당 최대체력이 50까지는 커버가 된다는 의미이다.
    //}

    //private void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.Q))
    //    {
    //        setCurHpTo(1);
    //    }   
    //}
    //public void init()
    //{
    //    _HPBar = new GameObject[10];
    //    Debug.Log("hpbargrid allocated..");
    //    // hpBar의 개수는 동물체력의 5로나눈 몫이다.
    //    _curHp = _OwnerAnimal.curHealth;
    //    _oriHp = _OwnerAnimal.animalData.health;

    //    /*
    //     체력  : hp개수
    //     1 ~ 5 : 1
    //     6 ~ 10 : 2
    //     11 ~ 15 : 3
    //    */
    //    int hpBarCount = 0;
    //    if (_curHp % 5 != 0) // _curHp 가 5의배수일때와 아닐때 hp를 설정하는 기준이 달라진다.
    //        hpBarCount = _curHp / 5 + 1;
    //    else
    //        hpBarCount = _curHp / 5;

    //    // 동물의 HP에 따라 HP BAR의 개수를 조절해가며 생성한다. 예) 6이면 2개의 BAR, 12면 3개의 BAR, 2이면 1개의 BAR

    //    for (int i = 0; i < hpBarCount; i++)
    //    {
    //        GameObject newHpBar = (GameObject)Instantiate(_HpBarPrefab);
    //        newHpBar.transform.SetParent(this.transform);
    //        _HPBar[i] = newHpBar;
    //    }
    //    if (_curHp % 5 != 0)
    //    {
    //        _HPBar[hpBarCount - 1].GetComponent<HPBar>().setCurHpTo(_curHp % 5);
    //        _HPBar[hpBarCount - 1].GetComponent<HPBar>().setOriHpTo(_curHp % 5);

    //    }
    //    // HPBAR들을 생성했으면 이제 GRID에 맞게 리포지션해준다..
    //    GetComponent<UIGrid>().Reposition();
    //}

    //public void GainCurHp(int amount)
    //{
    //    int quetient = amount / 5 + 1;
    //    int remainder = amount % 5;

    //    amount = remainder;

    //    while(quetient-- != 0 && _HPBar[_curHpBarIndex].GetComponent<HPBar>().GainCurHp(amount))
    //    {            
    //        _curHpBarIndex++;
    //        _HPBar[_curHpBarIndex].SetActive(true);
    //        GetComponent<UIGrid>().Reposition();
    //        amount = 5;
    //    }
    //}

    //public void ReduceCurHp(int amount)
    //{
    //    int quetient = amount / 5 + 1;
    //    int remainder = _curHp % 5;
    //    amount = remainder;

    //    while (quetient-- != 0 && _HPBar[_curHpBarIndex].GetComponent<HPBar>().ReduceCurHp(amount))
    //    {
    //        _curHpBarIndex--;
    //        if(_curHpBarIndex < 0)
    //        {
    //            return;
    //        }
    //        _HPBar[_curHpBarIndex].SetActive(false);
    //        GetComponent<UIGrid>().Reposition();
    //        _curHp -= remainder;
    //        remainder = _curHp % 5;
    //        amount = remainder;
    //    }
    //}

    //public void GainOriHp(int amount)
    //{
    //    while(_HPBar[_curHpBarIndex].GetComponent<HPBar>().GainOriHp(amount))
    //    {
    //        _curHpBarIndex--;
    //        _HPBar[_curHpBarIndex].SetActive(false);
    //        GetComponent<UIGrid>().Reposition();
    //    }
    //}

    //public void ReduceOriHp(int amount)
    //{
    //    while (_HPBar[_curHpBarIndex].GetComponent<HPBar>().ReduceOriHp(amount))
    //    {
    //        _curHpBarIndex--;
    //        _HPBar[_curHpBarIndex].SetActive(false);
    //        GetComponent<UIGrid>().Reposition();
    //    }
    //}

    //public void setOriHpTo(int amount)
    //{
    //    GainOriHp(amount - _oriHp);
    //    _oriHp = amount;
    //}

    //public void setCurHpTo(int amount)
    //{
    //    if(amount - _curHp > 0)
    //    {
    //        GainCurHp(amount - _curHp);
    //    }
    //    else
    //    {
    //        ReduceCurHp(_curHp - amount);
    //    }
       
    //    _curHp = amount;
    //}
}
