﻿using UnityEngine;
using System.Collections.Generic;
using System;
using GameDataEditor;
using Random = UnityEngine.Random;


public class WhistleManager : MonoBehaviour 
{
	public static WhistleManager instance;

	[SerializeField]
	int countOfWhistlesGot = 0;

	[SerializeField]
	int countOfGoldenWhistlesGot = 0;
	
	[SerializeField]
	int currentTier = 0;

	/// 동물 이름, 상자깡에서 나온 카운팅
	Dictionary<string, int> countOfAnimalsWhistlesDic = new Dictionary<string, int>();

	void Awake () 
	{
		instance = this;
	}
	
	public string GetWhistle()
	{
		/// 상자 사이클 
		/// 90 실버 휘슬(75%), 25 골드 휘슬(21%), 2 백금 휘슬, 1 럭키 휘슬, 1 레어 휘슬, 1 스페셜 휘슬 = 총 120 사이클 

		string whistleName = "";

		countOfWhistlesGot++;

		/// 현재 승리 보상 상자 인덱스를 보상받을 때마다 저장시켜두고 있기 
		/// 인덱스 % 4 == 0  이면 골드 상자 
		if (countOfWhistlesGot % 4 == 0)
		{
			whistleName = GDEItemKeys.Whistle_GoldenWhistle;

			countOfGoldenWhistlesGot++;

			// 골드 상자를 몇번 받았는지도 저장해두고 있어야 한다 
			// 골드 상자 인덱스 % 6 == 0 이면 백금. 럭키. 레어. 스페셜을 순서대로 지급한다 
			if (countOfGoldenWhistlesGot % 6 == 0)
			{
				switch (countOfGoldenWhistlesGot / 6)
				{
					case 0:
					case 1:
						whistleName = GDEItemKeys.Whistle_PlatinumWhistle;
					break;
					case 2:
						whistleName = GDEItemKeys.Whistle_LuckyWhistle;
					break;
					case 3:
						whistleName = GDEItemKeys.Whistle_RareWhistle;
					break;
					case 4:
						whistleName = GDEItemKeys.Whistle_SpecialWhistle;
					break;
				}
			}
		}
		else
		{
			whistleName = GDEItemKeys.Whistle_SilverWhistle;
		}
		
		/// 총 인덱스가 120에 도달할 시 모든 인덱스를 리셋하고 처음부터 다시 돈다 
		if (countOfWhistlesGot >= 120)
		{
			countOfWhistlesGot = 0;
			countOfGoldenWhistlesGot = 0;
		}

		Debug.Log("GetWhistle : " + whistleName);

		return whistleName;
	}


	class AnimalInfo
	{
		public string animalName;
		public int countOfWhistles;	/// 지금까지 상자에서 뽑힌 횟수
		public float stake;			/// 다른 동물들과 뽑힌 횟수를 다 더해서 총합을 냈을 때, 이 동물이 가지는 횟수 지분율 
		public float stakeOffset;		/// 많이 뽑힌 동물은 덜 뽑히게 하고, 덜 뽑힌 동물은 더 뽑히게 하기 위한 확률 보정 오프셋
		public float finalizedChance;	/// 최종적으로 결정되는 이 동물의 가챠 확률 
	}
	List<AnimalInfo> animalInfoList = new List<AnimalInfo>();

	enum Rarity { S, A, B, C  }

	public void OpenWhistle(string whistleName)
	{
		GDEWhistleData whistleData = new GDEWhistleData(whistleName);

		/// 1. 무슨 등급을 뽑을지 결정 
		Rarity decidedRarity = whistleName == GDEItemKeys.Whistle_LuckyWhistle ? Rarity.B : Rarity.C;

        int randRoll = Random.Range(0, 100);

        int current = 0;
		/// 특별상자는 C등급이 나오지 않게 한다 
        for (int i = 0; i < (int)(whistleName == GDEItemKeys.Whistle_LuckyWhistle ? Rarity.B : Rarity.C); ++i)
        {
            current += whistleData.Tier_GoldAndChances[currentTier].Chances[i];

            if (randRoll < current)
            {
                decidedRarity = (Rarity)i;

                break;
            }
        }

		/// 2. 내가 뽑을 수 있는 동물들 중에 그 등급인 애들만 선별한다 
		animalInfoList.Clear();

		List<GDEAnimalData> allAnimalData = GDEDataManager.GetAllItems<GDEAnimalData>();

		do
		{
			foreach (var animalData in allAnimalData)
			{
				if (animalData.rarity == "") continue;

				/// 티어 체크
				if (!whistleData.ignoreAnimalTier && animalData.tier > currentTier) continue;

				/// 등급 체크
				Rarity animalRarity = (Rarity)Enum.Parse(typeof(Rarity), animalData.rarity);
				if (animalRarity == decidedRarity)
				{
					/// 데이터 이름으로 Dictionary을 검색해서 뽑힌 횟수를 알아와야 한다 
					int countOfAnimalGot = 0;
					countOfAnimalsWhistlesDic.TryGetValue(animalData.Key, out countOfAnimalGot);

					AnimalInfo animalInfo = new AnimalInfo();
					animalInfo.animalName = animalData.Key;
					animalInfo.countOfWhistles = countOfAnimalGot;

					animalInfoList.Add(animalInfo);
				}
			}

			/// 등급을 뽑으려고 하는데 내 티어에서는 뽑을 수 있는 등급이 없을 때가 있다 
			/// 그럴 때 뽑는 등급을 한 단계 낮춘다 
			if (animalInfoList.Count <= 0) decidedRarity--;
		}
		while (animalInfoList.Count <= 0);

		string debugString = "";

		/// 3. 확률 보정 
		/// 카운팅 총합 계산 
		int totalCount = 0;
		foreach (AnimalInfo animalInfo in animalInfoList)
		{
			totalCount += animalInfo.countOfWhistles;
		}

		/// 각 카드가 총합에서 차지하는 지분율 계산 
		for (int i = 0; i < animalInfoList.Count && totalCount > 0; ++i)
		{
			AnimalInfo animalInfo = animalInfoList[i];
			animalInfo.stake = (float)animalInfo.countOfWhistles / (float)totalCount * 100f;
		}

		/// 모든 카드의 지분율이 공평할 때의 지분율을 계산 
		float basicStake = 100f / (float)animalInfoList.Count;
		debugString += "공평 확률 : " + basicStake + "\n";
		
		/*
		/// 지분율 중 가장 높은 지분율이 몇인지 알아내기
		float stakeOffsetFromTheMostStake = 0f;
		float theMostStake = 0f;
		for(int i=0; i<LIST_MAX; ++i)
		{
			if (cardStakeList[i] > theMostStake)
			{
				theMostStake = cardStakeList[i];
				stakeOffsetFromTheMostStake = basicStake - cardStakeList[i];
			}
		}
		
		/// 최소 보장 확률 
		float minimumGuaranteedChance = 0.1f;
		
		/// minimumGuaranteedChance를 넣는 이유는 이것을 고려하지 않으면  offsetWeight가 높아졌을 때 지분율이 높으면 마이너스 확률이 배정되기 때문이다abstract
		/// 마이너스 확률이 보장되면 플러스 확률로 배정된 카드가 확정적으로 뽑히겠지만, 문제는 인덱스상 앞쪽에 배치된 카드가 거의 무조건 뽑히게 된다
		/// 그래서 지분율이 가장 큰 카드라도 마이너스 확률로 배정되지 않는 offsetWeight를  계산한다 
		float limit_offsetWeight = (minimumGuaranteedChance - basicStake) / stakeOffsetFromTheMostStake;
		*/

		/// 오프셋 차이를 더 크게 만들어서 확률에서 더 큰 차이가 나도록 만들기 위함
		/// 오프셋 웨이트는 리스트 총 갯수에 따라 유동적이어야 한다 
		/// 총 갯수가 커질 때 커지지 않으면 보정이 너무 작아져서 새로운게 너무 안 뽑히고
		/// 총 갯수가 작을 때 너무 크면 너무 잘 뽑혀서 문제가 된다 
	
		/// 이 값은 뽑힐 수 있는 총 카드 갯수를 기준으로 잡는게 좋다 
		/// 총 카드 갯수 6일 때, 이 값이 3이면 가장 지분율을 가진 카드가 뽑힐 확률이 50% 이상 배정된다
		/// 이 값이 총 카드 갯수의 수렴할 수록 지분율이 낮은 카드가 뽑힐 확률이 100%로 수렴한다
		float OFFSET_WEIGHT = (float)animalInfoList.Count / 2f;

		/// 각각의 카드 지분율이 공평 지분율이 되기 위한 확률 오프셋을 계산
		for (int i = 0; i < animalInfoList.Count && totalCount > 0; ++i)
		{
			AnimalInfo animalInfo = animalInfoList[i];
			animalInfo.stakeOffset = (basicStake - animalInfo.stake) * OFFSET_WEIGHT;
		}

		/// 확률 오프셋을 통해 카드들의 최종 확률을 결정한다 
		float totalChance = 0f;
		for (int i = 0; i < animalInfoList.Count; ++i)
		{
			AnimalInfo animalInfo = animalInfoList[i];

			animalInfo.finalizedChance = basicStake + animalInfo.stakeOffset;

			debugString += string.Format("{0} 뽑힌 수 : {1}, 지분율 : {2}, 최종확률 {3} \n", animalInfo.animalName, animalInfo.countOfWhistles, animalInfo.stake, animalInfo.finalizedChance);

			totalChance += animalInfo.finalizedChance;
		}
		
		debugString += "확률 총합 : " + totalChance + "\n";


		/// 4. 동물 뽑기 
		string decidedAnimal = animalInfoList[animalInfoList.Count-1].animalName;

        int randRoll2 = Random.Range(0, 100);

        float current2 = 0;
        foreach (AnimalInfo animalInfo in animalInfoList)
        {
            current2 += animalInfo.finalizedChance;

            if (randRoll2 < current2)
            {
                decidedAnimal = animalInfo.animalName;

                break;
            }
        }

		debugString += "뽑힌 동물 : " + decidedAnimal + "\n";


		/// 5. 뽑힌 동물 카운팅 증가 
		if (countOfAnimalsWhistlesDic.ContainsKey(decidedAnimal))
		{
			countOfAnimalsWhistlesDic[decidedAnimal]++;
		}
		else
		{
			countOfAnimalsWhistlesDic[decidedAnimal] = 1;
		}

		Debug.Log(debugString);


		/*
		알고리즘 정리 
		예를 들어, 뽑을 수 있는 카드 5개가 있고, 각각의 뽑힌 횟수가 아래와 같다고 가정한다
		10  10  10  10  0
		총 5개
		총합은 40 
		총합에서 각각의 카드가 가지는 지분율은 아래와 같다
		25% 25% 25% 25% 0%
		만약, 이 5개 카드에 아무런 보정을 주지 않고 가챠를 돌릴 때 확률은 아래와 같다 
		20% 20% 20% 20% 20%
		카드 각각의 지분율이 공평한 확률이 되기 위한 확률 가중치값을 각각 계산한다 
		-5% -5% -5% -5% +20%
		확률 가중치값을 더해서 최종 확률을 결정한다
		15% 15% 15% 15% 40%
		* 오프셋 웨이트를 통해 가중치를 더 심하게 줄 수 있다 
		 */
	}
}
