﻿using UnityEngine;
using UnityEngine.UI;
using GameDataEditor;
using System.Collections;
using System.Collections.Generic;
using Photon;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public delegate void OnAnimalDamagedDelegate(Animal damagedAnimal);
public delegate void OnSelectedAnimalDelegate(Animal selectedAnimal);
public delegate void OnSetTargetAnimalDelegate(Animal targetAnimal);
public delegate void OnAnimalDiedDelegate(Animal deadAnimal);
public delegate void OnAnimalLevitatedDelegated(Animal levitatedAnimal);
public delegate void OnAnimalGoredDelegated(Animal goredAnimal);
public delegate void OnAnimalBittenDelegated(Animal bittenAnimal);
public delegate void OnAnimalBlockedDelegated(Animal animal, Animal targetAnimal);
public delegate void OnChangedPlayerTurnDelegate(string turnUserId);

public class GameManager : CacheMonobehavior, IPunTurnManagerCallbacks
{
    public static GameManager instance;


    [SerializeField]
    UILabel tileIdxDebugUI;

    public enum ResultType
    {
        None = 0,
        Draw,
        LocalWin,
        LocalLoss
    }
    private ResultType result;

    private PunTurnManager turnManager;

    public GameObject selectionHighlight_blue;
    public GameObject selectionHighlight_red;

    /// 밤낮 관리자
    DayAndNight dayAndNight;

    public bool IsMyTurn {
        get {
            string whosturn = PhotonNetwork.room.GetWhoIsTurn();
            return ((whosturn == PhotonNetwork.player.UserId) && (whosturn != userIDForSimulation_ai));
        }
    }

    public OnAnimalDamagedDelegate OnAnimalDamaged = delegate {};
    public OnSelectedAnimalDelegate OnSelectedAnimal = delegate {};
    public OnChangedPlayerTurnDelegate OnChangedPlayerTurn = delegate {};
    public OnAnimalDiedDelegate OnAnimalDiedDel = delegate {};
    public OnAnimalLevitatedDelegated OnAnimalLevitated = delegate {};
    public OnAnimalGoredDelegated OnAnimalGored = delegate {};
    public OnAnimalBittenDelegated OnAnimalBitten = delegate {};
    public OnAnimalBlockedDelegated OnAnimalBlocked = delegate {};
    public OnSetTargetAnimalDelegate OnSetTargetAnimal = delegate {};

    public bool IsCanClick { get { return IsMyTurn; } }

    /// <summary>
    ///  가장 이상적인 플레이 턴은 1점을 할 수 있는 플레이 턴 + 2점까지 얻기는 빠듯한 턴.
	///  2점까지 안정적으로 얻을 수 있다면 게임이 루즈해질 가능성이 있다.
    ///  사실 패색은 1점이 먹혔을 때 드러나는 경우가 많다.
    ///  거진 1점 먹히고 약간 발버둥치다 의지를 잃는다.딱 그 알맞은 시간이 중요.
    /// </summary>
    public const int LIMIT_GAME_TURN = 30;

    /// <summary>
    ///  지는 쪽에게 희망을 주는 시간. 
    ///  게임의 순간적인 긴장감과 몰입감을 주는 시간이다.
    ///  이기는 쪽이 깃발을 들고 있다면, 추가시간을 주지 않는다.
    ///  반드시 지는 쪽이 깃발을 들고 있거나, 동점인 상황에서 둘 중 하나가 깃발을 들고 있을 때만 발동된다.
    /// </summary>
    [System.NonSerialized]
    public bool isOverTime;
    [System.NonSerialized]
    public bool isUsingSkill =false;

    Animal _selectedAnimal;
    public Animal SelectedAnimal
    {
        get { return _selectedAnimal; }
        set
        {
            _selectedAnimal = value;
/*
            if (IsTrackedCoroutineRunning("AutoSelectedAnimalCoroutine"))
                skipAutoSelectedAnimalCoroutine = true;
 */
            OnSelectedAnimal(value);
        }
    }

    Animal _targetAnimal;
    public Animal targetAnimal
    {
        get { return _targetAnimal; }
        set
        {
            _targetAnimal = value;

            OnSetTargetAnimal(value);
        }
    }
    
    Tile _clickedTile;
    public Tile ClickedTile
    {
        get { return _clickedTile; }
        set { _clickedTile = value; }
    }
    [System.NonSerialized]
    public List<Hunter> allHunters = new List<Hunter>();
    [System.NonSerialized]
    public List<Animal> allAnimals = new List<Animal>();
    [System.NonSerialized]
    public List<Animal> myAnimals = new List<Animal>();
    [System.NonSerialized]
    List<Animal> _enemyAnimals = new List<Animal>();
    public List<Animal> enemyAnimals 
    { 
        get 
        {
            if (PhotonNetwork.offlineMode)
            {
                _enemyAnimals.Clear();
                
                if (PhotonNetwork.player.UserId == userIDForSimulation_origin)
                {
                    _enemyAnimals.AddRange(animals2ForSimulation);
                    _enemyAnimals.AddRange(huntersForSimulation);
                }
                else
                {
                    _enemyAnimals.AddRange(animals1ForSimulation);
                }
            }
            
            return _enemyAnimals;
        }
    }


    /// 공통적으로 사용하는 사운드 모아 넣기
    [SerializeField]
    public AudioClip clockSound;
    [SerializeField]
    public AudioClip movingSound_water;
    [SerializeField]
    public AudioClip movingSound_forest;
    [SerializeField]
    public AudioClip waterSplashSound;
    [SerializeField]
    public AudioClip waddleInGrassSound;
    [SerializeField]
    public AudioClip hopInGrassSound;
    [SerializeField]
    public AudioClip[] attackSound_graze;
    [SerializeField]
    public AudioClip[] attackSound_hit;
    [SerializeField]
    public AudioClip[] attackSound_critical;
    [SerializeField]
    public AudioClip missSound;
    [SerializeField]
    public AudioClip fallSound;
    [SerializeField]
    public AudioClip fallDownSound;
    [SerializeField]
    public AudioClip hpReducingSound;
    [SerializeField]
    public AudioClip concealSound;
    [SerializeField]
    public AudioClip unconcealSound;
    [SerializeField]
    public AudioClip surprisedSound;
    [SerializeField]
    public AudioClip surprisedSound2;
    [SerializeField]
    public AudioClip submergeSound;
    [SerializeField]
    public AudioClip unsubmergedSound;
    [SerializeField]
    public AudioClip climbTreeSound;
    [SerializeField]
    public AudioClip unclimbTreeSound;
    [SerializeField]
    public AudioClip turnSound_enemy;
    [SerializeField]
    public AudioClip turnSound_player;
    [SerializeField]
    public ParticleSystem[] rippleEffects;
    [SerializeField]
     public ParticleSystem splashParticle;
    [SerializeField]
    public ParticleSystem[] sandWalkingDust;
    [SerializeField]
    public List<GameObject> stunFXList;

    /// 오프라인 모드 시뮬레이션을 위한 변수들 
    /// 한명의 턴이 끝나면 PhotonPlayer의 userID를 바꾸는 식이다
    [System.NonSerialized]
    public List<Animal> animals1ForSimulation = new List<Animal>();
    [System.NonSerialized]
    public List<Animal> animals2ForSimulation = new List<Animal>();
    [System.NonSerialized]
    public List<Animal> huntersForSimulation = new List<Animal>();

    [System.NonSerialized]
    public string userIDForSimulation_origin = "Shayne";
    [System.NonSerialized]
    public string userIDForSimulation_another = "SAYERY";
    [System.NonSerialized]
    public string userIDForSimulation_ai = "Ai userID";
    
    /// 유저 타일 시야
    public Dictionary<string, List<Tile>> sightOfUser = new Dictionary<string, List<Tile>>();

    List<Animal> _enemyListInMySight = new List<Animal>();
    public List<Animal> enemyListInMySight { get { RefreshEnemyListInMySight(); return _enemyListInMySight; } }

    /// 깃발 및 스코어
    public List<Flag> flags = new List<Flag>();
    public Dictionary<string, int> score = new Dictionary<string, int>();

    /// AI 테스트용 변수
    [SerializeField]
    public bool isOnAI = false;
    [SerializeField]
    bool AI_firstTurn = false;
    [SerializeField]
    public bool showSightOfAI = false;
    [SerializeField]
    bool isOnStartDirecting = true;

    /// Hunter에 쓰일 총알 관리 
    [SerializeField]
    public Bullet[] bullets;
    [SerializeField]
    public float bulletSpeed = 500f;   /// 1 초당 총알 속도

    /// 로컬라이징 기본 언어 
    [SerializeField]
    string localizingLanguageCode = "ko";

    [SerializeField]
    Camera battleCamera;
    [SerializeField]
    Camera lobbyCamera;

    [SerializeField]
    int turnDuration = 15;

    /// 내가 플레이어들의 애니멀들을 생성했는지 안했는지 체크하기 위한 변수 
    /// Key : playerUserId
    public Dictionary<string, bool> isCreatedPlayersAnimals = new Dictionary<string, bool>();

    public static readonly string PLAYERS_INDEX_PROP_KEY = "playersIndex";
    public static readonly string HUNTERS_PROP_KEY = "hunterKey";
    /// OnCustomProperties에서 누가 애니멀을 보내왔는지 간단하게 체크하기 위해 있는 키
    /// 이 값은 단순 체크용이기 때문에, 이 값을 이용해서 무엇을 하면 안됨
    public static readonly string WHO_DID_SET_ANIMALS_PROP_KEY = "whoDidSetAnimals";

    public enum RoomState
    {
        ROOM_STATE_WAITING_FOR_START,
        ROOM_STATE_CREATING_ENVIRONMENT,
        ROOM_STATE_GAMING,
        ROOM_STATE_PAUSE
    }


    /// Use this for initialization
    void Awake()
    {
        instance = this;

        GDEDataManager.Init("gde_data");

        TileChunk.instance.OnCreatedTiles += OnCreatedTilesSetCustomPropertiesWithPlayerInfo;
        Application.runInBackground = true;

    }


    void Start()
    {
        // TurnManagerListener를 등록해서 TurnManager의 이벤트를 받는다
        turnManager = gameObject.AddComponent<PunTurnManager>();
        turnManager.TurnManagerListener = this;
        turnManager.TurnDuration = turnDuration;// 턴 시간 적당히 주자 

        UIManager.instance.turnManager = GetComponent<PunTurnManager>();
        UIManager.instance.RefreshUIViews();
        dayAndNight = FindObjectOfType<DayAndNight>();

        TileChunk.instance.OnClickedTile += OnClickedTile;

        I2.Loc.LocalizationManager.CurrentLanguageCode = localizingLanguageCode;

        InitParticleFXRenderQueue();
    }


    void Init()
    {
        SelectedAnimal = null;

        targetAnimal = null;

        UIManager.instance.endTurnButton.ResetDefaultColor();

        currentPlayer1ActionCount = 0;
        currentPlayer2ActionCount = 0;

        /// 애니멀이 죽거나 하면 myAnimals 인덱싱이 바뀌기 때문에 턴 바뀔 때마다 0으로 초기화 해줘야 한다 
        autoSelectAnimalIndex = 0;

        Camera.main.depthTextureMode = DepthTextureMode.None;       
    }

    void InitParticleFXRenderQueue()
    {
        // 매터리얼의 렌더링오더를 바꿀수있는 아직 내가아는 유일한 방법 혹시 제트파이팅이 발생하면 생각해보자.
        // 모든 투명 매터리얼의 기본 renderQueue는 3000이다. 클수록 나중에 렌더링되어서 픽셀에 찍히게됨.
        foreach(var rippleEffect in rippleEffects) {
            if (rippleEffect.GetComponent<Renderer>().material.renderQueue == 3000)
                rippleEffect.GetComponent<Renderer>().material.renderQueue += 2;
        }
        

        if (splashParticle.GetComponent<Renderer>().material.renderQueue == 3000)
        {
            splashParticle.GetComponent<Renderer>().material.renderQueue++;
            foreach (var renderer in splashParticle.GetComponentsInChildren<Renderer>())
            {
                renderer.material.renderQueue++;
            }
        }
    }
    float time1 = 0;
    float time2 = 0;
    float timeForWater = 0;
    float chunkRippleSize = 0;
    void Update()
    {
        time1 += Time.deltaTime;
        time2 += Time.deltaTime;
        timeForWater += Time.deltaTime;
        chunkRippleSize += Time.deltaTime * 200;

        if (chunkRippleSize > 220)
            chunkRippleSize = 10;

        if (time1 > 1)
            time1 = 0;

        if (time2 > 7)
        {
            time2 = 0;            
        }
        else if(time2 < 1)
        {
            Shader.SetGlobalFloat("_Time2", time2);
        }
        else
        {
            Shader.SetGlobalFloat("_Time2", 0);
        }
        Shader.SetGlobalFloat("_Time1", time1);
        Shader.SetGlobalFloat("_Time3", timeForWater);
        Shader.SetGlobalFloat("_ChunkRippleSize", chunkRippleSize);

        if (_clickedTile != null)
            tileIdxDebugUI.text = "tileIdx :" + _clickedTile._tileIndex.ToString() + "  type : " + _clickedTile.type.ToString() ;
        
        //stateDebugUI.text = stateEnum.ToString();
        // 게임매니저에서 자원시스템 디버그를 위해서 RTY버튼을 추가하였다. 
        // R은 비구름을 생성하고 T는 비구름을 제거한다. Y는 현재 타일들의 상태들에 대해서 자원을 처리한다.
        if (PhotonNetwork.isMasterClient)
        {
            if (Input.GetKeyDown(KeyCode.C))
            {
                StartGame();

                /// 턴 시작
                turnManager.BeginTurn();

                /// 턴 차례 지정 
                PhotonNetwork.room.SetWhoIsTurn(PhotonNetwork.playerList[0]);

                PhotonNetwork.room.ChangeRoomState(RoomState.ROOM_STATE_GAMING);
            }
        }

        // for debugging, it's useful to have a few actions tied to keys:
        if (Input.GetKeyUp(KeyCode.L))
        {
            PhotonNetwork.LeaveRoom();
        }
    }


    private void OnCreatedTilesSetCustomPropertiesWithPlayerInfo()
    {
        /// 마스터 클라이언트가 플레이어들의 순서를 저장해놓는다
        /// 나중에 플레이어가 접속이 끊겨 재접속을 하게 되면 이 playerList에서의 순서는 바뀌기 때문  
        if (PhotonNetwork.isMasterClient)
        {
            Hashtable playersIndexProp = new Hashtable();
            Hashtable playersIndex = new Hashtable();
            for (int i = 0; i < PhotonNetwork.playerList.Length; ++i)
            {
                /// 플레이어가 나갔다 들어오면 플레이어 ID가 바뀐다
                /// 나갔다 들어와도 바뀌지 않는 플레이어 닉네임을 키값으로 써야한다
                playersIndex[PhotonNetwork.playerList[i].UserId] = i;
            }
            if (PhotonNetwork.offlineMode)
            {
                playersIndex[userIDForSimulation_another] = 1;    /// 동물 vs 인간을 위해 플레이어2는 삭제한다
                //playersIndex[userIDForSimulation_ai] = 1;// 99;

                /// 시야 등록
                sightOfUser.Add(userIDForSimulation_origin, new List<Tile>());
                sightOfUser.Add(userIDForSimulation_another, new List<Tile>());
                sightOfUser.Add(userIDForSimulation_ai, new List<Tile>());
            }

            /// 타일의 Detect정보가 모든유저의 아이디를 키값으로 딕셔너리로 한다.. 맵이 생성된순간은 모두 false로 추가해준다.
            for (int i = 0; i < TileChunk.instance.tileSizeHeight; i++)
            {
                for (int j = 0; j < TileChunk.instance.tileSizeWidth; j++)
                {
                    TileChunk.instance.tiles[i, j].isDetected.Add(userIDForSimulation_origin, false);
                    TileChunk.instance.tiles[i, j].isDetected.Add(userIDForSimulation_another, false);
                }
            }
            playersIndexProp[PLAYERS_INDEX_PROP_KEY] = playersIndex;


            /// 카메라 기본 넥서스 위치로 이동 시켜놓음
            int nexusTileIndex = GetTileIndexOfPlayerNexus(0);
            int nexusTileIndex_X = nexusTileIndex % TileMaker.instance.tileSize;
            int nexusTileIndex_Y = nexusTileIndex / TileMaker.instance.tileSize;
            Tile tile = TileChunk.instance.tiles[nexusTileIndex_X, nexusTileIndex_Y];
            Camera.main.MoveTo(tile.position, true);


            /// 깃발 셋업
            int originFlagPositionTileIndex_originPlayer = GetTileIndexOfPlayerNexus(0);
            int originFlagPositionTileIndex_originPlayer_X = originFlagPositionTileIndex_originPlayer % TileMaker.instance.tileSize;
            int originFlagPositionTileIndex_originPlayer_Y = originFlagPositionTileIndex_originPlayer / TileMaker.instance.tileSize;
            
            int originFlagPositionTileIndex_anotherPlayer = GetTileIndexOfPlayerNexus(1);
            int originFlagPositionTileIndex_anotherPlayer_X = originFlagPositionTileIndex_anotherPlayer % TileMaker.instance.tileSize;
            int originFlagPositionTileIndex_anotherPlayer_Y = originFlagPositionTileIndex_anotherPlayer / TileMaker.instance.tileSize;

            Tile originFlagPositionTile_originPlayer = TileChunk.instance.tiles[originFlagPositionTileIndex_originPlayer_X, originFlagPositionTileIndex_originPlayer_Y];
            Tile originFlagPositionTile_anotherPlayer = TileChunk.instance.tiles[originFlagPositionTileIndex_anotherPlayer_X, originFlagPositionTileIndex_anotherPlayer_Y];

            originFlagPositionTile_originPlayer.originFlagTileOwnerUserId = userIDForSimulation_origin;
            originFlagPositionTile_anotherPlayer.originFlagTileOwnerUserId = userIDForSimulation_another;
            originFlagPositionTile_originPlayer.isOriginFlagTile = true;
            //originFlagPositionTile_originPlayer.isRock = false;
            originFlagPositionTile_anotherPlayer.isOriginFlagTile = true;
            //originFlagPositionTile_anotherPlayer.isRock = false;

            /// isRock일 때 다익스트라 갱신을 다시 해줘야한다 
            TileChunk.instance.ChnangeTileOccupation(originFlagPositionTile_originPlayer._tileIndex, false);
            TileChunk.instance.ChnangeTileOccupation(originFlagPositionTile_anotherPlayer._tileIndex, false);

            flags[0].ownerUserId = userIDForSimulation_origin;
            flags[1].ownerUserId = userIDForSimulation_another;
            flags[0].originPositionTile = originFlagPositionTile_originPlayer;
            flags[1].originPositionTile = originFlagPositionTile_anotherPlayer;
            flags[0].originPositionTile_enemy = originFlagPositionTile_anotherPlayer;
            flags[1].originPositionTile_enemy = originFlagPositionTile_originPlayer;

            score.Add(userIDForSimulation_origin, 0);
            score.Add(userIDForSimulation_another, 0);

            PhotonNetwork.room.SetCustomProperties(playersIndexProp);
        }
    }

    // 마스터클라이언트만 호출하는 함수이다.
    // Data에서 사냥꾼의 정보를 읽어 프로퍼티를 셋한다.
    void SetCustomPropertiesHunters()
    {
        if (!PhotonNetwork.room.CustomProperties.ContainsKey(PLAYERS_INDEX_PROP_KEY))
        {
            return;
        }
        /// 기지가 애니멀보다 먼저 세팅되어 있다는 보장이 되어있어야 함 
        Hashtable playersIndex = (Hashtable)PhotonNetwork.room.CustomProperties[PLAYERS_INDEX_PROP_KEY];

        // 자신의 userID를 사용하게되어있지만 제 3자의 id(userIDForSimulation_ai)를 이용해야함. 물론 호출은 master클라이언트    
        // 현재는 마스터클라이언트의 넥서스 주위에 사냥꾼이 생성될 것이다.
        int playerIndex = (int)playersIndex[userIDForSimulation_ai];
        int playerNexusTileIndex = GetTileIndexOfPlayerNexus(playerIndex);


        /// 테스트 플레이어 데이타들 중에서 랜덤으로 하나 뽑아오기 
        GDEPlayerData playersData = new GDEPlayerData(GDEItemKeys.Player_AI_Hunter);
        List<GDEAnimalData> huntersData = playersData.animals;

        /// 넥서스 주위 8칸 타일이 기본 스폰 타일이고, 동물의 수가 그것보다 많으면 좀 더 늘린다 
        List<int> emptyTileList = GetEmptyTileListBy(playerNexusTileIndex, huntersData.Count > 8 ? 8 + 12 : 8);

        Hashtable hashHuntersWithLocation = new Hashtable();
        foreach (var hunterData in huntersData)
        {
            int randomTileIndex = emptyTileList.Random();
            hashHuntersWithLocation.Add(randomTileIndex, hunterData);
            emptyTileList.Remove(randomTileIndex);
        }

        SetCustomProperiesWithNewHunterList(hashHuntersWithLocation);
    }

    [SerializeField]
    GameObject stunFxPool;
    [SerializeField]
    GameObject stunFX;

    public GameObject GetStunFx()
    {
        for(int i = 0; i < stunFXList.Count; i++)
        {
            // 스턴 fx풀중에 사용중이지 않은녀석을 찾아서 반환한다.
            if (!stunFXList[i].gameObject.GetActive())
            {
                return stunFXList[i];
            }
        }
        // 여기까지왔다면 풀의 모든 fx가 사용중이란소리기때문에 새로만들고 리스트에추가하고 걔를 반환한다
        GameObject newStunFX = stunFxPool.AddChild(stunFX);
        stunFXList.Add(newStunFX);

        return newStunFX;
    }
   
    /// 나의 애니멀 정보 커스텀 프로퍼티에 입력
    GDEPlayersData playersData;
    Hashtable playersIndex;
    void SetCustomPropertiesMyAnimals()
	{	
		if(!PhotonNetwork.room.CustomProperties.ContainsKey(PLAYERS_INDEX_PROP_KEY))
		{
			return;
		}

        /// 기지가 애니멀보다 먼저 세팅되어 있다는 보장이 되어있어야 함 
        playersIndex = (Hashtable)PhotonNetwork.room.CustomProperties[PLAYERS_INDEX_PROP_KEY];
        int playerIndex = (int)playersIndex[PhotonNetwork.player.UserId];
        int playerNexusTileIndex = GetTileIndexOfPlayerNexus(playerIndex);

        /// 테스트 플레이어 데이타들 중에서 랜덤으로 하나 뽑아오기 
        playersData = new GDEPlayersData(GDEItemKeys.Players_TestPlayers);
        List<GDEAnimalData> myAnimalsData = playersData.Players[playerIndex].animals;

        List<int> emptyTileList = GetEmptyTileListBy(playerNexusTileIndex, myAnimalsData.Count > 8 ? 8 + 12 : 8);

        Hashtable hashAnimalWithLocation = new Hashtable();
        foreach (var animalData in myAnimalsData) {
            /// 랜덤 위치 타일 얻어오기 
            int randomTileIndex = emptyTileList.Random();
            hashAnimalWithLocation.Add(randomTileIndex, animalData);
            emptyTileList.Remove(randomTileIndex);
        }

        SetCustomProperiesWithNewAnimalList(hashAnimalWithLocation);
    }


    [SerializeField]
    int nexusDistanceFromCenterLine = 0;
    public int GetTileIndexOfPlayerNexus(int playerIndex)
    {
        switch (playerIndex)
        {
        case 0:
            return (TileMaker.instance.tileSize * TileMaker.instance.tileSize - TileMaker.instance.tileSize / 2) - (TileMaker.instance.tileSize * (TileMaker.instance.tileSize/2 - nexusDistanceFromCenterLine));
        case 1:
            return (TileMaker.instance.tileSize / 2) + (TileMaker.instance.tileSize * (TileMaker.instance.tileSize/2 - nexusDistanceFromCenterLine));
            case 99:
                return (TileMaker.instance.tileSize / 2) + (TileMaker.instance.tileSize * (TileMaker.instance.tileSize / 2));
            default:
                return 0;
        }
    }
    

    // 매개변수 해쉬테이블에 생성되어야 할 사냥꾼의 위치, 사냥꾼의능력치가 다 담겨있음.
    public void SetCustomProperiesWithNewHunterList(Hashtable hashHunterWithLocation)
    {
        Hashtable huntersProp = new Hashtable();

        foreach (var key in hashHunterWithLocation.Keys)
        {
            GDEAnimalData hunterData = (GDEAnimalData)hashHunterWithLocation[key];
            int hunterViewID = PhotonNetwork.AllocateViewID();
            Hashtable hunterProp = SetHunterProp(hunterData, (int)key, hunterViewID);
            string animalPropKey = Animal.GetAnimalPropKey(PhotonNetwork.player.UserId, hunterViewID);
            huntersProp[animalPropKey] = hunterProp;
        }
        /// 프로퍼티에 입력
        huntersProp[HUNTERS_PROP_KEY] = PhotonNetwork.player.UserId;
        PhotonNetwork.room.SetCustomProperties(huntersProp);
    }

    public void SetCustomProperiesWithNewAnimalList(Hashtable hashAnimalWithLocation)
    {
        Hashtable myAnimalsProp = new Hashtable();

        foreach (var key in hashAnimalWithLocation.Keys)
        {
            GDEAnimalData animalData = (GDEAnimalData)hashAnimalWithLocation[key];
            int animalViewID = PhotonNetwork.AllocateViewID();
            Hashtable animalProp = SetAnimalProp(animalData, (int)key, animalViewID);
            string animalPropKey = Animal.GetAnimalPropKey(PhotonNetwork.player.UserId, animalViewID);
            myAnimalsProp[animalPropKey] = animalProp;
        }
        /// 프로퍼티에 입력
        myAnimalsProp[WHO_DID_SET_ANIMALS_PROP_KEY] = PhotonNetwork.player.UserId;
        PhotonNetwork.room.SetCustomProperties(myAnimalsProp);
    }

    Hashtable SetHunterProp(GDEAnimalData hunterData, int tileIndex, int animalViewID)
    {
        Hashtable tempHunterProp = new Hashtable();
        string animalKey = hunterData.Key;
        tempHunterProp[Animal.ANIMAL_DATA_KEY_PROP_KEY] = animalKey;
        tempHunterProp[Animal.ANIMAL_OWNED_TILE_INDEX_PROP_KEY] = tileIndex;
        tempHunterProp[Animal.ANIMAL_VIEW_ID_PROP_KEY] = animalViewID;
        tempHunterProp[Animal.ANIMAL_OWNER_USER_ID_PROP_KEY] = PhotonNetwork.player.UserId;

        return tempHunterProp;
    }

    Hashtable SetAnimalProp(GDEAnimalData animalData, int tileIndex, int animalViewID)
    {
        Hashtable tempAnimalProp = new Hashtable();
        string animalKey = animalData.Key;
        tempAnimalProp[Animal.ANIMAL_DATA_KEY_PROP_KEY] = animalKey;
        tempAnimalProp[Animal.ANIMAL_OWNED_TILE_INDEX_PROP_KEY] = tileIndex;
        tempAnimalProp[Animal.ANIMAL_VIEW_ID_PROP_KEY] = animalViewID;
        tempAnimalProp[Animal.ANIMAL_OWNER_USER_ID_PROP_KEY] = PhotonNetwork.player.UserId;

        return tempAnimalProp;
    }

   

	public Animal CreateAnimal(string dataKey, int ownedTileIndex, int photonViewID, Hashtable animalProp)
	{
		/// 데이타 
		GDEAnimalData animalData = new GDEAnimalData(dataKey);
		
		/// 위치 타일 얻어오기 
		Tile ownedTile = TileChunk.instance.tiles[ownedTileIndex%TileMaker.instance.tileSize, ownedTileIndex/TileMaker.instance.tileSize];

        /// 동물 생성
        Animal newAnimal = Instantiate(Resources.Load("Animals/" + animalData.prefabName, typeof(Animal)), ownedTile.position, Quaternion.identity) as Animal;

		/// Photon View ID
		newAnimal.GetComponent<PhotonView>().viewID = photonViewID;

		/// 초기화
		newAnimal.Init(animalProp);

		return newAnimal;
    }

	public Hunter CreateHunter(string dataKey, int ownedTileIndex, int photonViewID, Hashtable hunterProp)
    {
        /// 데이타 
        GDEAnimalData hunterData = new GDEAnimalData(dataKey);

        /// 위치 타일 얻어오기 
        Tile ownedTile = TileChunk.instance.tiles[ownedTileIndex % TileMaker.instance.tileSize, ownedTileIndex / TileMaker.instance.tileSize];

        /// 사냥꾼 생성
        Hunter newHunter = Instantiate(Resources.Load("Hunters/" + hunterData.prefabName, typeof(Hunter)), ownedTile.position, Quaternion.identity) as Hunter;

        /// Photon View ID
        newHunter.GetComponent<PhotonView>().viewID = photonViewID;

        /// 초기화
        newHunter.Init(hunterProp);

        return newHunter;
    }


    List<int> usedTileIndexes = new List<int>();
    List<int> GetEmptyTileListBy(int centerTileIndex, int capacity)
    {
        List<int> emptyTileList = new List<int>();

        int centerTileIndex_X = centerTileIndex % TileMaker.instance.tileSize;
        int centerTileIndex_Y = centerTileIndex / TileMaker.instance.tileSize;


        int curSearchingRange = 0;

        while (emptyTileList.Count < capacity)
        {
            ++curSearchingRange;

            for (int x = -curSearchingRange; x <= curSearchingRange; ++x)
            {
                for (int y = -curSearchingRange; y <= curSearchingRange; ++y)
                {
                    /// 넘어온 CenterTileIndex를 기준으로 주위를 점점 넓혀가며 빈 자리를 찾기 위함
                    if (Mathf.Abs(x) != curSearchingRange && Mathf.Abs(y) != curSearchingRange) continue;

                    int emptyTileIndex = (centerTileIndex_X + x) + (centerTileIndex_Y + y) * TileMaker.instance.tileSize;
                    if (!usedTileIndexes.Contains(emptyTileIndex))
                    {
                        /// 해당 타일 인덱스가 비어있거나 비어있지 않아도 usedTileIndexes에 넣는다 
                        /// 비어있는 타일이면 그 타일 인덱스를 사용할 것이기 때문이고
                        /// 비어있는 타일이 아니라도 다음에 이 함수를 호출할 때, 그 타일을 검색에서 제외시키기 위해서다 
                        /// * 만약 동적으로 타일의 상태가 바뀌게 되는 시스템이 생긴다면 usedTileIndexes는 Refresh되야 한다
                        usedTileIndexes.Add(emptyTileIndex);

                        /// 해당 타일이 벽 타일인지 체크
                        /// 주의* 애니멀 생성이 타일 생성보다 늦게 되기 때문에 이런 코드가 가능한 것이다
                        Tile tile = TileChunk.instance.GetTile(centerTileIndex_X + x, centerTileIndex_Y + y);
                        if (tile != TileChunk.instance.tempTile && !tile.isRock && !tile.isOriginFlagTile)
                        {
                            emptyTileList.Add(emptyTileIndex);
                        }
                    }

                    if (emptyTileList.Count >= capacity) break;
                }

                if (emptyTileList.Count >= capacity) break;
            }
        }

        return emptyTileList;
    }


    void SetCameraToNexus()
    {
        Hashtable playersIndex = (Hashtable)PhotonNetwork.room.CustomProperties[PLAYERS_INDEX_PROP_KEY];
        int playerIndex = (int)playersIndex[PhotonNetwork.player.UserId];
        int playerNexusTileIndex = GetTileIndexOfPlayerNexus(playerIndex);
        Vector3 playerCenterTilePos = TileChunk.instance.tiles[playerNexusTileIndex % TileMaker.instance.tileSize, playerNexusTileIndex / TileMaker.instance.tileSize].position;

        Camera.main.MoveTo(playerCenterTilePos);
    }

    Hunter CreateHunterByHash(Hashtable hunterTable)
    {
        /// 애니멀 데이타 키
        string hunterKey = (string)hunterTable[Animal.ANIMAL_DATA_KEY_PROP_KEY];

        /// 위치 타일 인덱스
        int tileIndex = (int)hunterTable[Animal.ANIMAL_OWNED_TILE_INDEX_PROP_KEY];

        /// Photon View ID
        int hunterPhotonViewID = (int)hunterTable[Animal.ANIMAL_VIEW_ID_PROP_KEY];

        return CreateHunter(hunterKey, tileIndex, hunterPhotonViewID, hunterTable);
    }

    Animal CreateAnimalByHash(Hashtable animalTable)
    {
        /// 애니멀 데이타 키
        string animalKey = (string)animalTable[Animal.ANIMAL_DATA_KEY_PROP_KEY];

        /// 위치 타일 인덱스
        int tileIndex = (int)animalTable[Animal.ANIMAL_OWNED_TILE_INDEX_PROP_KEY];

        /// Photon View ID
        int animalPhotonViewID = (int)animalTable[Animal.ANIMAL_VIEW_ID_PROP_KEY];

        return CreateAnimal(animalKey, tileIndex, animalPhotonViewID, animalTable);
    }

    void SyncHuntersByUsingCustomProperties(Hashtable customProperty)
    {
        // 기지 셋업이 안되어있다면 종료
        if (!PhotonNetwork.room.CustomProperties.ContainsKey(PLAYERS_INDEX_PROP_KEY))
        {
            return;
        }

        string userID = (string)customProperty[HUNTERS_PROP_KEY];
        string expectedHutnerPropKey = Animal.GetAnimalPropKey(userID);
        Hashtable tempHunterProp = new Hashtable(); 

        GameObject parentObject = null;

        foreach (var tempCustomProperty in customProperty)
        {
            string hunterPropKey = (string)tempCustomProperty.Key;
            if (hunterPropKey.Contains(expectedHutnerPropKey))
            {
                tempHunterProp = (Hashtable)customProperty[hunterPropKey];
                bool isExisting = false;
                foreach (var hunter in allHunters)
                {
                    if (hunter.photonView.viewID == (int)tempHunterProp[Animal.ANIMAL_VIEW_ID_PROP_KEY])
                    {
                        isExisting = true;
                        break;
                    }
                }
                if (!isExisting)
                {
                    // 사냥꾼생성
                    Hunter newHunter = CreateHunterByHash((Hashtable)tempCustomProperty.Value);
                    allHunters.Add(newHunter);

                    if (parentObject == null)
                        parentObject = new GameObject(userID + " of AI");

                    newHunter.transform.SetParent(parentObject.transform);

                    if (((Hashtable)tempCustomProperty.Value)[Animal.ANIMAL_OWNER_USER_ID_PROP_KEY].ToString() == PhotonNetwork.player.UserId)
                    {
                        //allHunters.Add(newHunter);
                        newHunter.photonView.TransferOwnership(8888);
                    }
                }
            }
        }

        /// 동물 하나라도 생성했다면, 해당 플레이어의 동물들을 생성 완료 했다고 간주
        if (!isCreatedPlayersAnimals.ContainsKey(userID))
        {
            isCreatedPlayersAnimals.Add(userID, true);
        }

        TileChunk.instance.RefreshFieldOfView();
    }

    void SyncAnimalsByUsingCustomProperties(Hashtable customProperty)
	{
		// 기지 셋업이 안되어있다면 종료
		if(!PhotonNetwork.room.CustomProperties.ContainsKey(PLAYERS_INDEX_PROP_KEY))
		{
			return;
		}

        string userID = (string)customProperty[WHO_DID_SET_ANIMALS_PROP_KEY];
        string expectedAnimalPropKey = Animal.GetAnimalPropKey(userID);
        Hashtable tempAnimalProp = new Hashtable();

        GameObject parentObject = null;

        foreach (var tempCustomProperty in customProperty)
        {
            string animalPropKey = (string)tempCustomProperty.Key;
            if (animalPropKey.Contains(expectedAnimalPropKey))
            {
                tempAnimalProp = (Hashtable)customProperty[animalPropKey];  // 여기서 지금 animalPropKey가 쌩뚱맞은게 들어왔단의미이다.
                bool isExisting = false;
                foreach (var animal in allAnimals)
                {
                    if (animal.photonView.viewID == (int)tempAnimalProp[Animal.ANIMAL_VIEW_ID_PROP_KEY])
                    {
                        isExisting = true;
                        break;
                    }
                }
                if (!isExisting)
                {
                    Animal newAnimal = CreateAnimalByHash((Hashtable)tempCustomProperty.Value);
                    allAnimals.Add(newAnimal);

                    TileChunk.instance.absoluteSightTileTable.Add(newAnimal, new List<Tile>());

                    if (parentObject == null)
                        parentObject = new GameObject(userID + "'s Animals");

                    newAnimal.transform.SetParent(parentObject.transform);

                    if (((Hashtable)tempCustomProperty.Value)[Animal.ANIMAL_OWNER_USER_ID_PROP_KEY].ToString() == PhotonNetwork.player.UserId)
                    {
                        myAnimals.Add(newAnimal);
                    }
                }
            }           
        }

        /// 동물 하나라도 생성했다면, 해당 플레이어의 동물들을 생성 완료 했다고 간주
        if (!isCreatedPlayersAnimals.ContainsKey(userID))
        {
            isCreatedPlayersAnimals.Add(userID, true);
        }

        TileChunk.instance.RefreshFieldOfView();
    }

    public void StopSelectionHighlight()
    {
        // 현재 time 을 0으로 하면 trainRenderer의 자국이 사라짐, 1로하면 생김
        TrailRenderer[] trailRenderers = selectionHighlight_blue.GetComponentsInChildren<TrailRenderer>();
        for (int i = 0; i < trailRenderers.Length; i++)
        {
            trailRenderers[i].time = 0;
        }
        
        // 현재 time 을 0으로 하면 trainRenderer의 자국이 사라짐, 1로하면 생김
        trailRenderers = selectionHighlight_red.GetComponentsInChildren<TrailRenderer>();
        for (int i = 0; i < trailRenderers.Length; i++)
        {
            trailRenderers[i].time = 0;
        }
    }

    /// 타일을 클릭했을 때 들어옴
    /// 이동범위 밖의 타일을 클릭해도 UI가 뜨는데 이건 지금 수정안하겠다. 어짜피 미완성기능이기때문에
    /// TODO: 타일클릭에서 뜨는 UI의 모든 경우의수를 다뤄야한다. (혹시모를 스킬같은)
	void OnClickedTile(Tile tile)
	{
		Debug.Log("OnClickWith : " + tile._tileIndex.ToString());

        ClickedTile = tile;

        targetAnimal = null;

        /// 타일 위에 동물이 있다면, 동물 선택으로 바꿔주기, 
        /// 동물이 있더라도 그 동물이 visible해야 클릭가능(은신일때 클릭가능하면안됨)
        if (tile.owningAnimal != null && tile.isVisible && tile.owningAnimal.isVisible)
        {
            OnClickWith(tile.owningAnimal);
            
            return;
        }
    }


    /// 내 턴일 때만 애니멀 클래스에서 호출한다
    public void OnClickWith(Animal animal)
    {
        Debug.Log("OnClickWith : " + animal.animalData.SchemaName());

        UIManager.instance.OnClickWith(animal);

        /// 새로 클릭된 동물이 내 동물이라면
        if (animal.photonView.isMine)
        {
            SelectedAnimal = animal;
        }
        /// 새로 클릭된 동물이 내 동물이 아니라면
        else
        {
            targetAnimal = animal;
        }
    }


    public void OnClickBattleButton()
    {
        string whistle = WhistleManager.instance.GetWhistle();

        WhistleManager.instance.OpenWhistle(whistle);
    }


    void StartGame()
    {
        // 마스터 클라이언트가 시작 사인을 한다 
        if (PhotonNetwork.isMasterClient)
        {
            TileMaker.instance.GenerateTileObjects();
        }
    }

    
    // 이 함수는 나의 턴일 때만 버튼을 누를수 있도록 설정됨
    public void EndMyTurn()
    {
        Init();

        /// CustomPoperties 동기화 속도는 RaiseEvent 보다 느리다 
        /// SetPlayerFinishedTurn을 OnEvent보다 먼저하지만, CustomPoperties 동기화가 느려서 OnEvent 처리에 문제가 생김
        ForceFinishThisPlayerTurn();
    }
    

    void CheckIfCanSwitchRoomState()
    {
        if ((PhotonNetwork.room.PlayerCount == 2 && PhotonNetwork.isMasterClient) || PhotonNetwork.offlineMode)
        {
            switch (PhotonNetwork.room.GetRoomState())
            {
                case RoomState.ROOM_STATE_WAITING_FOR_START:
                    {
                        /// GenerateTileObjects 보다 먼저 해줘야 함 
                        /// GenerateTileObjects 처리 중에 SetCustomProperties()가 호출되고 CheckIfCanSwitchRoomState가 다시 호출되면서 무한루프에 빠질 수 있음
                        PhotonNetwork.room.ChangeRoomState(RoomState.ROOM_STATE_CREATING_ENVIRONMENT);

                        /// 카메라 스위칭
                        battleCamera.gameObject.SetActive(true);
                        lobbyCamera.gameObject.SetActive(false);

                        StartGame();

                        break;
                    }
                case RoomState.ROOM_STATE_CREATING_ENVIRONMENT:
                    {
                        /// 게임 시작 후, 애니멀 동기화가 끝나면 턴을 시작함 
                        bool isAllCreatedPlayersAnimals = true;
                        
                        Hashtable playersIndex = (Hashtable)PhotonNetwork.room.CustomProperties[PLAYERS_INDEX_PROP_KEY];
                        foreach (var playerIndex in playersIndex)
                        {
                            string playerUserID = (string)playerIndex.Key;
                            bool isCreated;
                            isCreatedPlayersAnimals.TryGetValue(playerUserID, out isCreated);
                            if (!isCreated)
                            {
                                isAllCreatedPlayersAnimals = false;
                                break;
                            }
                        }

                        if (isAllCreatedPlayersAnimals)
                        {
                            /// BGM
                            BGMPlayer.instance.Play(BGMPlayer.BGM.BATTLE);
                            
                            /// 위에 위치한 플레이어 동물 회전시키기 
                            /// * 야매 코드 위치
                            Quaternion rotation = Quaternion.Euler(0, 180, 0);

                            foreach (var animals2 in animals2ForSimulation)
                            {
                                if (animals2.ownedTile.type == TileMaker.TileType.RIVER)
                                {
                                    rotation.eulerAngles += Quaternion.Euler(animals2.xAngle_fallingIntoWater, 0, 0).eulerAngles;
                                }
                                animals2.transform.rotation = rotation;
                                rotation = Quaternion.Euler(0, 180, 0);
                            }

                            foreach (var hunter in huntersForSimulation)
                            {
                                if (hunter.ownedTile.type == TileMaker.TileType.RIVER)
                                {
                                    rotation.eulerAngles += Quaternion.Euler(hunter.xAngle_fallingIntoWater, 0, 0).eulerAngles;
                                }
                                hunter.transform.rotation = rotation;
                                rotation = Quaternion.Euler(0, 180, 0);
                            }

                            StartCoroutine(StartDirecting());
                        }

                        break;
                    }
                case RoomState.ROOM_STATE_PAUSE:
                    {
                        turnManager.Resume();

                        PhotonNetwork.room.ChangeRoomState(RoomState.ROOM_STATE_GAMING);

                        break;
                    }
            }
        }
        else
        {
            Debug.Log("Waiting for another player");
        }
    }


    float cameraPanningTimeForStartDirecting = 2f;
    IEnumerator StartDirecting()
    {
        if (isOnStartDirecting && !AI_firstTurn)
        {
            /// 카메라 느린 속도로 적의 넥서스로 패닝, 
            int enemyNexusTileIndex = GetTileIndexOfPlayerNexus(1);
            int enemyNexusTileIndex_X = enemyNexusTileIndex % TileMaker.instance.tileSize;
            int enemyNexusTileIndex_Y = enemyNexusTileIndex / TileMaker.instance.tileSize;
            Tile enemyNexusTile = TileChunk.instance.tiles[enemyNexusTileIndex_X, enemyNexusTileIndex_Y];
            Camera.main.MoveTo(enemyNexusTile.position, false, cameraPanningTimeForStartDirecting);

            yield return new WaitForSeconds(cameraPanningTimeForStartDirecting);

            
            /// AI 강제로 보여주기 
            foreach (var hunter in huntersForSimulation)
            {
                hunter.SetVisible(true);

                hunter.ownedTile.isVisible = true;
            }

            TileChunk.instance.fogOfWar.RefreshTexture();

			/// 몇초 정도 보여주고
            yield return new WaitForSeconds(2f);

			/// 보여주는거 다시 풀기 
            TileChunk.instance.RefreshFieldOfView();

            foreach (var hunter in huntersForSimulation)
            {
                hunter.SetVisible(hunter.ownedTile.isVisible);
            }

            yield return new WaitForSeconds(1f);


            /// 카메라 느린 속도로 플레이어 넥서스로 패닝 
            int playerNexusTileIndex = GetTileIndexOfPlayerNexus(0);
            int playerNexusTileIndex_X = playerNexusTileIndex % TileMaker.instance.tileSize;
            int playerNexusTileIndex_Y = playerNexusTileIndex / TileMaker.instance.tileSize;
            Tile playerNexusTile = TileChunk.instance.tiles[playerNexusTileIndex_X, playerNexusTileIndex_Y];
            Camera.main.MoveTo(playerNexusTile.position, false, cameraPanningTimeForStartDirecting);

            yield return new WaitForSeconds(cameraPanningTimeForStartDirecting);

            /// 게임 목표 UI 표시
            UIManager.instance.centerHeadTextUI.gameObject.SetActive(true);

            yield return new WaitForSeconds(2f);
        }

        /// 턴 시작
        turnManager.BeginTurn();

        /// 턴 차례 지정 
        PhotonNetwork.room.SetWhoIsTurn(PhotonNetwork.playerList[0]);

        PhotonNetwork.room.ChangeRoomState(RoomState.ROOM_STATE_GAMING);

        /// 자동 턴 종료 코루틴 실행 
        /// *서버 들어가기 전까지는 수동으로 넘긴다. RPC 들어가면 방식을 바꿔야 한다 
        //StartCoroutine(AutoSelectedAnimalCoroutine());
    }


    void ForceFinishThisPlayerTurn()
    {
        string currentTurnPlayerUserId = PhotonNetwork.room.GetWhoIsTurn();
        foreach (var player in PhotonNetwork.playerList)
        {
            if (player.UserId == currentTurnPlayerUserId)
            {
                turnManager.ForceFinishPlayer(player);
                break;
            }
        }
    }


    // 마스터 클라이언트가 호출함.
    void ChangeTurnToNextPlayer()
    {
        if (!PhotonNetwork.room.CustomProperties.ContainsKey(PLAYERS_INDEX_PROP_KEY))
        {
            return;
        }

        /// 쪼매 지저분한 코드들
        /// 기지 세팅을 위해 쓰던 PlayersIndex 프로퍼티를 재사용하기 위함 
        int nextTurnPlayerIndex = 0;

        /// 여기서는 현재 턴 플레이어의 인덱스를 알아와서, 다음 플레이어 인덱스가 무엇이 될지 알아온다 
        string currentTurnPlayerUserId = PhotonNetwork.room.GetWhoIsTurn();
        Hashtable playersIndex = (Hashtable)PhotonNetwork.room.CustomProperties[PLAYERS_INDEX_PROP_KEY];
        foreach (var playerIndex in playersIndex)
        {
            if ((string)playerIndex.Key == currentTurnPlayerUserId)
            {
                int currentTurnPlayerIndex = (int)playerIndex.Value;
                nextTurnPlayerIndex = currentTurnPlayerIndex == playersIndex.Count - 1 ? 0 : currentTurnPlayerIndex + 1;
                break;
            }
        }

        string nextTurnPlayerUserId = PhotonNetwork.player.UserId;
        int i = 0;
        foreach (var playerIndex in playersIndex)
        {
            if (i == nextTurnPlayerIndex)
            {
                nextTurnPlayerUserId = (string)playerIndex.Key;
                break;
            }
            ++i;
        }
        
        /// PlayerList에서 다음 턴 플레이어 UserId와 같은 녀석 찾아서 턴 넘기기 
        foreach (var player in PhotonNetwork.playerList)
        {
            if (player.UserId == nextTurnPlayerUserId)
            {
                PhotonNetwork.room.SetWhoIsTurn(player);
                break;
            }
        }
    }

    [PunRPC]
    IEnumerator DoAI() 
    {
        List<Animal> aiAnimalList = PhotonNetwork.player.UserId == userIDForSimulation_another ? animals2ForSimulation : huntersForSimulation;
        for (int i = 0; i < aiAnimalList.Count;)
        {
            Animal animal = aiAnimalList[i];
            if (animal.ai == null) continue;

            var AICoroutine = animal.ai.DoAI();
            while (AICoroutine.MoveNext()) yield return AICoroutine.Current;

            /// AI가 반격 맞고 죽었다면 리스트에서 빠지기 때문에 ++시키지 않는다 
            if (animal.curHealth > 0) ++i;
        }

        EndMyTurn();
    }

    public void OnSwitchedDayAndNight(DayAndNight.TimeOfNature switchedTime)
    {
        TileChunk.instance.OnSwitchedDayAndNight(switchedTime);

        UIManager.instance.RefreshUIViews();
    }


    public Flag GetFlagWithUserId(string userId)
    {
        foreach (var flag in flags)
        {
            if (flag.ownerUserId == userId)
            {
                return flag;
            }
        }

        return null;
    }


    public void AddScore(string userId)
    {
        score[userId] += 1;

        /// 3점 내기로 하면 빡세다.
        /// 오버워치는 3점 내기이지만, 3점을 내려면 상대팀이 던지거나, 압도적인 실력차일 때만 가능하다. 
        /// 실력이 동등하다고 했을 때 평균적으로 1:0으로 끝난다. 혹은 무승부로 승자결정전까지 간다 
        /// 플레이타임을 좀 더 빨리 끝내고 싶다면 1점 내기로 하는게 좋겠으나,
        /// '역전'이라는 요소도 게임을 재밌게 하므로 딱 적당한 2점 내기를 설정한다.
        const int END_GAME_SCORE = 2;
        if (score[userId] >= END_GAME_SCORE)
        {
            /// 게임 종료 
            if (PhotonNetwork.offlineMode)
            {
                /// player_origin 을 기준으로 승패를 결정한다 
                UIManager.instance.DisplayGameResultSprite(userId == userIDForSimulation_origin ? ResultType.LocalWin : ResultType.LocalLoss);
            }
        }
        else
        {
            CheckToCanProceedOverTime();
        }
        
        UIManager.instance.RefreshUIViews();
    }

/*
    public void SetAutoSelectedAnimal(float delayTimeSec = 1f)
    {
        if (!IsTrackedCoroutineRunning("AutoSelectedAnimalCoroutine")) StartTrackedCoroutine("AutoSelectedAnimalCoroutine", delayTimeSec);
    }
 */

    //bool skipAutoSelectedAnimalCoroutine = false;
    /// 동물들 각자가 행동을 끝마치는 정확한 타이밍에 턴을 넘기기 위해서는 각 행동 각자가 AutoSelectedAnimalCoroutine()을 호출해줘야 한다  
    /// 하지만, 만약 까먹고 AutoSelectedAnimalCoroutine을 호출하지 않는다면 영영 턴을 넘기지 못하는 버그가 생기게 된다 
    /// 그래서 자동 턴 종료 처리를 어떻게 처리해야 할지 생각을 많이 해봤었다 
    /// 가장 실수 유발을 적게 하려면 GameManager에서 턴 종료를 할지 말지 체크하는 코루틴을 돌리는게 좋을 것 같다
    IEnumerator AutoSelectedAnimalCoroutine(/*float delayTimeSec*/)
    {
        //yield return new WaitForSeconds(delayTimeSec);

        while (true)
        {
            bool canCheck = PhotonNetwork.offlineMode ? PhotonNetwork.player.UserId == userIDForSimulation_origin : IsMyTurn;

            /// 내 턴에서 어떤 동물의 행동이 끝났으면 자동으로 다음 동물을 선택시킨다 
            if (canCheck /*&& !skipAutoSelectedAnimalCoroutine*/)
            {
                bool canAnyoneAction = false;
                for(int i = 0; i < myAnimals.Count; i++)
                {
                    if (!myAnimals[i].isFinishedTurnOrder)
                    {/*
                        SelectedAnimal = animal;

                        Vector3 destination = animal.transform.position;

                        Camera.main.MoveTo(destination);
                        */ 
                        canAnyoneAction = true;

                        break;
                    } 
                }

                /// 넘길만한 동물이 없으면 턴을 넘긴다 
                if (!canAnyoneAction)
                {
                    yield return new WaitForSeconds(1f);
                    
                    EndMyTurn();
                }
            }

            yield return new WaitForSeconds(0.5f);
        }

        //skipAutoSelectedAnimalCoroutine = false;
    }
    
    // 동물이 어떤 행위를 했을때 호출하는 함수, 플레이어마다 한턴에 조작할 수 있는 동물의 개수는 정해져있다(animalActionCount). 
    // 기본으로 한마리만 움직일수있도록할 것이지만 perk혹은 동물의 무리 특성을 부여할 가능성도 있기 때문에
    // 플레이어가 움직일 수 있는 동물의 개수만큼 동물을 움직였는지 체크하고 그렇다면 다른 동물을 더이상 조작하지 못하도록 canAction을 false한다.
    int currentPlayer1ActionCount = 0;
    // 이 변수는 턴이 끝날때마다 초기화된다.
    int currentPlayer2ActionCount = 0;
    public void OnAnimalDidAction(Animal animal)
    {
        int actionCount = 0;

        // 해당 동물의 플레이어의 액션카운트를 증가시켜서 이 플레이어의 데이터에 저장되어있는 액션카운트와 비교
        if (animal.ownerUserId == userIDForSimulation_origin)
        {
            currentPlayer1ActionCount++;
            actionCount = currentPlayer1ActionCount;
        }
        else
        {
            currentPlayer2ActionCount++;
            actionCount = currentPlayer2ActionCount;
        }

        int playerIndex = (int)playersIndex[PhotonNetwork.player.UserId];

        // 현재 액션카운트가 한계치에 도달하면
        if(playersData.Players[playerIndex].actionCount == actionCount)
        {
            for (int i = 0; i < myAnimals.Count; i++)
            {
                // 이번턴에 행동을 시작한 동물을 제외하고 모두 canAction을 false시킨다.
                if(myAnimals[i] != animal && !myAnimals[i].isDoingAction)
                {
                    myAnimals[i].canAction = false;
                }
            }    
        }

        bool isEverybodyCantAction = true;
        for(int i = 0; i < myAnimals.Count; i++)
        {
            if (myAnimals[i].canAction)
            {
                isEverybodyCantAction = false;
            }
        }
        // 모든 동물의 canAction이 false라면 turn end버튼 강조
        if (isEverybodyCantAction)
            UIManager.instance.endTurnButton.defaultColor = Color.red;
    }

    int autoSelectAnimalIndex = 0;
    public void AutoSelectNextAnimalCanAction()
    {
        if (IsMyTurn)
        {
            /// 먼저 내 동물들 중 행동이 가능한 동물이 적어도 한 마리는 있어야 한다 
            bool canAnyoneAction = false;
            foreach (var animal in myAnimals)
            {
                if (!animal.isFinishedTurnOrder)
                {
                    canAnyoneAction = true;

                    break;
                }
            }

            /// 넘길만한 동물이 없으면 종료
            if (!canAnyoneAction)
            {
                return;
            }

            /// 찾기 
            bool foundAnimal = false;
            while(!foundAnimal)
            {
                Animal animal = myAnimals[autoSelectAnimalIndex];
                if (!animal.isFinishedTurnOrder)
                {
                    SelectedAnimal = animal;

                    Vector3 destination = animal.transform.position;

                    Camera.main.MoveTo(destination);

                    foundAnimal = true;
                }

                ++autoSelectAnimalIndex;
                if (autoSelectAnimalIndex >= myAnimals.Count)
                {
                    autoSelectAnimalIndex = 0;
                }
            }
        }
    }



    #region TurnManager Callbacks

    public void OnTurnBegins(int turn)
    {
        Debug.Log("OnTurnBegins. turn: " + turn);

        /// 시야 갱신
        TileChunk.instance.RefreshFieldOfView();
    }


    /// 모든 플레이어의 턴이 끝나면 호출됨
    /// 이 함수는 OnPlayerFinished 함수와 처리가 혼동되기 때문에 사용되지 않는다 
    /// 모든 턴 완료처리는 OnPlayerFinished 하도록 한다 
    public void OnTurnCompleted(int obj)
    {
        Debug.Log("OnTurnCompleted: " + obj);
    }


    public bool CheckToCanProceedOverTime()
    {
        /// 이 함수는 추가시간에만 들어와야 하는 함수
        if (!isOverTime)
        {
            return false;
        }

        bool canProceedOverTime = false;

        if (PhotonNetwork.offlineMode)
        {
            /// origin이 유리한 상황에서
            if (score[userIDForSimulation_origin] > score[userIDForSimulation_another])
            {
                /// another가 깃발을 들고 있다면 추가시간
                canProceedOverTime = !GetFlagWithUserId(userIDForSimulation_origin).isOriginPosition;
            }
            /// another가 유리한 상황에서
            else if (score[userIDForSimulation_origin] < score[userIDForSimulation_another])
            {
                /// origin이 깃발을 들고 있다면 추가시간
                canProceedOverTime = !GetFlagWithUserId(userIDForSimulation_another).isOriginPosition;
            }
            /// 동점인 상황에서
            else if (score[userIDForSimulation_origin] == score[userIDForSimulation_another])
            {
                /// 둘 중 한명이라도 깃발을 들고 있다면 추가시간
                canProceedOverTime = !GetFlagWithUserId(userIDForSimulation_origin).isOriginPosition || !GetFlagWithUserId(userIDForSimulation_another).isOriginPosition;
            }
        }


        if (!canProceedOverTime)
        {
            /// player_origin 을 기준으로 승패를 결정한다 
            if (PhotonNetwork.offlineMode)
            {
                ResultType result = ResultType.Draw;
                if (score[userIDForSimulation_origin] > score[userIDForSimulation_another])
                {
                    result = ResultType.LocalWin;
                }
                else if (score[userIDForSimulation_origin] < score[userIDForSimulation_another])
                {
                    result = ResultType.LocalLoss;
                }
                UIManager.instance.DisplayGameResultSprite(result);
            }
        }

        return canProceedOverTime;
    }

    public void OnAnimalDeath(Animal deadAnimal)
    {
        OnAnimalDiedDel(deadAnimal);

        if (deadAnimal.photonView.isMine)
        {
            myAnimals.Remove(deadAnimal);
        }

        if (PhotonNetwork.offlineMode)
        {
            animals1ForSimulation.Remove(deadAnimal);
            animals2ForSimulation.Remove(deadAnimal);
            huntersForSimulation.Remove(deadAnimal);
        }
    }


    public void RefreshEnemyListInMySight()
    {
        _enemyListInMySight.Clear();
        foreach (Animal enemy in enemyAnimals)
        {
            /// 공격할 수 있는 대상인가 
            if (enemy.curHealth <= 0) continue;

            if (enemy.isConcealed || enemy.isClimbedTree || enemy.isSubmerged) continue;

            /// 시야 범위 안에 들어와있는가
            bool isInMySight = false;
            foreach (Tile tileInMySight in GameManager.instance.sightOfUser[PhotonNetwork.player.UserId])
            {
                if (tileInMySight._tileIndex == enemy.ownedTile._tileIndex)
                {
                    isInMySight = true;
                    break;
                }
            }
            if (!isInMySight) continue;
            
            /// 추가
            _enemyListInMySight.Add(enemy);
        }
    }


    /// when a player moved (but did not finish the turn)
    public void OnPlayerMove(PhotonPlayer photonPlayer, int turn, object move)
    {
        Debug.Log("OnPlayerMove: " + photonPlayer + " turn: " + turn + " action: " + move);
    }


    /// when a player made the last/final move in a turn
    /// 이 함수가 플레이어의 턴이 변경되는 함수 
    /// OnTurnCompleted - 모든 플레이어의 턴이 끝났을 때 들어오는 함수
    /// OnPlayerFinished - 한명의 플레이어 턴이 끝날 때마다 들어오는 함수 
    /// OnTurnCompleted가 들어오기 전 OnPlayerFinished가 먼저 호출된다
    /// 한명의 행동이 끝났다면 다른 한명에게 행동하라고 지시를 해줘야 한다 
    public void OnPlayerFinished(PhotonPlayer photonPlayer, int turn, object move)
    {
        Debug.Log("OnTurnFinished: " + photonPlayer + " turn: " + turn + " action: " + move);

        /// 오프라인 모드 전용
		/// 한명의 턴이 끝나면 myAnimals를 다른 녀석의 것으로 스위칭한다 
        if (PhotonNetwork.offlineMode)
        {
            /// ownership을 넘겨서 isMine체크가 true가 안되게 해놓기 
            for (int i = 0; i < myAnimals.Count; ++i)
            {
                Animal animal = myAnimals[i];
                animal.photonView.TransferOwnership(9999);
            }
            
            if (myAnimals == animals1ForSimulation)
            {
                myAnimals = animals2ForSimulation;
                PhotonNetwork.player.UserId = userIDForSimulation_another;
            }
            else if (myAnimals == animals2ForSimulation)
            {
                myAnimals = animals1ForSimulation;
                PhotonNetwork.player.UserId = userIDForSimulation_origin;
            }
            /* 
            if (myAnimals == animals1ForSimulation)
            {
                myAnimals = huntersForSimulation;
                PhotonNetwork.player.UserId = userIDForSimulation_ai;
            }
            else if(myAnimals == huntersForSimulation)
            {
                myAnimals = animals1ForSimulation;
                PhotonNetwork.player.UserId = userIDForSimulation_origin;
            }*/

            /// AI턴일 때 모든 동물 클릭하지 못하도록 내 동물 소유권도 가져오지 않는다 
            /// *정정. 소유권을 가져오지 않으니 isMine이 아니게 되어 AI가 AI를 적으로 인식하고 때리는 문제 발생 
            //if (!isOnAI)
            {
                /// 바뀐 myAnimals의 ownership을 가져와서 isMine체크가 true가 되게 하기 
                for (int i = 0; i < myAnimals.Count; ++i)
                {
                    Animal animal = myAnimals[i];
                    animal.photonView.TransferOwnership(PhotonNetwork.player.ID);
                }
            }

            TileChunk.instance.RefreshFieldOfView();
        }

        /// 턴 사운드. 플레이어 턴 사운드는 동물 클릭 소리가 이것을 대신할꺼다 
        if (photonPlayer.UserId != userIDForSimulation_origin)
        {
            audioSource.PlayOneShot(turnSound_enemy, 0.5f);
        }

        Init();

        UIManager.instance.RefreshUIViews();

        /// 자원을 처리하는 함수추가.
        //resourceManager.ProcessEachTileResource();


        /// 모든 플레이어의 턴이 끝났다면
        if (turnManager.IsCompletedByAll)
        {
            /// 모든 플레이어 동물들의 이동 비용을 초기화시켜주기 
            /// 지금은 플레이어 한명이 모든 플레이어가 어떤 동물들을 가지고 있는지 알 수 있는 방법이 없다
            /// 턴 종료가 되면 알아서 자기 동물들을 관리하도록 하자
            if (PhotonNetwork.offlineMode)
            {
                foreach (var animal in animals1ForSimulation)
                {
                    animal.OnTurnCompleted();
                }
                foreach (var animal in animals2ForSimulation)
                {
                    animal.OnTurnCompleted();
                }
                foreach(var hunter in huntersForSimulation)
                {
                    hunter.OnTurnCompleted();
                }
            }
            else
            {
                foreach (var myAnimal in myAnimals)
                {
                    myAnimal.OnTurnCompleted();
                }
            }

            /// 턴제한 
            bool canBeginTurn = true;
            if (PhotonNetwork.room.GetTurn() >= LIMIT_GAME_TURN)
            {
                /// OverTime을 체크하는 가장 최적의 시기가 PhotonNetwork.room.GetTurn() - LIMIT_GAME_TURN 이 0이 되는 턴이 끝났을 때
                isOverTime = true;
                canBeginTurn = CheckToCanProceedOverTime();
            }

            /// 이 콜백을 받으면 BeginTurn을 호출해줘야 한다 
            if (canBeginTurn && PhotonNetwork.isMasterClient)
            {
                dayAndNight.OnTurnCompleted();

                turnManager.BeginTurn();
            }
        }

        /// 다른 녀석 턴으로 넘기기  
        /// 이런 중요 프로세스는 마스터 클라이언트가 해야 한다
        /// 마스터 클라이언트가 아닌 다른 플레이어에게 이런 작업을 맡기면 그 플레이어가 나갈 시 게임 진행이 막힌다 
        /// 마스터 클라이언트는 포톤 내부에서 자동으로 위임하기 때문에 게임 진행이 막히지 않음
        if (PhotonNetwork.isMasterClient)
        {
            ChangeTurnToNextPlayer();
        }
    }


    /// 플레이어 타임이 오버될 때 호출됨
    public void OnTurnTimeEnds(int obj)
    {
        Debug.Log("OnTurnTimeEnds " + obj);

        Init();

        /// 이 타이머 콜백은 모든 플레이어가 호출할 수 있는 함수임
        /// 모든 플레이어 전부가 같은 내용으로 커스텀 프로퍼티를 수정하는건 낭비임 
        /// 그러므로, 마스터 클라이언트 한명만 수정하게 한다 
        if (PhotonNetwork.isMasterClient)
        {
            ForceFinishThisPlayerTurn();
        }
    }

    #endregion



    #region Photon Callbacks

    public override void OnLeftRoom()
    {
        Debug.Log("OnLeftRoom");

        UIManager.instance.RefreshUIViews();
    }


    public override void OnJoinedRoom()
    {
        if (PhotonNetwork.offlineMode)
        {
            PhotonNetwork.player.UserId = userIDForSimulation_origin;
        }
        
        CheckIfCanSwitchRoomState();

        UIManager.instance.RefreshUIViews();
    }


    public override void OnPhotonPlayerConnected(PhotonPlayer otherPlayer)
    {
        Debug.Log("Other player arrived");

        CheckIfCanSwitchRoomState();

        UIManager.instance.RefreshUIViews();
    }


    public override void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
    {
        Debug.Log("Other player disconnected! " + otherPlayer.ToStringFull());

        // 일시정지
        // Pause는 한명만 요청하는게 맞지만, 연결이 끊긴 플레이어가 마스터 클라이언트였다면, 누가 퍼즈를 걸어야할지 애매하다
        // 그러므로, 모든 플레이어가 Pause를 요청하게 한다
        // 가장 마지막으로 Pause를 요청한 녀석 기준으로 ElapsedTime이 캡쳐될 것임 
        turnManager.Pause();
        PhotonNetwork.room.ChangeRoomState(RoomState.ROOM_STATE_PAUSE);

        UIManager.instance.RefreshUIViews();
    }


    public override void OnPhotonCustomRoomPropertiesChanged(Hashtable propertiesThatChanged)
    {
        /// 기지 세팅이 되었을 때
        if (propertiesThatChanged.ContainsKey(PLAYERS_INDEX_PROP_KEY))
        {
            SetCustomPropertiesMyAnimals();
            
            if (PhotonNetwork.offlineMode)
            {
                PhotonNetwork.player.UserId = userIDForSimulation_another;

                SetCustomPropertiesMyAnimals();
            }
            /*
            // 마스터클라이언트가 사냥꾼의 정보를 프로퍼티에 올리는 함수를 호출.
            if (PhotonNetwork.isMasterClient)
            {
                PhotonNetwork.player.UserId = userIDForSimulation_ai;
                SetCustomPropertiesHunters();
            } */
        }
        /// 누군가가 애니멀 세팅을 보내왔을 때
        else if (propertiesThatChanged.ContainsKey(WHO_DID_SET_ANIMALS_PROP_KEY))
        {
            SyncAnimalsByUsingCustomProperties(propertiesThatChanged);

            if (PhotonNetwork.offlineMode)
            {
                /// 시뮬레이션에 사용하기 위한 AnimalsList 정리
                if (PhotonNetwork.player.UserId == userIDForSimulation_origin)
                {
                    foreach (var animal in myAnimals)
                    {
                        if (!animals1ForSimulation.Contains(animal))
                        {
                            animals1ForSimulation.Add(animal);
                            animal.animalListBelongTo = animals1ForSimulation;
                        }
                    }
                }
                else if (PhotonNetwork.player.UserId == userIDForSimulation_another)
                {
                    foreach (var animal in myAnimals)
                    {
                        if (!animals2ForSimulation.Contains(animal))
                        {
                            animals2ForSimulation.Add(animal);
                            animal.animalListBelongTo = animals2ForSimulation;
                            animal.photonView.TransferOwnership(9999);
                        }
                    }
                }

                if (turnManager.Turn == 0)
                {
                    if (PhotonNetwork.player.UserId == userIDForSimulation_origin)
                    {
                        myAnimals.Clear();
                    }
                    else if (PhotonNetwork.player.UserId == userIDForSimulation_another || PhotonNetwork.player.UserId == userIDForSimulation_ai)
                    {
                        if (AI_firstTurn)
                        {
                            myAnimals = animals2ForSimulation;
                            PhotonNetwork.player.UserId = userIDForSimulation_another;
                        }
                        else
                        {
                            myAnimals = animals1ForSimulation;
                            PhotonNetwork.player.UserId = userIDForSimulation_origin;
                        }
                    }
                }
            }

            if (PhotonNetwork.offlineMode)
            {
                StartCoroutine(CheckIfCanSwitchRoomStateWithDelay());
            }
            else
            {
                CheckIfCanSwitchRoomState();
            } 
        }
        // 헌터 프로퍼티가 업데이트되면
        else if (propertiesThatChanged.ContainsKey(HUNTERS_PROP_KEY))
        {
            // 헌터프로퍼티 토대로 헌터를 생성한다.
            SyncHuntersByUsingCustomProperties(propertiesThatChanged);
            if (PhotonNetwork.offlineMode)
            {
                if (PhotonNetwork.player.UserId == userIDForSimulation_ai)
                {
                    foreach (var hunter in allHunters)
                    {
                        if (!huntersForSimulation.Contains(hunter))
                        {
                            huntersForSimulation.Add(hunter);
                            //animal.photonView.TransferOwnership(9999);
                        }
                    }
                }
                /*
                if (turnManager.Turn == 0)
                {
                    if (PhotonNetwork.player.UserId == userIDForSimulation_origin)
                    {
                        myAnimals.Clear();
                    }
                    else if (PhotonNetwork.player.UserId == userIDForSimulation_another || PhotonNetwork.player.UserId == userIDForSimulation_ai)
                    {
                        myAnimals = animals1ForSimulation;
                        PhotonNetwork.player.UserId = userIDForSimulation_origin;
                    }
                }*/

                if (AI_firstTurn)
                {
                    /// 테스트하기 위해 첫 시작을 Hunter가 하도록 함 
                    myAnimals = huntersForSimulation;
                    PhotonNetwork.player.UserId = userIDForSimulation_ai;
                }
                else
                {
                    myAnimals = animals1ForSimulation;
                    PhotonNetwork.player.UserId = userIDForSimulation_origin;
                }
            }
            
            if (PhotonNetwork.offlineMode)
            {
                StartCoroutine(CheckIfCanSwitchRoomStateWithDelay());
            }
            else
            {
                CheckIfCanSwitchRoomState();
            }
        }
        /// 플레이어 턴 교체될 때
		else if (propertiesThatChanged.ContainsKey(TurnExtensions.WhoIsTurnPropKey))
        {
            OnChangedPlayerTurn(PhotonNetwork.room.GetWhoIsTurn());

            StartCoroutine(PlayerActionTurnStarted());
        }
        

        Debug.Log("OnPhotonCustomRoomPropertiesChanged: " + propertiesThatChanged.ToStringFull());

        UIManager.instance.RefreshUIViews();
    }


    /// 오프라인모드를 하면 이벤트 함수가 즉각 호출되기 때문에 동물이 생성되고 바로 게임 시작이 이뤄진다 
    /// 이러면 Animal 클래스 Start 함수가 호출이 안되서 NullReferenceException이 뜬다 
	/// 그래서 1프레임 기다리고 게임을 시작한다 
    IEnumerator CheckIfCanSwitchRoomStateWithDelay()
    {
        yield return null;

        CheckIfCanSwitchRoomState();
    }


    // 플레이어가 바뀔때 처음 호출되는 함수, 동물을 랜덤하게 하나 고름
    IEnumerator PlayerActionTurnStarted()
    {
        /// BiteCommand 연출 처리 때문에 이렇게 해놓은 듯 
		/// canMove가 true로 안 바꿔주면 위험한 코드라 주석 
        // while (!InputToEvent.canMove)
        // {
        //     yield return null;
        // }
        yield return null;

        //if (IsMyTurn)
        if (PhotonNetwork.room.GetWhoIsTurn() == userIDForSimulation_origin || !isOnAI)
        {
            InputToEvent.canMove = true;
            autoSelectAnimalIndex = Random.Range(0, myAnimals.Count);
            AutoSelectNextAnimalCanAction();
        }
        //else if (PhotonNetwork.room.GetWhoIsTurn() == userIDForSimulation_ai && PhotonNetwork.isMasterClient)
        else if (PhotonNetwork.player.UserId != userIDForSimulation_origin && isOnAI)
        {
            InputToEvent.canMove = false;

            if (!IsTrackedCoroutineRunning("DoAI")) StartTrackedCoroutine("DoAI");
        }
    }
	#endregion
}





public static class GameExtensions
{
    public static readonly string ROOM_STATE_PROP_KEY = "roomState";


    /// 룸 상태 변경 익스텐션 메소드
    public static void ChangeRoomState(this Room room, GameManager.RoomState roomState)
    {
        if (room == null || room.CustomProperties == null)
        {
            return;
        }

        Hashtable roomStateProp = new Hashtable();
        roomStateProp[ROOM_STATE_PROP_KEY] = (int)roomState;

        room.SetCustomProperties(roomStateProp);
    }


    public static GameManager.RoomState GetRoomState(this RoomInfo room)
    {
        /// 디폴트는 ROOM_STATE_WAITING_FOR_START
        if (room == null || room.CustomProperties == null || !room.CustomProperties.ContainsKey(ROOM_STATE_PROP_KEY))
        {
            return GameManager.RoomState.ROOM_STATE_WAITING_FOR_START;
        }

        return (GameManager.RoomState)room.CustomProperties[ROOM_STATE_PROP_KEY];
    }


    public static void Shuffle<T>(this List<T> list)
    {
        int random1;
        int random2;

        T tmp;

        for (int index = 0; index < list.Count; ++index)
        {
            random1 = UnityEngine.Random.Range(0, list.Count);
            random2 = UnityEngine.Random.Range(0, list.Count);

            tmp = list[random1];
            list[random1] = list[random2];
            list[random2] = tmp;
        }
    }
}