﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BreedManager : MonoBehaviour {
    public TileChunk tileChunk;

    public UIManager uiManager;
    Tile selectedTileForBreeding;
    Animal selectedAnimalForBreeding;
    bool isBreeding = false;
    bool isBreedingTileClicked = false;
    List<Tile> tileListForBreeding;

    void Start()
    {
        tileListForBreeding = new List<Tile>();

        tileChunk.OnClickedTile += OnClickWith;
    }


    // breed조건 추가필요...
    public bool CheckIfCanBreed(Animal animal)
    {
        Tile tile = animal.ownedTile;
        if (tileChunk.GetTile(tile.TileIndex_X - 1, tile.TileIndex_Y).type != TileMaker.TileType.NA_TILE)
        {
            if (tileChunk.GetTile(tile.TileIndex_X - 1, tile.TileIndex_Y).owningAnimal != null)
            {
                if (tileChunk.GetTile(tile.TileIndex_X - 1, tile.TileIndex_Y).owningAnimal.species == animal.species)
                {
                    return true;
                }
            }
        }
        if (tileChunk.GetTile(tile.TileIndex_X, tile.TileIndex_Y - 1).type != TileMaker.TileType.NA_TILE)
        {
            if (tileChunk.GetTile(tile.TileIndex_X, tile.TileIndex_Y - 1).owningAnimal != null)
            {
                if (tileChunk.GetTile(tile.TileIndex_X, tile.TileIndex_Y - 1).owningAnimal.species == animal.species)
                {
                    return true;
                }
            }
        }
        if (tileChunk.GetTile(tile.TileIndex_X + 1, tile.TileIndex_Y).type != TileMaker.TileType.NA_TILE)
        {
            if (tileChunk.GetTile(tile.TileIndex_X + 1, tile.TileIndex_Y).owningAnimal != null)
            {
                if (tileChunk.GetTile(tile.TileIndex_X + 1, tile.TileIndex_Y).owningAnimal.species == animal.species)
                {
                    return true;
                }
            }
        }
        if (tileChunk.GetTile(tile.TileIndex_X, tile.TileIndex_Y + 1).type != TileMaker.TileType.NA_TILE)
        {
            if (tileChunk.GetTile(tile.TileIndex_X, tile.TileIndex_Y + 1).owningAnimal != null)
            {
                if (tileChunk.GetTile(tile.TileIndex_X, tile.TileIndex_Y + 1).owningAnimal.species == animal.species)
                {
                    return true;
                }
            }
        }
        return false;
    }
    

    public bool CheckIfCanGrowUp(Animal animal)
    {
        if (true) //(animal.animalData.isBaby) growup이 아기일때만 하는것이아니고 계속 성장을 한다. 그래서 일단 보류해 놓았다.
            return true;
        else
            return false;
    }


    public void OnClickBreedButton()
    {
        //GameManager.instance.stateEnum = GameManager.instance.processStateEnum.BREED;
        selectedAnimalForBreeding = GameManager.instance.SelectedAnimal;
        ShowPossibleTilesForBreeding(selectedAnimalForBreeding);
    }


    public void OnClickCancelButton()
    {
        //GameManager.instance.stateEnum = GameManager.instance.processStateEnum.IDLE;
        uiManager.HideSkillUI();
    }

    void ShowPossibleTilesForBreeding(Animal animal)
    {
        GetPossibleTileForBreeding(animal);
        foreach (Tile tile in tileListForBreeding)
        {
            tileChunk.AddVerticesIntoLinePointsByScale(tile, 0.9f, tileChunk.lineMagenta);
        }
        //tileChunk.DisplayAreasToBreed();
    }


    void GetPossibleTileForBreeding(Animal animal)
    {
        Tile coreTile = animal.ownedTile;
        tileListForBreeding.Clear();

        if (tileChunk.GetTile(coreTile.TileIndex_X - 1, coreTile.TileIndex_Y).type != TileMaker.TileType.NA_TILE)
        {
            if (tileChunk.GetTile(coreTile.TileIndex_X - 1, coreTile.TileIndex_Y).owningAnimal == null 
                && !tileChunk.GetTile(coreTile.TileIndex_X - 1, coreTile.TileIndex_Y).isRock)
            {
                tileListForBreeding.Add(tileChunk.GetTile(coreTile.TileIndex_X - 1, coreTile.TileIndex_Y));
            }
        }
        if (tileChunk.GetTile(coreTile.TileIndex_X, coreTile.TileIndex_Y - 1).type != TileMaker.TileType.NA_TILE)
        {
            if (tileChunk.GetTile(coreTile.TileIndex_X, coreTile.TileIndex_Y - 1).owningAnimal == null
                && !tileChunk.GetTile(coreTile.TileIndex_X, coreTile.TileIndex_Y - 1).isRock)
            {
                tileListForBreeding.Add(tileChunk.GetTile(coreTile.TileIndex_X, coreTile.TileIndex_Y - 1));
            }
        }
        if (tileChunk.GetTile(coreTile.TileIndex_X + 1, coreTile.TileIndex_Y).type != TileMaker.TileType.NA_TILE)
        {
            if (tileChunk.GetTile(coreTile.TileIndex_X + 1, coreTile.TileIndex_Y).owningAnimal == null
                && !tileChunk.GetTile(coreTile.TileIndex_X + 1, coreTile.TileIndex_Y).isRock)
            {
                tileListForBreeding.Add(tileChunk.GetTile(coreTile.TileIndex_X + 1, coreTile.TileIndex_Y));
            }
        }
        if (tileChunk.GetTile(coreTile.TileIndex_X, coreTile.TileIndex_Y + 1).type != TileMaker.TileType.NA_TILE)
        {
            if (tileChunk.GetTile(coreTile.TileIndex_X, coreTile.TileIndex_Y + 1).owningAnimal == null
                && !tileChunk.GetTile(coreTile.TileIndex_X, coreTile.TileIndex_Y + 1).isRock)
            {
                tileListForBreeding.Add(tileChunk.GetTile(coreTile.TileIndex_X, coreTile.TileIndex_Y + 1));
            }
        }
    }


    public void OnClickOKButton()
    {
        //selectedAnimalForBreeding.Breed(selectedTileForBreeding);
        //GameManager.instance.stateEnum = GameManager.instance.processStateEnum.IDLE;
    }


    public void OnClickWith(Tile tile)
    {/*
        if (GameManager.instance.stateEnum == GameManager.instance.processStateEnum.BREED)
        {
            selectedTileForBreeding = tile;
            tileChunk.UndisplaySelectedTile();
            tileChunk.AddVerticesIntoLinePointsByScale(tile, 0.5f, tileChunk.lineGreen);
            tileChunk.DisplaySelectedTile();
            isBreedingTileClicked = true;
            uiManager.DisplayOKUI();
            uiManager.DisplayCancelUI();    
        }
        else return;*/
    }


    public void CancelBreeding()
    {
        isBreeding = false;
        isBreedingTileClicked = false;
        selectedTileForBreeding = null;
        //GameManager.instance.stateEnum = GameManager.instance.processStateEnum.IDLE;
    }
}
