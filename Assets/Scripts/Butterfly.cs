﻿using UnityEngine;
using System.Collections;

public class Butterfly : MonoBehaviour {

    float speed = 15.0f;
    bool up = false;
	// Use this for initialization
	void Start () {
        FlyTweenHash = iTween.Hash("easeType", iTween.EaseType.linear, "looktime",1, "speed", speed, "onstart", "ToggleFlyAnimate", "onstartparams", false,
                                                                                            "oncomplete", "DrinkFlower",
                                                                                            "islocal", false);

        butterflyAnimator = GetComponent<Animator>();

        GetComponentInChildren<Renderer>().material.renderQueue = 3002;
        Color color= Color.black; 
        switch (Random.Range(1, 4)) {
            case 1:
                color = new Color(251, 0, 255);
                break;
            case 2:
                color = new Color(0, 213, 255);
                break;
            case 3:
                color = new Color(251, 255, 0);
                break;
        }
        GetComponentInChildren<Renderer>().material.SetColor("_TintColor", color);

        ButterflyFlyToFlower();
    }
    bool _isVisible = false;
    bool isVisible
    {
        get
        {
            return _isVisible;
        }
        set
        {
            _isVisible = value;
            GetComponentInChildren<SkinnedMeshRenderer>().enabled = value;
        }
    }
    private void Update()
    {
        if(TileChunk.instance.GetTileWithPosition(transform.position).isVisible != isVisible)
        {
            isVisible = !isVisible;
        }
    }

    Hashtable FlyTweenHash;
    public void ButterflyFlyToFlower()
    {       
        if (ButterflyManager.instance.flowersPos.Count == 0)
            return;
        else
        {
            Vector3 flowerPos = ButterflyManager.instance.flowersPos[Random.Range(0, ButterflyManager.instance.flowersPos.Count)];

            FlyTweenHash["x"] = flowerPos.x;
            FlyTweenHash["y"] = flowerPos.y;
            FlyTweenHash["z"] = flowerPos.z;
            FlyTweenHash["looktarget"] = flowerPos;

            iTween.MoveTo(gameObject, FlyTweenHash);            
        }
    }

    void DrinkFlower()
    {
        ToggleFlyAnimate(true);
    }

    Animator butterflyAnimator;
    bool isDrinking = false;
    void ToggleFlyAnimate(bool drinking)
    {
        isDrinking = drinking;
        butterflyAnimator.SetBool("isDrinking", drinking);
    }
}
