﻿using UnityEngine;
using System.Collections;

public delegate void OnMoveCompleteDelegate();

public class CameraMovement : CacheMonobehavior 
{
    public static CameraMovement instance;

    public float zoomSpeed = 4.0f;      // Speed of the camera going back and forth

    private Vector3 mouseOrigin;    // Position of cursor when mouse dragging starts
    private bool isPanning;     // Is the camera being panned?
    private bool isZooming;		// Is the camera zooming?

    [SerializeField]
    float orthographicSizeLimit_min = 50;
    [SerializeField]
    float orthographicSizeLimit_max = 150;

    [SerializeField]
    Camera effectCamera;

    [SerializeField]
    float moveSpeed = 10f;

    [System.NonSerialized]
    public Transform followingTarget;

    public OnMoveCompleteDelegate OnMoveComplete = delegate { };



    private void Awake()
    {
        instance = this;
    }

    void Update()
    {
        // Get the right mouse button
        if (Input.GetMouseButtonDown(0))
        {
            // Get mouse origin
            mouseOrigin = Input.mousePosition;
            isPanning = true;
        }

        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            isZooming = true;
        }

        // Disable movements on button release
        if (!Input.GetMouseButton(0)) isPanning = false;


        if (isZooming)
        {
            do
            {
                float wheelAxis = Input.GetAxisRaw("Mouse ScrollWheel");

                if (Camera.main.orthographic)
                {
                    if (wheelAxis > 0)
                    {
                        if (Camera.main.orthographicSize <= orthographicSizeLimit_min)
                        {
                            Camera.main.orthographicSize = orthographicSizeLimit_min;

                            break;
                        }
                    }
                    else
                    {
                        if (Camera.main.orthographicSize >= orthographicSizeLimit_max)
                        {
                            Camera.main.orthographicSize = orthographicSizeLimit_max;

                            break;
                        }
                    }

                    Camera.main.orthographicSize += -Input.GetAxis("Mouse ScrollWheel") * zoomSpeed;
                }
                else
                {
                    if (wheelAxis > 0)
                    {
                        if (transform.position.y < 100f) break;
                    }
                    else
                    {
                        if (transform.position.y > 200f) break;
                    }

                    Vector3 move = Input.GetAxis("Mouse ScrollWheel") * zoomSpeed * transform.forward;
                    transform.Translate(move, Space.World);
                }
                

            } while (false);

            effectCamera.orthographicSize = Camera.main.orthographicSize;
        }

        if (followingTarget != null)
        {
            Vector3 destination = followingTarget.position;
            destination.y = transform.position.y;
            
            if (Camera.main.orthographic)
            {
                destination.x += -120;
                destination.z += -120;
            }
            else
            {
                /// 카메라가 Y축으로 45도 회전되어 있고
                /// 높이가 200일 때, X : -120, Z : -120
                /// 높이가 100일 때, X : -60,  Z : -60
                float t = (destination.y - 100f) / 100f;

                /// 높이가 Zoom 때문에 100~200 사이가 되었을 때, 보간처리를 통해 딱 화면 중앙에 놓이도록 함 
                float x = Mathf.Lerp(-60f, -120f, t);
                float z = Mathf.Lerp(-60f, -120f, t);

                destination.x += x;
                destination.z += z;
            }
            
            transform.position = Vector3.Lerp(transform.position, destination, Time.deltaTime * moveSpeed);
        }
    }


    void OnCompleteMove()
    {
        OnMoveComplete();
    }
}



public static class CameraExtentions
{
    public static void MoveTo(this Camera camera, Vector3 destination, bool immediate = false, float time = 2f)
    {
        if (camera == null)
        {
            return;
        }

        destination.y = Camera.main.transform.position.y;


        /// 카메라가 Y축으로 45도 회전되어 있고
        /// 높이가 200일 때, X : -120, Z : -120
        /// 높이가 100일 때, X : -60,  Z : -60
        float t = (destination.y - 100f) / 100f;

        /// 높이가 Zoom 때문에 100~200 사이가 되었을 때, 보간처리를 통해 딱 화면 중앙에 놓이도록 함 
        float x = Mathf.Lerp(-60f, -120f, t);
        float z = Mathf.Lerp(-60f, -120f, t);

        destination.x += x;
        destination.z += z;

        if (immediate)
        {
            camera.transform.position = destination;
        }
        else
        { 
            iTween.MoveTo(camera.gameObject, iTween.Hash("position", destination, "time", time, "oncomplete", "OnCompleteMove"));
        }
    }
}