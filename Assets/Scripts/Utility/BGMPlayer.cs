﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BGMPlayer : MonoBehaviour
{
    public enum BGM
    {
        TITLE,
        BATTLE,
        ROOM,
        LOBBY,
    }
    BGM _currentBGM = BGM.TITLE;

    public static BGMPlayer instance;

    [SerializeField]
    AudioClip[] _titleBGMList;

    [SerializeField]
    AudioClip[] _battleBGMList;

    [SerializeField]
    AudioClip[] _roomBGMList;

    [SerializeField]
    AudioClip[] _lobbyBGMList;

    AudioSource _audioSource;

    ///  설정용 On/Off 변수
    bool _settingIsOnBGM = true;

    bool _stopPlayCoroutine = false;

    void Awake()
    {
        instance = this;

        _audioSource = GetComponent<AudioSource>();
    }

    void Start()
    {
    }

    void OnDestroy()
    {
    }

    void OnToggle_BGM(bool isOn)
    {
        _settingIsOnBGM = isOn;

        if (isOn)
        {
            Play();
        }
        else
        {
            Stop();
        }
    }

    public void Play(BGM bgm)
    {
        _currentBGM = bgm;
        
        if (_settingIsOnBGM)
        {
            _stopPlayCoroutine = false;

            StartCoroutine(Play());
        }
    }

    IEnumerator Play()
    {
        while (_settingIsOnBGM && !_stopPlayCoroutine)
        {
            var clip = GetBGMClip(_currentBGM);
            _audioSource.clip = clip;
            _audioSource.Play();
            yield return new WaitForSeconds(clip.length);
        }
    }

    public void Stop()
    {
        _audioSource.Stop();

        _audioSource.clip = null;

        _stopPlayCoroutine = true;
    }

    AudioClip GetBGMClip(BGM bgm)
    {
        AudioClip clip = null;

        switch (bgm)
        {
            case BGM.TITLE:
                {
                    if (_titleBGMList.Length > 0)
                    {
                        int randomIndex = Random.Range(0, _titleBGMList.Length);
                        clip = _titleBGMList[randomIndex];
                    }
                }
                break;

            case BGM.BATTLE:
                {
                    if (_battleBGMList.Length > 0)
                    {
                        int randomIndex = Random.Range(0, _battleBGMList.Length);
                        clip = _battleBGMList[randomIndex];
                    }
                }
                break;

            case BGM.ROOM:
                {
                    if (_roomBGMList.Length > 0)
                    {
                        int randomIndex = Random.Range(0, _roomBGMList.Length);
                        clip = _roomBGMList[randomIndex];
                    }
                }
                break;

            case BGM.LOBBY:
                {
                    if (_lobbyBGMList.Length > 0)
                    {
                        int randomIndex = Random.Range(0, _lobbyBGMList.Length);
                        clip = _lobbyBGMList[randomIndex];
                    }
                }
                break;
        }

        return clip;
    }
}
