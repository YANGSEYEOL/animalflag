﻿using UnityEngine;
using System.Collections;

public class AutoDestroy : MonoBehaviour
{
    [SerializeField]
    float _lifeTime = 1.0f;

    // unity functions ---------------------------------------------------
    void Start()
    {
        Destroy(gameObject, _lifeTime);
    }
}
