﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ItemScroll : MonoBehaviour
{
    public GameObject AddItem(GameObject itemPrefab)
    {
        // 아이템 추가
        GameObject newItem = Instantiate(itemPrefab, itemPrefab.transform.position, itemPrefab.transform.rotation) as GameObject;
        newItem.name = itemPrefab.name + transform.childCount;
        newItem.transform.SetParent(gameObject.transform);
        newItem.SetActive(true);

        return newItem;
    }

    public void ResetItems()
    {
        RectTransform[] children = GetComponentsInChildren<RectTransform>();
        foreach (RectTransform child in children)
        {
            if (child.gameObject != gameObject && child.gameObject.activeSelf)
            {
                Destroy(child.gameObject);
            }
        }

        transform.DetachChildren();
    }
}