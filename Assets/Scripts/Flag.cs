﻿using UnityEngine;
using Photon;
using Vectrosity;
using System.Collections;
using System.Collections.Generic;

public class Flag : CacheMonobehavior
{
    GameManager gameManager;
    TileChunk tileChunk;

    [SerializeField]
    float heightOffset = 50f;
    float _rotation;
    float _height;

    string _ownerUserId;
    public string ownerUserId 
    { 
        get { return _ownerUserId; } 
        set 
        { 
            _ownerUserId = value; 

            line.color = value == gameManager.userIDForSimulation_origin ? Color.white : Color.yellow;
        }
    }

    Tile _ownedTile;
    public Tile ownedTile
    {
        get { return _ownedTile; }
        set
        {
            if (_ownedTile != null)
            {
                _ownedTile.OnChangedOwningAnimal -= OnChangedOwningAnimalFromOwnedTile;
            }

            _ownedTile = value;

            if (value != null)
            {
                value.OnChangedOwningAnimal += OnChangedOwningAnimalFromOwnedTile;

                Vector3 position = value.position;
                position.y = _height;
                transform.position = position;
            }
        }
    }

    Tile _originPositionTile;
    public Tile originPositionTile
    {
        get { return _originPositionTile; }
        set
        {
            _originPositionTile = value;

            ownedTile = value;
        }
    }

    public Tile originPositionTile_enemy;

    Animal _carryingAnimal;
    public Animal carryingAnimal
    {
        get { return _carryingAnimal; }
        set
        {
            if (_carryingAnimal != null)
            {
                _carryingAnimal.OnDeath -= OnDeathCarryingAnimal;
            }

            _carryingAnimal = value;

            if (value != null)
            {
                value.OnDeath += OnDeathCarryingAnimal;
            }
        }
    }

    Animal _retrievingAnimal;
    Animal retrievingAnimal
    {
        get { return _retrievingAnimal; }
        set
        {
            if (_retrievingAnimal != null)
            {
                _retrievingAnimal.OnChangedOwnedTile -= OnChangedOwnedTileFromRetrievingAnimal;
            }

            _retrievingAnimal = value;

            if (value != null)
            {
                value.OnChangedOwnedTile += OnChangedOwnedTileFromRetrievingAnimal;
            }
        }
    }

    bool isWaitingToResetPosition = false;

    public bool isOriginPosition { get { return ownedTile == originPositionTile; } }

    Material lineMaterial;
    VectorLine line;
    [SerializeField]
    Mesh flagMesh;

    [SerializeField]
    AudioClip goalInSound;
    [SerializeField]
    AudioClip stealSound;


    private void Start()
    {
        gameManager = GameManager.instance;
        gameManager.OnChangedPlayerTurn += OnChangedPlayerTurn;

        tileChunk = TileChunk.instance;
        lineMaterial = tileChunk.lineMaterial;
        /* 
        List<Vector3> cubePoints = new List<Vector3>();
        cubePoints.Add(new Vector3(-0.5f, -0.5f, 0.5f));
        cubePoints.Add(new Vector3(0.5f, -0.5f, 0.5f));
        cubePoints.Add(new Vector3(-0.5f, 0.5f, 0.5f));
        cubePoints.Add(new Vector3(-0.5f, -0.5f, 0.5f));
        cubePoints.Add(new Vector3(0.5f, -0.5f, 0.5f));
        cubePoints.Add(new Vector3(0.5f, 0.5f, 0.5f));
        cubePoints.Add(new Vector3(0.5f, 0.5f, 0.5f));
        cubePoints.Add(new Vector3(-0.5f, 0.5f, 0.5f));
        cubePoints.Add(new Vector3(-0.5f, 0.5f, -0.5f));
        cubePoints.Add(new Vector3(-0.5f, 0.5f, 0.5f));
        cubePoints.Add(new Vector3(0.5f, 0.5f, 0.5f));
        cubePoints.Add(new Vector3(0.5f, 0.5f, -0.5f));
        cubePoints.Add(new Vector3(0.5f, 0.5f, -0.5f));
        cubePoints.Add(new Vector3(-0.5f, 0.5f, -0.5f));
        cubePoints.Add(new Vector3(-0.5f, -0.5f, -0.5f));
        cubePoints.Add(new Vector3(-0.5f, 0.5f, -0.5f));
        cubePoints.Add(new Vector3(0.5f, 0.5f, -0.5f));
        cubePoints.Add(new Vector3(0.5f, -0.5f, -0.5f));
        cubePoints.Add(new Vector3(0.5f, -0.5f, -0.5f));
        cubePoints.Add(new Vector3(-0.5f, -0.5f, -0.5f));
        cubePoints.Add(new Vector3(-0.5f, -0.5f, 0.5f));
        cubePoints.Add(new Vector3(-0.5f, -0.5f, -0.5f));
        cubePoints.Add(new Vector3(0.5f, -0.5f, -0.5f));
        cubePoints.Add(new Vector3(0.5f, -0.5f, 0.5f));
        line = new VectorLine("Flag", cubePoints, lineMaterial, tileChunk.intensityOfGlowLine);
        */
        
        line = new VectorLine("Flag", new Vector3[0], lineMaterial, 5.0f, LineType.Discrete);
        line.MakeWireframe(flagMesh);

        /// 깃발 껐다 킬 때 라인 몇개가 남아서 렌더링 자국이 남는 현상을 방지 
        line.UseIndependentCanvas3D(); 

        // Make this transform have the vector line object that's defined above
        // This object is a rigidbody, so the vector object will do exactly what this object does
        // "false" is added at the end, so that the cube mesh is not replaced by an invisible bounds mesh
        VectorManager.useDraw3D = true;
        VectorManager.ObjectSetup(gameObject, line, Visibility.Dynamic, Brightness.None, false);


        _height = heightOffset;
    }


    void Update()
    {
        UpdatePosition();
        //UpdateRotation();
    }


    void UpdatePosition()
    {
        if (carryingAnimal != null)
        {
            Vector3 targetPosition = carryingAnimal.transform.position;

            targetPosition.y = _height;

            if (float.IsNaN(targetPosition.x) == false && float.IsNaN(targetPosition.z) == false)
            {
                transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime * 10f);
            }
        }
    }


    void UpdateRotation()
    {
        _rotation += Time.deltaTime * 90f;
        _rotation = _rotation % 360;

        transform.rotation = Quaternion.Euler(0, _rotation, 0);
    }


    public void ResetPosition()
    {
        ownedTile = originPositionTile;

        /// 추가시간일 때, 게임 종료 체크
        gameManager.CheckToCanProceedOverTime();
    }


    void OnChangedPlayerTurn(string turnUserId)
    { 
        /*if (PhotonNetwork.offlineMode)
        {
            /// 색깔 바꾸기 
            line.color = turnUserId == ownerUserId ? Color.white : Color.yellow;
        }*/

        /// owner의 깃발로 점수를 획득하는 것은 owner가 아닌 enemy다 
        /// enemy가 점수를 획득 후, enemy 턴이 다시 왔을 때 깃발 위치를 다시 시작 지점으로 옮긴다 
        if (isWaitingToResetPosition && turnUserId != ownerUserId)
        {
            isWaitingToResetPosition = false;

            SetVisible(true);

            ResetPosition();
        }
    }

    
    void OnChangedOwningAnimalFromOwnedTile(Animal animal)
    {
        if (animal == null)
        {
            return;
        }

        if (animal.ownerUserId == ownerUserId && !isOriginPosition)
        {
            /// 깃발 회수 가능 
            animal.canRetrieveFlag = true;

            retrievingAnimal = animal;
        }
    }


    public void PlayFlagStealOrGoalSound(Animal animal)
    {
        if (animal.ownerUserId != ownerUserId && ownedTile == animal.movingTile)
        {
            audioSource.PlayOneShot(stealSound, 0.5f);
        }

        if (carryingAnimal == animal && carryingAnimal.movingTile == originPositionTile_enemy)
        {
            audioSource.PlayOneShot(goalInSound, 0.5f);
        } 
    }


    public void OnMoved(Animal animal, Tile tile)
    {
        if (animal.ownerUserId != ownerUserId && ownedTile == animal.movingTile)
        {
            /// 깃발 탈취 가능
            carryingAnimal = animal;

            ownedTile = null;
        }

        if (carryingAnimal == animal && carryingAnimal.movingTile == originPositionTile_enemy)
        {
            /// 득점 계산
            isWaitingToResetPosition = true;

            gameManager.AddScore(carryingAnimal.ownerUserId);

            carryingAnimal = null;

            SetVisible(false);
        } 
    }


    void OnChangedOwnedTileFromRetrievingAnimal(Tile tile)
    {
        if (tile != ownedTile)
        {
            /// 깃발있는 타일에서 벗어나면 retriving 취소
            retrievingAnimal.canRetrieveFlag = false;

            retrievingAnimal = null;
        }
    }


    void OnDeathCarryingAnimal(Animal animal)
    {
        carryingAnimal = null;

        ownedTile = tileChunk.GetTileWithPosition(transform.position);
    }


    void SetVisible(bool visible)
    {
        /// 야매 플레이
        /// line.drawEnd를 0으로 바꿔도 자꾸 라인이 완벽하게 안 사라지길래 
        /// 아예 line을 그리는 canvas자체를 껐다 켜버렸다 
        line.canvas3D_independent.gameObject.SetActive(visible);
    }
}

