﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;

// UI를 컨트롤하는 매니져
public class UIManager : Photon.PunBehaviour
{
    public static UIManager instance;

    public PunTurnManager turnManager;
    GameManager _gameManager;
    DayAndNight dayAndNight;
    TileChunk tileChunk;
    public BreedManager breedManager;
    
    public Camera _uiCamera;
    public Camera _worldCamera;

    public UIAtlas atlasForHpBar;
    public GameObject hpBarPrefab;

    public UIPanel _disconnectedPanel;

    public GameObject hpBarContainer;
    public UIButton endTurnButton;


    public GameObject selectionArrow;
    public GameObject targetSprite;

    public UIButton eatButton;
    public UIButton drinkButton;
    public UIButton breedButton;
    public UIButton growUpButton;
    public UIButton skillUseButton;
    public UIButton okButton;
    public UIButton cancelButton;
    public UIButton learnButton;
    public UILabel learnLabel;

    public GameObject popUpContainer;
    public GameObject popUpPrefab;
    public Sprite[] popUpSprites;

    public UILabel tileInfoTypeLabel;
    public UISprite tileInfoSeasonSprite;
    public UISprite tileInfoGrassWaterSprite;
    public UILabel tileInfoGrassWaterCountLabel;
    public UILabel tileInfoMeatCountLabel;

    public UISprite animalInfoSprite;
    public UILabel animalInfoLevelLabel;
    public UILabel animalInfoHpLabel;

    public UIButton[] skillUseButtons;

    public UIWidget skillTreeWidget;
    public UISprite[] skillContainer = new UISprite[3];
    public SkillButton[] skillButton = new SkillButton[6];
    public UIInput ConnectUiView;
    public UIPanel GameUiView;

    public UISprite TimerClock;
    public UISlider TimerFillImageSlider;
    public UISprite TimerFillImageSprite;
    public UILabel TimeText;

    public UILabel RemotePlayerText;
    public UILabel LocalPlayerText;
    public UILabel RemainingTurnOfSwitchingDayAndNightText;
    public UILabel centerHeadTextUI;
    public UISprite gameResultSprite;
    public UILabel remainingTurnText;
    public UILabel curcommandIndicator;
    public GameObject buttons;
    public UILabel skillDescription;
    SkillButton selectedSkillButton;
    public UILabel confirmUI_name;
    public UILabel confirmUI_desc;
    public UISprite confirmUI_bg;
    public UILabel enemyTurnText;

    public UIPanel lobbyUIView;
    public GameObject lobbyModel;
    
    public CommandButton[] commandButtons;
    public UIScrollView commandButtonsScrollView;
    public UIGrid commandButtonsScrollGrid;

    
    // hp바 구할때 반복되는 변수라 위에 설정함.
    Vector2 pos;

    /// Command를 위한 변수들
    public GameObject confirmUI;
    Command previousCommand;
    MoveCommand moveCommand;

    Command _curCommand;
    public Command curCommand
    {
        get { return _curCommand; }
        set
        {
            if (_curCommand != null && _curCommand != value)
            {
                _curCommand.Cancel();
            }

            /// previousCommand는 항상 moveCommand로 고정시킨다
            /// 어떤 커맨드를 취소시키면 무조건 무브 커맨드 롤백시키기 위함
            previousCommand = moveCommand;

            _curCommand = value;
            curcommandIndicator.text = _curCommand.GetType().ToString();
        }
    }

    ///  HUDText
    [SerializeField]
    HUDText[] hudTexts;
    [System.NonSerialized]
    int hudTextIndex = 0;
    [SerializeField]
    Transform[] hudTextFollowTargets;

    private void Awake()
    {
        instance = this;
    }


    private void Start()
    {
        _gameManager = FindObjectOfType<GameManager>();
        dayAndNight = FindObjectOfType<DayAndNight>();
        tileChunk = FindObjectOfType<TileChunk>();
        tileChunk.OnClickedTile += OnClickedTile;

        moveCommand = FindObjectOfType<MoveCommand>();
        _curCommand = moveCommand;

        _gameManager.OnChangedPlayerTurn += OnChangedPlayerTurn;
        _gameManager.OnSelectedAnimal += OnSelectedAnimal;
        _gameManager.OnSetTargetAnimal += OnSetTargetAnimal;

        commandButtonsScrollGrid.onReposition += OnCommandButtonsScrollGridReposition;
    }

    bool isClockRing = false;
    const int RINGTIME = 5;
    Color gauageColor = new Color(1, 0f, 0f, 1);
    Coroutine uiShakeCoroutine;
    private void Update()
    {
        // 타임바
        // 이 코드는 코루틴으로 대체 가능 
        if (!turnManager.IsTimeOver && !turnManager.IsPaused)
        {
            // 타임 텍스트
            if (this.turnManager.Turn > 0)
            {
                if(this.turnManager.RemainingSecondsInTurn > RINGTIME)
                {
                    //this.TimeText.text = ((int)this.turnManager.RemainingSecondsInTurn).ToString();
                    if (isClockRing)
                    {
                        //this.TimeText.fontSize = 20;
                        //this.TimeText.color = Color.yellow;
                        GetComponent<AudioSource>().Stop();
                        isClockRing = false;
                        TimerFillImageSprite.spriteName = "Bar_05";
                        TimerFillImageSprite.color = Color.white;
                        StopCoroutine(uiShakeCoroutine);
                       // TimerFillImageSprite.applyGradient = false;
                    }
                }
                else
                {
                    //this.TimeText.text = this.turnManager.RemainingSecondsInTurn.ToString("F1");
                    if (!isClockRing)
                    {
                        //this.TimeText.fontSize = 40;
                        //this.TimeText.color = Color.red;
                        GetComponent<AudioSource>().PlayOneShot(GameManager.instance.clockSound, 1.0f);
                        uiShakeCoroutine = StartCoroutine(UIShake(TimerClock, 5, 60, RINGTIME));
                        isClockRing = true;
                        TimerFillImageSprite.spriteName = "Bar_01";
                        TimerFillImageSprite.color = gauageColor;
                    }
                }
                 
                // 타임 게이지바 표현 
                TimerFillImageSlider.value = this.turnManager.RemainingSecondsInTurn / this.turnManager.TurnDuration;               
            }
        }

        // Disconnect Panel
        if (PhotonNetwork.inRoom)
        {
            // 순간적인 인터넷 끊김으로 인해 타임아웃됐을 경우 재연결 가능하게 만드는 UI 활성 유무
            // 만약 그럴 떄 들어오는 콜백 함수 있다면 대체 가능
            if (PhotonNetwork.connected && _disconnectedPanel.gameObject.GetActive())
            {
                _disconnectedPanel.gameObject.SetActive(false);
            }
            if (!PhotonNetwork.connected && !PhotonNetwork.connecting && !_disconnectedPanel.gameObject.GetActive())
            {
                _disconnectedPanel.gameObject.SetActive(true);
            }
        }
    }

    public override void OnJoinedRoom()
    {
        RefreshUIViews();
    }

    void OnSelectedAnimal(Animal animal)
    {
        SetSelctionArrow(animal);
    }

    void OnSetTargetAnimal(Animal animal)
    {
        SetSelctionArrow(animal);
    }


    void OnClickedTile(Tile tile)
    {
        RefreshTileInfoUI(tile);
        RefreshAnimalInfoUI(tile.owningAnimal);
    }

    void ResetSelectionArrow()
    {
        selectionArrow.SetActive(false);
    }

    void SetSelctionArrow(Animal animal)
    {
        if (animal == null)
        {
            ResetSelectionArrow();
            return;
        }
        selectionArrow.GetComponent<UIFollowTarget>().enabled = true;
        selectionArrow.GetComponent<UIFollowTarget>().target = animal.UIPositionTransform.transform;
        selectionArrow.GetComponentInChildren<UISprite>().spriteName = animal.ownerUserId == PhotonNetwork.player.UserId ? "selectionArrow_cyan" : "selectionArrow_red";
        selectionArrow.SetActive(true);
    }

    public void OnClickWith(Animal animal)
    {
        //RefreshTileInfoUI(animal.ownedTile);
        //RefreshAnimalInfoUI(animal);
        //RefreshSkillList(animal);
    }


    void OnChangedPlayerTurn(string turnUserId)
    {
        CloseConfirmUI();
        
        enemyTurnText.text = PhotonNetwork.player.UserId + "의 턴       ";
        enemyTurnText.gameObject.SetActive(turnUserId != GameManager.instance.userIDForSimulation_origin);

        ResetTargetUI();
    }


    // UITileInfo의 내용을 갱신
    public void RefreshTileInfoUI(Tile tile)
    {
        if (tile == null)
        {
            return;
        }
        /*
        tileInfoTypeLabel.text = tile.type.ToString();
        if(tile.tileSeason == Tile.SeasonEnum.NORMAL)
        {
            tileInfoSeasonSprite.spriteName = "icon_normal";
        }
        else if (tile.tileSeason == Tile.SeasonEnum.DRAUGHT)
        {
            tileInfoSeasonSprite.spriteName = "icon_draught";
        }
        else 
        {
            tileInfoSeasonSprite.spriteName = "icon_rainy";
        }

        if(tile.type == TileMaker.TileType.RIVER)
        {
            tileInfoGrassWaterSprite.spriteName = "icon_water";
            tileInfoGrassWaterCountLabel.text = tile.water.ToString();
        }

        else 
        {
            tileInfoGrassWaterSprite.spriteName = "icon_grass";
            tileInfoGrassWaterCountLabel.text = tile.grass.ToString();
        }

        tileInfoMeatCountLabel.text = tile.meat.ToString();*/
    }


    public void RefreshAnimalInfoUI(Animal animal)
    {
        if (animal == null)
        {
            return;
        }

        animalInfoSprite.spriteName = "Portr_" + animal.species.ToString();
        // 동물 레벨 반영
        animalInfoHpLabel.text = animal.curHealth.ToString() + " / " + animal.animalData.health.ToString();
    }


    /// <summary>
    /// 방이 접속되거나 턴이 종료됐을 때 등 꽤 중요한 순간에만 호출된다
    /// </summary>
    public void RefreshUIViews()
    {
        // RedFillImage의 AnchorMax를 왼쪽으로 땡겨서 타이머 게이지바 UI를 왼쪽으로 돌려놓는다
        ConnectUiView.gameObject.SetActive(!PhotonNetwork.inRoom);
        GameUiView.gameObject.SetActive(PhotonNetwork.inRoom);
        lobbyUIView.gameObject.SetActive(!PhotonNetwork.inRoom);
        lobbyModel.gameObject.SetActive(!PhotonNetwork.inRoom);
 
        RefreshPlayerTexts();

        RefreshDayAndNightText();

        int remainingTurn = GameManager.LIMIT_GAME_TURN - PhotonNetwork.room.GetTurn();
        if (remainingTurn >= 0)
        {
            remainingTurnText.text = "Remaining Turn : " + (remainingTurn).ToString();
        }
        else
        {
            remainingTurnText.text = "OverTime !!!";
        }
    }

    void RefreshPlayerTexts()
    {
        // 플레이어들의 이름, 스코어 띄우는 함수 

        // GetNext()로 다음 플레이어를 가져올 수 있네
        PhotonPlayer remote = PhotonNetwork.player.GetNext();
        PhotonPlayer local = PhotonNetwork.player;

        if (PhotonNetwork.offlineMode)
        {
            this.LocalPlayerText.text = _gameManager.userIDForSimulation_origin;
            this.RemotePlayerText.text = _gameManager.userIDForSimulation_another;

            if (_gameManager.score.ContainsKey(_gameManager.userIDForSimulation_origin))
            {
                LocalPlayerText.text += " , Score : " + _gameManager.score[_gameManager.userIDForSimulation_origin].ToString();
            }
            if (_gameManager.score.ContainsKey(_gameManager.userIDForSimulation_another))
            {
                RemotePlayerText.text += " , Score : " + _gameManager.score[_gameManager.userIDForSimulation_another].ToString();
            }
        }
        else
        {
            if (remote != null)
            {
                // should be this format: "name        00"
                this.RemotePlayerText.text = remote.NickName;// + "        " + remote.GetScore().ToString("D2");
            }
            else
            {
                // 상대방이 없다면 상대방 기다리고 있다고 띄우기 
                // this.TimeText.text = "";
                this.RemotePlayerText.text = "waiting for another player";
            }
            if (local != null)
            {
                // should be this format: "YOU   00"
                this.LocalPlayerText.text = local.NickName;//"YOU   " + local.GetScore().ToString("D2");
            }
        }  
    }


    public void OnClickedOKButton()
    {/*
        switch (_gameManager.stateEnum)
        {
            case GameManager.processStateEnum.BREED:
                breedManager.OnClickOKButton();
            break;
        }*/
        HideOKUI();
        HideCancelUI();
    }


    public void OnClickedCancelButton()
    {/*
        switch (_gameManager.stateEnum)
        {
            case GameManager.processStateEnum.BREED:
                 breedManager.CancelBreeding();
            break;
        }*/
        HideOKUI();
        HideCancelUI();
    }

    public void OnClickedFirstSkillButton()
    {
        _gameManager.SelectedAnimal.learnedSkill[0].Do();
    }

    public void OnClickedSecondSkillButton()
    {
        _gameManager.SelectedAnimal.learnedSkill[1].Do();
    }

    public void OnClickedThirdSkillButton()
    {
        _gameManager.SelectedAnimal.learnedSkill[2].Do();
    }

    public void ResetTargetUI()
    {
        targetSprite.SetActive(false);
        targetSprite.GetComponent<UIFollowTarget>().target = null;
    }

    // 단일타겟 크로스헤어 ui 
    public void SetTargetUi(Animal targetAnimal)
    {
        if (targetSprite.GetActive() == false)
        {
            targetSprite.SetActive(true);
        }

        targetSprite.GetComponent<UIFollowTarget>().target = targetAnimal.UIPositionTransform.transform;
        targetSprite.GetComponent<Animator>().SetTrigger("expandTrigger");
    }

    public void DisplayEatUI()
    {
        //eatButton.gameObject.SetActive(true);
    }


    public void HideEatUI()
    {
        //eatButton.gameObject.SetActive(false);
    }


    public void DisplayBreedUI()
    {
        //breedButton.gameObject.SetActive(true);
    }


    public void HideBreedUI()
    {
        //breedButton.gameObject.SetActive(false);
    }


    public void DisplayGrowUpUI()
    {
        //growUpButton.gameObject.SetActive(true);
    }


    public void HideGrowUpUI()
    {
        //growUpButton.gameObject.SetActive(false);
    }


    public void DisplayDrinkUI()
    {
        drinkButton.gameObject.SetActive(true);
    }


    public void HideDrinkUI()
    {
        drinkButton.gameObject.SetActive(false);
    }


    public void DisplayOKUI()
    {
        okButton.gameObject.SetActive(true);
    }


    public void HideOKUI()
    {
        okButton.gameObject.SetActive(false);
    }


    public void DisplayCancelUI()
    {
        cancelButton.gameObject.SetActive(true);
    }


    public void HideCancelUI()
    {
        cancelButton.gameObject.SetActive(false);
    }

    public void DisplaySkillUI(Animal animal)
    {
        skillTreeWidget.gameObject.SetActive(true);
        SetSkillButtons(animal);
    }

    public void HideSkillUI()
    {
        skillTreeWidget.gameObject.SetActive(false);
    }

    public void RefreshSkillList(Animal animal)
    {
        HideFirstSkillButton();
        HideSecondSkillButton();
        HideThirdSkillButton();

        if(animal.tier >= 1)
        {
            DisplayFirstSkillButton();
        }
        if (animal.tier >= 2)
        {
            DisplaySecondSkillButton();
        }
        if (animal.tier >= 3)
        {
            DisplayThirdSkillButton();
        }

        RefreshSkillUseButtons(animal);
    }

    public void RefreshSkillUseButtons(Animal animal)
    {
        // 스프라이트 변경
        for (int i = 0; i < animal.tier; i++)
        {
            skillUseButtons[i].normalSprite = animal.learnedSkill[i].skillName + "_icon";
            skillUseButtons[i].GetComponent<UILabel>().text = animal.learnedSkill[i].skillName;
        }
        
        // 패시브 액티브 구분해서 클릭가능여부 설정
    }

    public void DisplayFirstSkillButton()
    {
        skillUseButtons[0].gameObject.SetActive(true);
    }

    public void HideFirstSkillButton()
    {
        skillUseButtons[0].gameObject.SetActive(false);
    }

    public void DisplaySecondSkillButton()
    {
        skillUseButtons[1].gameObject.SetActive(true);
    }

    public void HideSecondSkillButton()
    {
        skillUseButtons[1].gameObject.SetActive(false);
    }

    public void DisplayThirdSkillButton()
    {
        skillUseButtons[2].gameObject.SetActive(true);
    }

    public void HideThirdSkillButton()
    {
        skillUseButtons[2].gameObject.SetActive(false);
    }

    public override void OnConnectionFail(DisconnectCause cause)
    {
        _disconnectedPanel.gameObject.SetActive(true);
    }


    public void EnableLearnUI()
    {
        learnButton.enabled = true;
        learnButton.defaultColor = Color.white;
        learnLabel.color = Color.red;
    }


    public void DisableLearnUI()
    {
        learnButton.enabled = false;
        learnButton.defaultColor = Color.grey;
        learnLabel.color = Color.black;
    }


    public void SetSkillButtons(Animal animal)
    {
        for(int i = 0; i < 6; i++)
        {
            skillButton[i].SetButtonColorAndSprite(animal);
        }
        
        for(int i = 0; i <  3; i++)
        {
            if(animal.tier == i)
            {
                skillContainer[i].color = Color.white;
            }
            else 
                skillContainer[i].color = Color.grey;
        }
    }


    public void ShowDescription()
    {
        skillDescription.text = selectedSkillButton.skill.description;
    }


    public void OnClickSkillButton(SkillButton skillButton)
    {
        selectedSkillButton = skillButton;
        ShowDescription();
        if (CheckProperSkillTier(skillButton))
        {
            EnableLearnUI();
        }
        else
        {
            DisableLearnUI();
        }
    }

    bool CheckProperSkillTier(SkillButton skillButton)
    {
        if (_gameManager.SelectedAnimal.tier == skillButton.buttonIndex / 2)
            return true;
        else
            return false;
    }

    public void OnClickGrowButton()
    {
        //_gameManager.stateEnum = GameManager.processStateEnum.GROWUP;
    }


    public void OnClickLearnButton()
    {/*
        _gameManager.stateEnum = GameManager.processStateEnum.IDLE;
        HideSkillUI();
        _gameManager.SelectedAnimal.NotifyGrowUp(selectedSkillButton.skill);*/
    }


    GameObject newPopUp;
    public IEnumerator PopUpUI(Animal animal, string spriteName, string message)
    {
        newPopUp = Instantiate(popUpPrefab);
        newPopUp.transform.SetParent(popUpContainer.transform);
        newPopUp.GetComponent<PopUpLabel>().Init(animal, spriteName, message);
        newPopUp.GetComponent<PopUpLabel>().PopUptoSky();
        yield return new WaitForSeconds(0.2f);
    }


    public void RefreshDayAndNightText()
    {
        switch (dayAndNight.currentTimeOfNature)
        {
            case DayAndNight.TimeOfNature.DAY:
                RemainingTurnOfSwitchingDayAndNightText.text = "Remaining Day Turn : " + dayAndNight.remainingTurnOfSwitchingTime.ToString();
                break;
            case DayAndNight.TimeOfNature.NIGHT:
                RemainingTurnOfSwitchingDayAndNightText.text = "Remaining Night Turn : " + dayAndNight.remainingTurnOfSwitchingTime.ToString();
                break;
        }
    }

/*
    public void DisplayDetectedUI()
    {
        DetectedTextUI.gameObject.SetActive(true);
    }
 */

    public void DisplayGameResultSprite(GameManager.ResultType result)
    {
        gameResultSprite.gameObject.SetActive(true);

        switch (result)
        {
            case GameManager.ResultType.LocalWin:
                gameResultSprite.spriteName = "IconResultWin";
                break;
            case GameManager.ResultType.LocalLoss:
                gameResultSprite.spriteName = "IconResultLoose";
                break;
            case GameManager.ResultType.Draw:
                gameResultSprite.spriteName = "IconResultDraw";
                break;
        }
    }


    public void ShowHUDText(string text, Color color, Vector3 position, bool independent = true)
    {
        hudTexts[hudTextIndex].Add(text, color, 0f);
        hudTextFollowTargets[hudTextIndex].position = position;

        if (independent)
        {
            IncreaseHUDTextIndex();
        }
    }

    /// 외부에서 HUDText를 비독립적으로 쓰다가, 같은 코드에서 새롭게 독립적으로 띄워야하는 타이밍이 있는데, 그럴 때를 위한 함수 
    public void IncreaseHUDTextIndex()
    {
        ++hudTextIndex;
        if (hudTextIndex >= hudTexts.Length) hudTextIndex = 0;
    }


    public CommandButton BorrowCommandButton(int buttonOrder, string iconName)
    {
        CommandButton unusedCommandButton = null;
        foreach (CommandButton commandButton in commandButtons)
        {
            if (!commandButton.isUsing) 
            {
                unusedCommandButton = commandButton;
                break;
            }
        }

        if (unusedCommandButton != null)
        {
            /// 커맨드버튼을 가져갈 때는 먼저 SetActive true를 해야만, 스크롤 뷰 갱신이 정상적으로 이루어진다 
            unusedCommandButton.isUsing = true;
            unusedCommandButton.gameObject.SetActive(true);
            unusedCommandButton.iconSprite.spriteName = iconName;
            unusedCommandButton.transform.SetSiblingIndex(buttonOrder);

            commandButtonsScrollGrid.repositionNow = true;
        }

        return unusedCommandButton;
    }


    public void ReturnCommandButton(CommandButton commandButton)
    {
        if (commandButton == null) return;

        commandButton.isUsing = false;
        commandButton.gameObject.SetActive(false);
    }


    /// ScrollView의 리셋은 반드시 UIGrid의 Reposition이 끝난 다음에 해야 제대로 작동한다 
    void OnCommandButtonsScrollGridReposition()
    {
        commandButtonsScrollView.ResetPosition();

        /// 뭔 이유 때문인지는 모르겠지만, CommandButton들 다시 자리를 잡아주면 Scene에서는 제대로 자리 잡힌걸로 나오는데 
        /// 렌더링은 이상한 곳에서 된다 
        /// 한번 껐다 키니까 제대로 자리 잡힌 곳에서 나오기 때문에, 이런 야매 코드를 시전해준다 
        foreach (CommandButton commandButton in commandButtons)
        {
            if (commandButton.isUsing)
            {
                commandButton.button.gameObject.SetActive(false);
                commandButton.button.gameObject.SetActive(true);
            }
        }
    }


    public void OpenConfirmUI(Command command)
    {
        curCommand = command;

        confirmUI.SetActive(true);

        /// 설명 띄우기 
        string commandName = I2.Loc.ScriptLocalization.Get(command.GetType().Name + "_name");
        string commandDesc = I2.Loc.ScriptLocalization.Get(command.GetType().Name + "_desc");
        confirmUI_name.text = commandName;
        confirmUI_desc.text = commandDesc;
        confirmUI_name.gameObject.SetActive(commandName.Length > 0);
        confirmUI_desc.gameObject.SetActive(commandDesc.Length > 0);
        confirmUI_bg.gameObject.SetActive(commandName.Length > 0 || commandDesc.Length > 0);
/*
        /// 커맨드 주체 동물에게 카메라 패닝 
        if (GameManager.instance.SelectedAnimal != null && command.GetType() != typeof(MoveCommand) && command.GetType() != typeof(AttackCommand))
        {
            /// 화면 중아보다 약간 위로 올려서 UI에 가리지 않게 한다 
            Vector3 cameraPannningPosition = GameManager.instance.SelectedAnimal.transform.position;
            //cameraPannningPosition.x -= Tile.TILE_WORLD_WIDTH * 0.5f;
            //cameraPannningPosition.z -= Tile.TILE_WORLD_HEIGHT * 0.5f;
            Camera.main.MoveTo(cameraPannningPosition);
        } */
    }

    IEnumerator UIShake(UISprite ui, float shakeSize = 2f, float shakeSpeed = 40f, float attackedShakeDuration = 0.8f)
    {
        /// Shake 연출
        float elapsed = 0f;

        float direction = -1f;
        // 물기 직전의 위치로 돌아가기 위해서.
        Vector3 beforePosition = ui.transform.localPosition;
        while (elapsed < attackedShakeDuration)
        {
            Vector3 destination = ui.transform.localPosition;

            /// 동물이 바라보는 기준으로 양옆 흔들기 
            Vector3 transformRight = ui.transform.right * direction * shakeSize;
            destination += transformRight;

            float distance = Vector3.Distance(destination, ui.transform.localPosition);
            while (distance > 0.1f && elapsed < attackedShakeDuration)
            {
                ui.transform.localPosition = Vector3.Lerp(ui.transform.localPosition, destination, Time.deltaTime * shakeSpeed);

                distance = Vector3.Distance(destination, ui.transform.localPosition);

                elapsed += Time.deltaTime;

                yield return null;
            }

            direction *= -1f;
        }

        ui.transform.localPosition = beforePosition;
    }


    public void CloseConfirmUI()
    {
        /// ConfirmUI 띄었다가 다른 동물 선택하면 ConseConfirmUI가 호출되는데 이때 커맨드가 다시 default로 돌아가도록 함
        curCommand = moveCommand;   

        confirmUI.SetActive(false);
    }


    public void OnClickConfirmUIButton(string buttonName)
    {
        switch (buttonName)
        {
            case "AcceptButton":
                curCommand.Do();
                break;
            case "CancelButton":
                curCommand.Cancel();
                break;
        }

        /// previousCommand는 항상 MoveCommand로 고정되어있다 
        /// Accept, Cancel 어떤 버튼을 눌러도 MoveCommand로 전환된다
        if (previousCommand != null)
        {
            previousCommand.Ready();
        }

        curCommand = previousCommand;

        confirmUI.SetActive(false);
    }
}


public abstract class Command : MonoBehaviour
{
    public string buttonSpriteName;
    public int buttonOrder;

    [System.NonSerialized]
    protected CommandButton commandButton;

    protected enum CommandState { NONE, DISABLE, READY, ACTIVE, ACTIVATED }

    private CommandState _state = CommandState.NONE;
    protected CommandState state
    {
        get { return _state; }
        set { _state = value; OnStateChanged(value); }
    }

    protected virtual void OnStateChanged(CommandState changedState)
    {
        if (commandButton == null) 
        {
            commandButton = UIManager.instance.BorrowCommandButton(buttonOrder, buttonSpriteName);
            EventDelegate.Add(commandButton.button.onClick, OnClickButton);
        }

        switch (changedState)
        {
            case CommandState.NONE:
                {
                    UIManager.instance.ReturnCommandButton(commandButton);
                    EventDelegate.Remove(commandButton.button.onClick, OnClickButton);
                    commandButton = null;
                }
                break;
            case CommandState.DISABLE:
                {
                    commandButton.button.isEnabled = false;
                    commandButton.iconSprite.color = commandButton.button.disabledColor;
                }
                break;
            case CommandState.ACTIVE:
                {
                    commandButton.button.isEnabled = true;
                    /// 원인을 모르겠지만, 처음 ResetDefaultColor를 호출하면 mStartingColor가 0,0,0,0 검은색으로 설정되는 경우가 간헐적으로 생긴다 
                    /// 다른 부분에서 다시 ResetDefaultColor를 하면 괜찮아지지만 원인을 모르겠다 
                    /// Awake, Start를 호출 안해서 생기는 문제도 아니다 
                    /// 버튼의 SetActive(false)를 초반에 호출하지 않게 하면 이 현상이 드물어지긴 하지만, 완전히 사라지진 않았다 
                    /// 그래서 임시로 Color.white를 대입해서 땜빵한다 
                    //commandButton.button.ResetDefaultColor(); 
                    commandButton.button.defaultColor = Color.white;
                    commandButton.iconSprite.color = commandButton.button.defaultColor;
                }
                break;
            case CommandState.READY:
                {
                    commandButton.button.isEnabled = true;
                    commandButton.button.defaultColor = commandButton.button.pressed;
                    commandButton.iconSprite.color = commandButton.button.disabledColor;
                }
                break;
            case CommandState.ACTIVATED:
                {
                    commandButton.button.isEnabled = true;
                    commandButton.button.defaultColor = Color.black;
                    commandButton.iconSprite.color = Color.yellow;
                }
                break;
        }
    }


    public abstract void Ready();
    public abstract void Do();
    public abstract void Cancel();

    public virtual void OnClickButton() {}
}