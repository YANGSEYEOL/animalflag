﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Photon;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class ResourceManager : Photon.PunBehaviour
{
    public GameManager gameManager;
    public TileMaker tileMaker;
    public TileChunk tileChunk;
    public UIManager uiManager;
    public static readonly string TILE_RESOURCE_PROP_KEY = "TileResource";
    public static readonly string TILE_RESOURCE_WATER_PROP_KEY = "TileResource_Water";
    public static readonly string TILE_RESOURCE_GRASS_PROP_KEY = "TileResource_Grass";
    public static readonly string TILE_RESOURCE_MEAT_PROP_KEY  = "TileResource_MEAT";

    bool isResourceInfoReceived = false;

    void Start()
    {
        GetComponent<PhotonView>().viewID = 3;
    }

    void Update()
    {
        // 현재는 마스터클라이언트가 Y를 누르면 자원처리를 하도록했다. 
        // 후에는 몇번의 턴이 끝날때 이 함수가 호출되도록해야 할 것이다.
        if (PhotonNetwork.isMasterClient)
        {
            if (Input.GetKeyDown(KeyCode.Y))
            {
                photonView.RPC("ProcessEachTileResource", PhotonTargets.All);
                if (gameManager.ClickedTile != null)
                  uiManager.RefreshTileInfoUI(gameManager.ClickedTile);
            }
        }
    }

    public void EatFood()
    {/*
        Animal animal = gameManager.SelectedAnimal;
        Tile tile = animal.ownedTile;
        if(animal.animalData.diet.Key == "Grass")
        {
            tile.grass -= animal.animalData.appetiteDiet;
        }
        else
        {
            tile.meat -= animal.animalData.appetiteDiet;
        }
        uiManager.RefreshTileInfoUI(animal.ownedTile);
        uiManager.HideEatUI();*/
    }

    public void DrinkWater()
    {
        Animal animal = gameManager.SelectedAnimal;
        Tile tile = animal.ownedTile;
        //tile.water -= animal.animalData.appetiteWater;
        uiManager.RefreshTileInfoUI(animal.ownedTile);
    }

    // 우기건기처리
    // 각 타일의 정보를 처리한다음에 마스터클라이언트는
    // 포톤프로퍼티에 각 타일의 자원상황을 기록해놓아야한다.
    [PunRPC]
    public IEnumerator ProcessEachTileResource()
    {
        foreach (Tile tile in tileChunk.tiles)
        {
            //tile.ProcessSeasonEffect();   // 로컬에 각 타일의 계절정보를 처리하여 자원을 업데이트한다.
        }

        if (PhotonNetwork.isMasterClient)
        {
            //SendTileEntireResourceInfo();
        }
        //SendUpdatedResourceInfo();
        yield return null;
    }

    void SendTileEntireResourceInfo()
    {
        Hashtable tileProp = new Hashtable();
        Hashtable tileEntireResources = new Hashtable();
        
        Hashtable TileWaterProp = new Hashtable();
        Hashtable TileGrassProp = new Hashtable();
        Hashtable TileMeatProp  = new Hashtable();

        // 자기가 가지고있는 모든 비타일의 정보를 보내줌 -> 우기지역이 여러곳이더라도 문제가 없겠넹
        for (int i = 0; i < TileMaker.instance.tileSize * TileMaker.instance.tileSize; i++)
        {
            int tileIndexX = i % TileMaker.instance.tileSize;
            int tileIndexY = i / TileMaker.instance.tileSize;/*
            TileWaterProp[i] = tileChunk.tiles[tileIndexX, tileIndexY].water;
            TileGrassProp[i] = tileChunk.tiles[tileIndexX, tileIndexY].grass;
            TileMeatProp[i]  = tileChunk.tiles[tileIndexX, tileIndexY].meat;*/
        }
        tileEntireResources[TILE_RESOURCE_WATER_PROP_KEY] = TileWaterProp;
        tileEntireResources[TILE_RESOURCE_GRASS_PROP_KEY] = TileGrassProp;
        tileEntireResources[TILE_RESOURCE_MEAT_PROP_KEY]  = TileMeatProp;

        tileProp[TILE_RESOURCE_PROP_KEY] = tileEntireResources;
        PhotonNetwork.room.SetCustomProperties(tileProp);
    }


    public override void OnPhotonCustomRoomPropertiesChanged(Hashtable propertiesThatChanged)
    {
        if (propertiesThatChanged.ContainsKey(TILE_RESOURCE_PROP_KEY))
        {
            SyncTileResourcesByUsingTilesProperties();
        }
    }

    // 기존의 우기 타일들을 건기 타일들로바꿔주고
    // 새로 받은 배열로 
    void SyncTileResourcesByUsingTilesProperties()
    {
        //// 타일 프로퍼티 맞춰줌...
        //if (!isResourceInfoReceived)
        //{
        //    // 마스터클라이언트가 올려놓은 각 타일의 자원프로퍼티를 토대로 다른 클라이언트들은 자신의 타일들의 자원을 업데이트시킨다
        //        if (!PhotonNetwork.isMasterClient)
        //        {           
        //           Hashtable tileResourceProp = (Hashtable)PhotonNetwork.room.CustomProperties[TILE_RESOURCE_PROP_KEY];
        //           Hashtable tileResourceWaterProp = (Hashtable)tileResourceProp[TILE_RESOURCE_WATER_PROP_KEY];
        //           Hashtable tileResourceGrassProp = (Hashtable)tileResourceProp[TILE_RESOURCE_GRASS_PROP_KEY];
        //           Hashtable tileResourceMeatProp = (Hashtable)tileResourceProp[TILE_RESOURCE_MEAT_PROP_KEY];

        //           int tileIndexX = 0;
        //           int tileIndexY = 0;
 
        //            for (int i = 0; i < TileMaker.instance.tileSize * TileMaker.instance.tileSize; i++)
        //            { 
        //             tileIndexX = i % TileMaker.instance.tileSize;
        //             tileIndexY = i / TileMaker.instance.tileSize;
        //             tileChunk.tiles[tileIndexX, tileIndexY].water = (int)tileResourceWaterProp[i];
        //             tileChunk.tiles[tileIndexX, tileIndexY].grass = (int)tileResourceGrassProp[i];
        //             tileChunk.tiles[tileIndexX, tileIndexY].meat  = (int)tileResourceMeatProp[i];
        //            }
        //        }
        //    // isResourceInfoReceived = true;
        //}
    }
}
