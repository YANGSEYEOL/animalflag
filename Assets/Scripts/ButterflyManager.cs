﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ButterflyManager : MonoBehaviour {

    public static ButterflyManager instance;

    [SerializeField]
    GameObject butterflyPrefab;
    GameObject[] butterflies;
    [SerializeField]
    GameObject butterflyGroup;

    const int butterflyCount = 3;

    public List<Vector3> flowersPos;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        flowersPos = new List<Vector3>();
        butterflies = new GameObject[butterflyCount];

        TileChunk.instance.OnCreatedTiles += CreateButterflies;
     //   TileChunk.instance.OnCreatedTiles += Fly;

    }

    void Fly()
    {
        for(int i = 0; i < butterflies.Length; i++)
        {
            butterflies[i].GetComponent<Butterfly>().ButterflyFlyToFlower();
        }
    }

    Vector3 GetRandomPosInMap()
    {
        Vector3 pos = new Vector3();
        pos.x = Random.Range(1, TileChunk.instance.tileSizeWidth * Tile.TILE_WORLD_WIDTH);
        pos.y = 0;        
        pos.z = Random.Range(-TileChunk.instance.tileSizeHeight * Tile.TILE_WORLD_HEIGHT, -1);
        return pos;
    }

    void CreateButterflies()
    {
        for (int i = 0; i < butterflyCount; i++)
        {
            butterflies[i] = (GameObject)Instantiate(butterflyPrefab, GetRandomPosInMap(), Quaternion.identity);
            butterflies[i].transform.rotation = Quaternion.AngleAxis(Random.Range(0, 90), Vector3.up);
            butterflies[i].transform.position = new Vector3(butterflies[i].transform.position.x, 5, butterflies[i].transform.position.z);
            butterflies[i].transform.SetParent(butterflyGroup.transform);
        }
    }


}
