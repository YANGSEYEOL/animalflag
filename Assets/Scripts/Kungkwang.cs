﻿using UnityEngine;
using System.Collections;

public class Kungkwang : Skill
{
    public override void Init(Animal _ownAnimal)
    {
        ownAnimal = _ownAnimal;
        InitSkillDescription();
        InitSkillName();
    }

    public override void InitSkillDescription()
    {
        description = "This is Kungkwang";
    }

    public override void InitSkillName()
    {
        skillName = "Kungkwang";
    }

    public override void InitIsActive()
    {
        isActive = true;
    }

    public override void Do()
    {
        Debug.Log(ownAnimal.name + "used a Kungkwang skill");
    }
}
