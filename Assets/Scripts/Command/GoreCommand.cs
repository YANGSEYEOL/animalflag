﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using I2.Loc;

// 장전해놓으면 다음턴에 빈 타일로 돌진한다. 가는 경로에 있는 모든 동물들은 1턴 스턴에걸린다.
public class GoreCommand : Command
{
    readonly int TARGET_RANGE = 4;
    readonly float GORE_SPEED = 2.5f;
    readonly int BASIC_DAMAGE = 1;
    readonly float FLYING_TIME_SCALE = 0.18f;
    public static GoreCommand instance;

    [SerializeField]
    AudioClip stompSound;

    Animal _ownAnimal;
    Animal ownAnimal
    {
        get { return _ownAnimal; }
        set
        {
            if (_ownAnimal != null)
            {
                _ownAnimal.OnFinishedMove -= OnFinishedMove;
                _ownAnimal.OnChangedCanAction -= OnChangedCanAction;
            }
            _ownAnimal = value;
            if (_ownAnimal != null)
            {
                _ownAnimal.OnFinishedMove += OnFinishedMove;
                _ownAnimal.OnChangedCanAction += OnChangedCanAction;
            }
        }
    }
    int targetIndex;
    List<Animal> validTargetAnimals;
    Tile[] goalTiles;
    Animal _targetAnimal;
    Animal targetAnimal
    {
        get { return _targetAnimal; }
        set
        {
            _targetAnimal = value;
            if (value == null)
                UIManager.instance.ResetTargetUI();
            else
                UIManager.instance.SetTargetUi(value);
        }
    }

    bool canGore { get { return ownAnimal != null && ownAnimal.animalData.skills.Contains("Gore") && ownAnimal.canAction; } }

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        GameManager.instance.OnSelectedAnimal += OnSelectedAnimal;
        Init();
    }

    public void Init()
    {
        validTargetAnimals = new List<Animal>();
        goalTiles = new Tile[4];
    }

    // 타겟이 선택가능한가?
    // 중간에 걸림돌이 없어야 한다. 그리고 일정 거리이상 멀어진 대상은 안된다.
    bool IsProperTarget(Animal targetAnimal)
    {
        for (int i = 0; i < 4; i++)
        {
            if (targetAnimal == validTargetAnimals[i])
            {
                targetIndex = i;
                return true;
            }
        }

        return false;
    }

    // 내가 선택할 수 있는 타일을 모두 구해서 Vectorline으로 보여준다.
    public void GetTargets(Animal ownAnimal)
    {
        if (ownAnimal == null)
            return;

        validTargetAnimals.Clear();
        for(int i = 0; i < 4; i++)
            goalTiles[i] = null;
        if(ownAnimal.ownedTile.type == TileMaker.TileType.RIVER)
        {
            return;
        }

        int tileX = ownAnimal.ownedTile.TileIndex_X;
        int tileY = ownAnimal.ownedTile.TileIndex_Y;
        List<int> x = new List<int>();
        List<int> y = new List<int>();
        int a = 1;
        int b = 0;
        int phase = 1;
        for (int i = 0; i < TARGET_RANGE * 2; i++)
        {
            x.Add(a);
            y.Add(b);
            a += phase;
            if (a > TARGET_RANGE)
            {
                a = -1;
                phase = -1;
            }
        }
        for (int i = 0; i < TARGET_RANGE * 2; i++)
        {
            x.Add(y[i]);
            y.Add(x[i]);
        }

        int targetIndex = 0;
        for (int i = 0; i < TARGET_RANGE * 4; i++)
        {
            if (TileChunk.instance.IsValidTileIndex(tileX + x[i], tileY + y[i]))
            {
                Tile targetAnimalTile = TileChunk.instance.tiles[tileX + x[i], tileY + y[i]];
                if (!targetAnimalTile.isRock)
                {
                    // 타겟동물이 없을 때,  
                    if (targetAnimalTile.owningAnimal == null)
                    {
                        if(targetAnimalTile.type == TileMaker.TileType.RIVER)
                        {
                            // 돌진 방향을 바꾸는 인덱스조절  
                            i += (TARGET_RANGE - (i % TARGET_RANGE));
                            i--;
                        }
                        continue;
                    }
                        
                    // 타겟동물로 스킵하는 조건 
                    /// 1. 우리동물
                    /// 2. 은신중인동물
                    /// 3. 타겟의 시야가 밝혀지지않은 방향
                    /// 4. 강을 건널수없다
                    if (!GameManager.instance.enemyAnimals.Contains(targetAnimalTile.owningAnimal) ||
                        !targetAnimalTile.isVisible || 
                        targetAnimalTile.type == TileMaker.TileType.RIVER)
                    {
                        i += (TARGET_RANGE - (i % TARGET_RANGE));
                        i--;
                        continue;   // 4방향중 이 방향은 타겟에 포함시키지 않고 스킵함
                    }

                    // 은신중인동물은 방향을 바꾸는것이아니라 인덱스를 하나만늘려준다. 
                    if (targetAnimalTile.owningAnimal.isConcealed ||
                        targetAnimalTile.owningAnimal.isClimbedTree ||
                        targetAnimalTile.owningAnimal.isSubmerged)
                    {
                        continue;
                    }

                    // goalTile은 들이받기 동물이 도착하는 타일인데 이 타일을 한 배열에 다담아준다.
                    if (x[i] > 0)
                        goalTiles[targetIndex] = TileChunk.instance.tiles[tileX + x[i] - 1, tileY + y[i]];
                    else if (x[i] < 0)
                        goalTiles[targetIndex] = TileChunk.instance.tiles[tileX + x[i] + 1, tileY + y[i]];
                    else if (y[i] > 0)
                        goalTiles[targetIndex] = TileChunk.instance.tiles[tileX + x[i], tileY + y[i] - 1];
                    else if (y[i] < 0)
                        goalTiles[targetIndex] = TileChunk.instance.tiles[tileX + x[i], tileY + y[i] + 1];

                    validTargetAnimals.Add(targetAnimalTile.owningAnimal);
                    targetIndex++;
                    i += (TARGET_RANGE - (i % TARGET_RANGE));
                    i--;
                }
                else
                {
                    i += (TARGET_RANGE - (i % TARGET_RANGE));
                    i--;
                }
            }
        }
    }

    // 이 동물이 선택되면 스킬버튼을 띄워야 한다.
    void OnSelectedAnimal(Animal animal)
    {
        ownAnimal = animal;

        SwitchState();
    }

    void OnFinishedMove(Animal animal)
    {
        SwitchState();
    }

    void OnChangedCanAction(bool canAction)
    {
        SwitchState();
    }

    public override void OnClickButton()
    {
        if (state == CommandState.READY)
        {
            targetIndex++;  // 타겟 리스트에서 자동으로 타겟설정
            if (validTargetAnimals.Count <= targetIndex)
            {
                targetIndex = 0;
            }
            targetAnimal = validTargetAnimals[targetIndex];
        }
        else
        {
            GetTargets(ownAnimal); // 타겟 리스트구하기
            targetIndex = 0;

            // 기본으로 첫번째 대상을 선택해주기
            if(validTargetAnimals.Count > 0 )
                targetAnimal = validTargetAnimals[targetIndex];
            
            state = CommandState.READY;
        }
        UIManager.instance.OpenConfirmUI(this);
    }

    public override void Cancel()
    {
        //foreach (var target in validTargetAnimals)
        //{
        //    target.SetTargetHighLightVisible(false);
        //}
        targetAnimal = null;

        SwitchState();
    }

    public override void Do()
    { 
        ownAnimal.canAction = false;
        GameManager.instance.OnAnimalDidAction(ownAnimal);
        StartCoroutine(Gore(ownAnimal,targetAnimal, goalTiles[targetIndex]));
        
        // 밑에 선택원을 지우기위해
        targetAnimal = null;
        state = CommandState.NONE;
    }

    public override void Ready()
    {

    }

    // 타일의 인덱스를 바탕으로 두 타일이 몇칸떨어져있는지 확인한다.
    public int GetDistance(int startIndex, int endIndex)
    {
        int distance = (endIndex - startIndex) >= TileChunk.instance.tileSizeWidth || (endIndex - startIndex) <= -TileChunk.instance.tileSizeWidth
        ? (endIndex - startIndex) / TileChunk.instance.tileSizeWidth : (endIndex - startIndex);
        distance = Mathf.Abs(distance);

        return distance;
    }

    public IEnumerator Gore(Animal ownAnimal,Animal targetAnimal, Tile goalTile)
    {
        UIManager.instance.ShowHUDText(I2.Loc.ScriptLocalization.Get(GetType().Name + "_name"), Color.yellow, ownAnimal.transform.position + Animal.DAMAGE_TEXT_POS_FACTOR);
        yield return new WaitForSeconds(0.7f);
        
        int[] path;
        // 동물의 처음위치
        int startIndex = ownAnimal.ownedTile._tileIndex;
        // 들이받고나서 최종적으로 도달하는 위치(타겟의 바로 옆타일)
        int endIndex = goalTile._tileIndex;
        // 내동물과 타겟동물 사이의 거리
        int distance = GetDistance(ownAnimal.ownedTile._tileIndex, targetAnimal.ownedTile._tileIndex);

        path = new int[distance + 1];
        path[0] = distance; // 타일 경로 개수

        // path구하는 공식
        if (distance == 1)
        {
            path[1] = startIndex;
        }
        else
        {
            for (int i = distance; i > 0; i--)
            {
                path[distance - i + 1] = (endIndex - startIndex) / (distance - 1) * (i - 1) + startIndex;
            }
        }
        
        /// 위치값차이 타겟 - 나 자신
        var targetLookVector = targetAnimal.ownedTile.position - ownAnimal.ownedTile.position;
        var lookRotation = Quaternion.LookRotation(targetLookVector);
        ownAnimal.transform.rotation = lookRotation;

        // disdtance가 1인경우는 제자리 움직임이다. move함수를 호출하지않고 그냥 제자리에서 회전만시키자
        bool areThereEnemiesAttackSkills= false;
        if (distance > 1)
        {
            // 들이받기 위해 달려가는모션 path따라 달려감
            ownAnimal.movingAnimSpeed = GORE_SPEED;
            
            /// 경로에 적 동물의 대기 공격 스킬이 있는지 체크 

            ExitGames.Client.Photon.Hashtable attackedQueue;
            int changedDestinationPathIndex;

            ownAnimal.CheckEnemiesAttackSkillsInPath(ref path, out areThereEnemiesAttackSkills, out attackedQueue, out changedDestinationPathIndex);

            /// 체크된게 있으면 돌리고, 체크되는게 없다면 일반 Move를 호출한다 
            IEnumerator moveCoroutine = null;
            if (areThereEnemiesAttackSkills)
            {
                moveCoroutine = ownAnimal.StartAttackedQueue(attackedQueue);
            }
            else
            {
                moveCoroutine = ownAnimal.Move(path, changedDestinationPathIndex);
            }
            yield return moveCoroutine;
        }

        ownAnimal.movingAnimSpeed = 1;

        // 내가 들이받으러 달려가는도중에 어떤동물이 있다면 아래 코드를 실행할 수 없다. 그냥 제자리멈춰서 반격을 당할것이다.
        if (areThereEnemiesAttackSkills)
        {
            yield break;
        }

        ownAnimal.ownedTile = goalTiles[targetIndex];

        /// 첫번째 타겟으로 타격 사운드 
        AudioClip atkSound = GameManager.instance.attackSound_critical[Random.Range(0, GameManager.instance.attackSound_critical.Length)];
        ownAnimal.audioSource.PlayOneShot(atkSound, 0.4f);

        /// 공격 연출 시작 
        ownAnimal.animator.SetTrigger("goreTrigger");

        AnimationClip goreClip = ownAnimal.GetAnimationClip("gore");

        yield return new WaitForSeconds(goreClip.length * 0.5f);

        // bite 풀리도록 들이받는순간 호출
        GameManager.instance.OnAnimalGored(targetAnimal);

        /// 이펙트
        AttackEffect.instance.Stop();
        AttackEffect.instance.Play(targetAnimal.transform.position, AttackCommand.HitResult.HIT);

        // 우선 tileToFly를 타겟동물의 타일로 초기화해놓은다.
        Tile tileToFly = targetAnimal.ownedTile;

        // flyRange는 처음에 나와 타겟의 거리이다. distance는 바로옆칸일때 0임
        int flyRange = distance;

        int indexX = targetAnimal.ownedTile.TileIndex_X;
        int indexZ = targetAnimal.ownedTile.TileIndex_Y;

        // 타겟이 바위나 또 다른 타겟에게 부딫힐 것이다. 이 위치를 secondTarget이라고 한다
        int secondTargetIndexX = -1;
        int secondTargetIndexZ = -1;

        // flyRange 까지 타일을 한칸씩 이동해가면서 타겟이 날라갈수있는 도착지점을 찾는작업
        for (int i = 1; i <= flyRange; i++)
        {
            // 0으로 나누지 않기 위해 0이 아닌지 체크해준다.
            if (targetLookVector.x != 0)
            {
                indexX = targetAnimal.ownedTile.TileIndex_X + i * +(int)(targetLookVector.x / Mathf.Abs(targetLookVector.x));
            }
            if (targetLookVector.z != 0)
            {
                indexZ = targetAnimal.ownedTile.TileIndex_Y + i * -(int)(targetLookVector.z / Mathf.Abs(targetLookVector.z));
            }  

            // 맵밖으로 나간다면 이제 이 위치에다가 동물을 날릴것이다.
            if (!TileChunk.instance.IsValidTileIndex(indexX, indexZ))
            {
                break;
            }
            // 바위나 동물을 만났을때 더 멀리 날라가지못하고 그 위치에 부딫힌다.
            else if(TileChunk.instance.tiles[indexX, indexZ].isRock ||
                     TileChunk.instance.tiles[indexX, indexZ].owningAnimal != null)
            {
                // 현재 indexX, indexZ는 secondTarget(바위나 또다른 동물)이 있는 위치다
                secondTargetIndexX = indexX;
                secondTargetIndexZ = indexZ;

                i--;
                // i를 하나 줄여서 다시 계산하면 타겟이 결국 도착하는 위치이다.
                if (targetLookVector.x != 0)
                {
                    indexX = targetAnimal.ownedTile.TileIndex_X + i * (int)(targetLookVector.x / Mathf.Abs(targetLookVector.x));
                }
                if (targetLookVector.z != 0)
                {
                    indexZ = targetAnimal.ownedTile.TileIndex_Y + i * -(int)(targetLookVector.z / Mathf.Abs(targetLookVector.z));
                }
                break;
            }
        }

        Animal.Direction direction = targetAnimal.GetDirectionOfTarget(ownAnimal);
        string goreFlyTrigger;
        switch (direction)
        {
            case Animal.Direction.FRONT:
                goreFlyTrigger = "goreFly_frontTrigger";
                break;
            case Animal.Direction.BEHIND:
                goreFlyTrigger = "goreFly_backTrigger";
                break;
            case Animal.Direction.SIDE:
                Vector3 cross = Vector3.Cross(ownAnimal.transform.forward, targetAnimal.transform.forward);
                if (cross.y < 0) // 대상의 왼쪽
                {
                    goreFlyTrigger = "goreFly_leftTrigger";

                }
                else
                {
                    goreFlyTrigger = "goreFly_rightTrigger";
                }

                break;
            default:
                goreFlyTrigger = null;
                break;
        }

        //  지금까지 계산한 indexX, indexZ가 맵 내부로 떨어진다면 낙사시키지않고 세컨드타겟이 있는지 확인하고 부딫히거나 그냥 날려보낸다
        if (TileChunk.instance.IsValidTileIndex(indexX, indexZ))
        {
            tileToFly = TileChunk.instance.tiles[indexX, indexZ];

            // 세컨드 타겟이 돌이나 다른동물이있다면 
            if (TileChunk.instance.IsValidTileIndex(secondTargetIndexX, secondTargetIndexZ) &&
                (TileChunk.instance.tiles[secondTargetIndexX, secondTargetIndexZ].isRock ||
                TileChunk.instance.tiles[secondTargetIndexX, secondTargetIndexZ].owningAnimal != null))
            {
                Vector3 secondTargetPos = (TileChunk.instance.tiles[secondTargetIndexX, secondTargetIndexZ].position + tileToFly.position) / 2;
                
                // 타겟과 세컨타겟 사이의 거리
                int flyingDistance = GetDistance(TileChunk.instance.tiles[secondTargetIndexX, secondTargetIndexZ]._tileIndex, 
                                                 targetAnimal.ownedTile._tileIndex);
                
                // 날라가는 동물을 눕혀서 흔드는효과
                targetAnimal.animator.SetBool("goreFly", true);
                targetAnimal.animator.SetTrigger(goreFlyTrigger);
                //targetAnimal.animator.speed = 0.5f;

                // 부딫히는 대상으로 타겟을 날려보낸다 
                yield return targetAnimal.MoveToDuring(secondTargetPos, FLYING_TIME_SCALE * flyingDistance);
                
                /// 이펙트
                AttackEffect.instance.Stop();
                AttackEffect.instance.Play(targetAnimal.transform.position, AttackCommand.HitResult.HIT);
                UIManager.instance.ShowHUDText((BASIC_DAMAGE + flyingDistance).ToString(), Color.yellow, targetAnimal.transform.position + Animal.DAMAGE_TEXT_POS_FACTOR);
                
                /// 두번째 타겟으로 타격 사운드 
                atkSound = GameManager.instance.attackSound_critical[Random.Range(0, GameManager.instance.attackSound_critical.Length)];
                ownAnimal.audioSource.PlayOneShot(atkSound, 0.4f);

                Animal secondTargetAnimal = TileChunk.instance.tiles[secondTargetIndexX, secondTargetIndexZ].owningAnimal != null ? 
                                            TileChunk.instance.tiles[secondTargetIndexX, secondTargetIndexZ].owningAnimal : null;

                // 세컨드 타겟이 rock이아니라 동물일때
                if(secondTargetAnimal)
                {
                   /// 세컨드타겟과 부딫히는 이펙트
                    AttackEffect.instance.Stop();
                    AttackEffect.instance.Play(secondTargetAnimal.transform.position, AttackCommand.HitResult.HIT);

                    UIManager.instance.ShowHUDText((BASIC_DAMAGE + flyingDistance).ToString(), Color.yellow, secondTargetAnimal.transform.position + Animal.DAMAGE_TEXT_POS_FACTOR);

                    // 세컨드타겟 동물을 흔드는효과
                    secondTargetAnimal.animator.SetBool("shake", true);
                    secondTargetAnimal.animator.SetTrigger("shakeTrigger");
                    secondTargetAnimal.animator.speed = 2;
                }

                targetAnimal.ownedTile = tileToFly;

                // 부딫히면서 반대로 떨어져야 하므로 goreFly를 false로 바꾸는 것이아니라 goreFly_collided를 true로 바꿈
                targetAnimal.animator.SetBool("goreFly_collided", true);

                // 부딫힌 위치에서 도착 타일로 돌아온다 
                yield return targetAnimal.MoveToRapidly(tileToFly.position);

                if (secondTargetAnimal)
                {
                    // 세컨드 타겟동물 흔드는 효과를 멈춘다
                    secondTargetAnimal.animator.SetBool("shake", false);
                    secondTargetAnimal.animator.speed = 1;
                    
                    // 효과를 다 보여주기 위해서 동물체력감소효과를 늦게해준다.
                    secondTargetAnimal.curHealth -= BASIC_DAMAGE + flyingDistance;
                }

                targetAnimal.curHealth -= BASIC_DAMAGE + flyingDistance;

                yield return new WaitForSeconds(0.5f);

                if (secondTargetAnimal)
                {
                    StartCoroutine(secondTargetAnimal.Damaged(BASIC_DAMAGE + flyingDistance, AttackCommand.HitResult.HIT, false));
                }
                StartCoroutine(targetAnimal.Damaged(BASIC_DAMAGE + flyingDistance, AttackCommand.HitResult.HIT, false));

                // 모든 작업이 끝났으니 다시 goreFly, goreFly_collided 돌려놓는다.
                targetAnimal.animator.SetBool("goreFly", false);
                targetAnimal.animator.SetBool("goreFly_collided", false);
            }
            else // 날라가는 타일이 빈타일이라면
            {
                // 타겟이 날라가는 거리
                int flyingDistance = GetDistance(tileToFly._tileIndex,
                                                 targetAnimal.ownedTile._tileIndex);

                targetAnimal.ownedTile = tileToFly;
                targetAnimal.animator.SetBool("goreFly", true);
                targetAnimal.animator.SetTrigger(goreFlyTrigger);
                
                // 부딫히는 대상으로 타겟을 날려보낸다 
                yield return targetAnimal.MoveToDuring(tileToFly.position, FLYING_TIME_SCALE * flyingDistance);

                targetAnimal.animator.SetBool("goreFly", false);
            }

            // 물에 떨어졌다면 각도를 바꺼야겠지
            if (targetAnimal.curHealth > 0)
            {
                // 물에떨어졌을때 각도 바꿔주고 떨어지는 소리, 물결표시
                if (tileToFly.type == TileMaker.TileType.RIVER)
                {
                    targetAnimal.audioSource.PlayOneShot(GameManager.instance.waterSplashSound, 0.2f);
                    GameManager.instance.splashParticle.transform.position = new Vector3(targetAnimal.transform.position.x, -5, targetAnimal.transform.position.z);
                    GameManager.instance.splashParticle.Play();

                    targetAnimal.transform.Rotate(Vector3.right, targetAnimal.xAngle_fallingIntoWater);
           
                    yield return targetAnimal.MoveToRapidly(tileToFly.position_AdjustedTileYFactor + Vector3.up * targetAnimal.yFactor_fallingIntoWater);
                }
                else
                {
                    var comebackCoroutine = targetAnimal.MoveToRapidly(tileToFly.position_AdjustedTileYFactor);
                    while (comebackCoroutine.MoveNext()) yield return comebackCoroutine.Current;
                }
                
                targetAnimal.SetVisible(targetAnimal.ownedTile.isVisible);
            }

            // 타일이 바뀐뒤 시야를 갱신
            TileChunk.instance.RefreshFieldOfView();
        }
        else   // 맵밖으로 벗어난 인덱스인경우 -> 낙사
        {
            // 적이 날라가는 위치 = 기존 타겟동물의 위치 + 타겟과 이 동물의 위치차이
            Vector3 flyPosition = targetAnimal.transform.position + targetLookVector;
            
            // 날라가는 동물을 눕혀서 흔드는효과, 좀 느리게
            targetAnimal.animator.SetBool("goreFly", true);
            targetAnimal.animator.SetTrigger(goreFlyTrigger);
            targetAnimal.animator.speed = 0.5f;

            // 타겟을 맵 밖으로 날려보낸다 
            yield return targetAnimal.MoveToDuring(flyPosition, FLYING_TIME_SCALE * distance);

            // 밖으로 날라갈때는 goreFly가 false가 되면 땅으로 엎어지는 애니메이션이 발동하면 안되고
            // 곧바로 idle로 넘어가야 낙사하는 애니메이션이 자연스럽기 때문이 goreFly를 false로 전환해주지 않고 바로 fallDown을 true로 바꺼준다
            targetAnimal.animator.SetBool("fallDown", true);
            targetAnimal.animator.speed = 1;
            

            Vector3 fallDownPos = flyPosition;
            yield return targetAnimal.FallDownOutOfMap(fallDownPos);
        }
    }
    
    public void GetTargets(Animal animal, ref List<Animal> targetAnimals, ref Tile[] _goalTiles)
    {
        GetTargets(animal);
        targetAnimals = validTargetAnimals;
        _goalTiles = goalTiles;
    }

    protected virtual void SwitchState()
    {
        if (canGore)
        {
            GetTargets(ownAnimal);
            state = ownAnimal.curRemainSkillCoolTime <= 0 && validTargetAnimals.Count > 0 ? CommandState.ACTIVE : CommandState.DISABLE;   
        }
        else
        {
            state = CommandState.NONE;
        }
    }

    protected override void OnStateChanged(CommandState changedState)
    {
        base.OnStateChanged(changedState);

        switch (changedState)
        {
            case CommandState.NONE:
                {
                }
                break;
            case CommandState.DISABLE:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(ownAnimal.curRemainSkillCoolTime > 0);
                    commandButton.cooltimeLabel.text = ownAnimal.curRemainSkillCoolTime.ToString();
                }
                break;
            case CommandState.READY:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
            case CommandState.ACTIVATED:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
            case CommandState.ACTIVE:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
        }
    }
}