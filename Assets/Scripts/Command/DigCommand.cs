﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using I2.Loc;

// 사정거리가 엄청 길어진다. 시야에 들어오는 동물을 하나 좇아 물을 수 있다. 녹턴 궁 같이 
public class DigCommand : Command
{
    public static DigCommand instance;

    [SerializeField]
    ParticleSystem dirtParticle;

    [SerializeField]
    GameObject[] digHoles;

    Animal _ownAnimal;
    Animal ownAnimal
    {
        get { return _ownAnimal; }
        set
        {
            if (_ownAnimal != null)
            {
                _ownAnimal.OnFinishedMove -= OnFinishedMove;
                _ownAnimal.OnChangedCanAction -= OnChangedCanAction;
            }
            _ownAnimal = value;
            if (_ownAnimal != null)
            {
                _ownAnimal.OnFinishedMove += OnFinishedMove;
                _ownAnimal.OnChangedCanAction += OnChangedCanAction;
            }
        }
    }

    Vectrosity.VectorLine _vectorline;
    Vectrosity.VectorLine vectorline
    {
        get
        {
            if (_vectorline == null)
                _vectorline = TileChunk.instance.lineCyan;
            return _vectorline;
        }
    }

    Tile _targetTile;
    Tile targetTile {
        get
        {
            return _targetTile;
        }
        set
        {
            if (_targetTile != null)
            {
                vectorline.Resize(0);
                vectorline.Draw3D();
            }

            _targetTile = value;

            if (_targetTile != null)
            {
                vectorline.Resize(0);
                TileChunk.instance.AddVerticesIntoLinePointsByScale(_targetTile, 1.0f, vectorline);
                vectorline.Draw3D();
            }
        }
    }


    bool canDig { get { return ownAnimal != null && ownAnimal.animalData.skills.Contains("Dig") && ownAnimal.canAction; } }

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        GameManager.instance.OnSelectedAnimal += OnSelectedAnimal;
        TileChunk.instance.OnClickedTile += OnClickedTile;
        Init();
    }

    public void Init()
    {
        //for (int i = 0; i < digHoles.Length; i++)
        //{
        //    if (digHoles[i].GetComponent<Renderer>().material.renderQueue == 3000)
        //        digHoles[i].GetComponent<Renderer>().material.renderQueue += 2;
        //}
    }

    protected virtual void SwitchState()
    {
        if (ownAnimal == null)
            return;

        if (canDig)
        {
            state = ownAnimal.curRemainSkillCoolTime <= 0 ? CommandState.ACTIVE : CommandState.DISABLE;
        }
        else
        {
            state = CommandState.NONE;
        }
    }

    void OnClickedTile(Tile clickedTile)
    {
        if (state == CommandState.NONE)
            return;

        if (clickedTile.type == TileMaker.TileType.RIVER)
            return;
        
        // 해당 타일이 visible하지 않으면 타겟타일로 일단 가능해야한다.
        // 해당 타일에 동물이 은신중이면 그 타일을 선택할 수 있다.
        if (!clickedTile.isVisible || 
            (clickedTile.owningAnimal != null && !clickedTile.owningAnimal.isVisible) ||
            (clickedTile.isVisible && clickedTile.owningAnimal == null))
        {
            if ((state == CommandState.ACTIVATED || state == CommandState.READY))
            {
                targetTile = clickedTile;

                UIManager.instance.OpenConfirmUI(this);
            }   
        }
    }

    void OnFinishedMove(Animal animal)
    {
        SwitchState();
    }

    void OnChangedCanAction(bool canAction)
    {
        SwitchState();
    }

    // 이 동물이 선택되면 스킬버튼을 띄워야 한다.
    void OnSelectedAnimal(Animal animal)
    {
        ownAnimal = animal;

        SwitchState();
    }

    public override void OnClickButton()
    {
        UIManager.instance.curCommand = this;

        state = CommandState.READY;

        // 랜덤하게 타일이 아무렇게나 선택이 됨
        do {
            targetTile = TileChunk.instance.tiles[Random.Range(0, TileChunk.instance.tileSizeHeight), Random.Range(0, TileChunk.instance.tileSizeWidth)];
        }
        while (targetTile.type == TileMaker.TileType.RIVER);

        UIManager.instance.OpenConfirmUI(this);
    }

    public override void Cancel()
    {
        vectorline.Resize(0);
        vectorline.Draw3D();
        SwitchState();
    }
    
    public override void Do()
    {
        ownAnimal.canAction = false;

        vectorline.Resize(0);
        vectorline.Draw3D();

        GameManager.instance.OnAnimalDidAction(ownAnimal);
        StartCoroutine(Dig(ownAnimal, targetTile));
        
        state = CommandState.NONE;
    }

    public override void Ready()
    {

    }

    protected override void OnStateChanged(CommandState changedState)
    {
        base.OnStateChanged(changedState);

        switch (changedState)
        {
            case CommandState.NONE:
                {
                }
                break;
            case CommandState.DISABLE:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(ownAnimal.curRemainSkillCoolTime > 0);
                    commandButton.cooltimeLabel.text = ownAnimal.curRemainSkillCoolTime.ToString();
                }
                break;
            case CommandState.READY:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
            case CommandState.ACTIVATED:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
            case CommandState.ACTIVE:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
        }
    }

    public IEnumerator Dig(Animal animal, Tile targetTile)
    {
        UIManager.instance.ShowHUDText(I2.Loc.ScriptLocalization.Get(GetType().Name + "_name"), Color.yellow, animal.transform.position + Animal.DAMAGE_TEXT_POS_FACTOR);
        yield return new WaitForSeconds(1f);
        
        /// 타겟타일을 쳐다본다.
        var targetLookVector = targetTile.position - ownAnimal.ownedTile.position;
        var lookRotation = Quaternion.LookRotation(targetLookVector);
        ownAnimal.transform.rotation = lookRotation;

        // 1초 쉬었다가 dig애니메이션시작
        yield return new WaitForSeconds(1f);
        animal.GetComponent<Animator>().SetTrigger("digTrigger");
        animal.GetComponent<Animator>().SetBool("dig", true);

        // 흙튀기는 fx
        dirtParticle.transform.position = animal.transform.position;
        dirtParticle.Play();

        // 땅에 구멍
        digHoles[0].transform.position = animal.transform.position + Vector3.up;
        digHoles[0].SetActive(true);

        // dig 애니메이션 하는동안 잠깐멈춰준다
        yield return new WaitForSeconds(animal.GetAnimationClip("dig").length);

        ///////////////////땅파고 들어가기 끝/////////////////////////////////////
    
        // 카메라가 타겟타일로 이동한다
        Camera.main.MoveTo(targetTile.position, true);

        yield return new WaitForSeconds(1f);

        // 해당 타일만 절대시야에 추가하고 시야업데이트
        TileChunk.instance.absoluteSightTileTable[ownAnimal].Add(targetTile);
        TileChunk.instance.RefreshFieldOfView();

        // 타겟타일에 동물이 있었다면? 이 경우는 이 타일 시야가 안보일경우에만 해당한다.
        // 안보이는 타일에 동물이 있는지 없는지 모르고 쓰기 때문이다.
        // 이 타일에 동물이있다면 스킬시전에 실패하고 다시 제자리로 돌아온다.
        if (targetTile.owningAnimal != null)
        {
            /// 타겟 동물 놀라는 연출 
            animal.audioSource.PlayOneShot(GameManager.instance.surprisedSound, 0.4f);
            UIManager.instance.ShowHUDText("!", Color.yellow, targetTile.owningAnimal.transform.position + Animal.DAMAGE_TEXT_POS_FACTOR);

            yield return new WaitForSeconds(2f);

            TileChunk.instance.absoluteSightTileTable[ownAnimal].Remove(targetTile);
            TileChunk.instance.RefreshFieldOfView();

            // 카메라가 타겟타일로 이동한다
            Camera.main.MoveTo(animal.transform.position, true);
            animal.GetComponent<Animator>().SetBool("dig", false);

            // 땅을 파고 올라오는 모션
            animal.GetComponent<Animator>().SetTrigger("digOutTrigger");
            
            // 흙튀기는 fx
            dirtParticle.transform.position = animal.transform.position;
            dirtParticle.Play();

            yield return new WaitForSeconds(1.0f);

            for (int i = 0; i < digHoles.Length; i++)
            {
                digHoles[i].SetActive(false);
            }
        }
        else
        {
            animal.GetComponent<Animator>().SetBool("dig", false);
            
            TileChunk.instance.absoluteSightTileTable[ownAnimal].Remove(targetTile);

            // 땅을 파고 올라오는 모션
            animal.GetComponent<Animator>().SetTrigger("digOutTrigger");

            // 동물 위치를 옮겨줌
            animal.ownedTile = targetTile;
            animal.transform.position = targetTile.position_AdjustedTileYFactor;

            // 흙튀기는 fx
            dirtParticle.transform.position = animal.transform.position;
            dirtParticle.Play();

            // 땅에 구멍
            digHoles[1].transform.position = animal.transform.position + Vector3.up;
            digHoles[1].SetActive(true);

            yield return new WaitForSeconds(1.0f);

            for (int i = 0; i < digHoles.Length; i++)
            {
                digHoles[i].SetActive(false);
            }        

            TileChunk.instance.RefreshFieldOfView();
        }
    }
}
