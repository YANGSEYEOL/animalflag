﻿using UnityEngine;

class OverwatchCommand : Command
{
    public static OverwatchCommand instance;

    protected Animal _animal;
    protected Animal animal
    {
        get { return _animal; }
        set
        {
            if (_animal != null)
            {
                _animal.OnFinishedMove -= OnFinishedMove;
                _animal.OnFinishedAttack -= OnFinishedAttack;
                _animal.OnChangedCanAction -= OnChangedCanAction;
            }

            _animal = value;

            if (value != null)
            {
                value.OnFinishedMove += OnFinishedMove;
                value.OnFinishedAttack += OnFinishedAttack;
                value.OnChangedCanAction += OnChangedCanAction;
            }
        }
    }


    protected virtual void Awake()
    {
        instance = this;
    }


    private void Start()
    {
        GameManager.instance.OnSelectedAnimal += OnSelectedAnimal;
        GameManager.instance.OnChangedPlayerTurn += OnChangedPlayerTurn;

        state = CommandState.NONE;
    }


    private void OnChangedPlayerTurn(string turnUserId)
    {
        animal = null;
    }


    private void OnSelectedAnimal(Animal selectedAnimal)
    {
        animal = selectedAnimal;

        SwitchState();
    }


    public void OnChangedCanAction(bool canAction)
    {
        SwitchState();
    }


    protected virtual void SwitchState()
    {
        state = animal != null && animal.canAction ? CommandState.ACTIVE : CommandState.NONE;
    }


    public override void OnClickButton()
    {
        state = CommandState.READY;

        UIManager.instance.OpenConfirmUI(this);
    }


    private void OnFinishedMove(Animal movedAnimal)
    {
        SwitchState();
    }


    private void OnFinishedAttack(Animal animal)
    {
        SwitchState();
    }


    public override void Ready()
    {
    }


    public override void Do()
    {
        state = CommandState.NONE;

        animal.Overwatch();

        GameManager.instance.OnAnimalDidAction(animal);
    }


    public override void Cancel()
    {
        SwitchState();
    }
}


