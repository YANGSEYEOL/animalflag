﻿using UnityEngine;
using System.Collections;

public class ClimbTreeCommand : ConcealCommand
{
    public static new ClimbTreeCommand instance;

    bool canClimbTree { get { return animal != null && animal.animalData.skills.Contains("ClimbTree") && animal.canAction; } }

    public override AudioClip concealSound { get { return GameManager.instance.climbTreeSound; } }
    public override AudioClip unconcealSound { get { return GameManager.instance.unconcealSound; } }



    protected override void Awake()
    {
        instance = this;
    }


    protected override void SwitchState()
    {
        if (canClimbTree)
        {
            /// 은신 중일 때 Unconceal을 할 수 있다 
            if (animal.isClimbedTree)
            {
                state = CommandState.ACTIVATED;
            }
            /// 은신 중이 아닐 때, GRASS 타일이면 Conceal을 사용할 수 있다 
            /// 그 외에는 DISABLE
            else
            {
                state = animal.ownedTile.type == TileMaker.TileType.FOREST && animal.curRemainSkillCoolTime == 0 ? CommandState.ACTIVE : CommandState.DISABLE;
            }
        }
        else
        {
            state = CommandState.NONE;
        }
    }


    public override void Do()
    {
        state = CommandState.NONE;
        GameManager.instance.OnAnimalDidAction(animal);
        animal.StartCoroutine(animal.Conceal(!animal.isClimbedTree));
    }
}
