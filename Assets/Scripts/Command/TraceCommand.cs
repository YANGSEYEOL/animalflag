﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using I2.Loc;

// 사정거리가 엄청 길어진다. 시야에 들어오는 동물을 하나 좇아 물을 수 있다. 녹턴 궁 같이 
public class TraceCommand : Command
{
    readonly int RANGE = 20;
    readonly float SPEED = 2f;

    public static TraceCommand instance;
    [SerializeField]
    AudioClip traceSound;
    Animal _ownAnimal;
    Animal ownAnimal
    {
        get { return _ownAnimal; }
        set
        {
            if (_ownAnimal != null)
            {
                _ownAnimal.OnFinishedMove -= OnFinishedMove;
                _ownAnimal.OnChangedCanAction -= OnChangedCanAction;
            }
            _ownAnimal = value;
            if (_ownAnimal != null)
            {
                _ownAnimal.OnFinishedMove += OnFinishedMove;
                _ownAnimal.OnChangedCanAction += OnChangedCanAction;
            }
        }
    }

    Vectrosity.VectorLine _vectorline;
    Vectrosity.VectorLine vectorline
    {
        get
        {
            if (_vectorline == null)
                _vectorline = TileChunk.instance.lineCyan;
            return _vectorline;
        }
    }

    Animal _targetAnimal;
    Animal targetAnimal
    {
        get { return _targetAnimal; }
        set
        {
            _targetAnimal = value;
            if (value == null)
                UIManager.instance.ResetTargetUI();
            else
                UIManager.instance.SetTargetUi(value);
        }
    }

    public struct Target
    {
        public Animal animal;
        public Tile tile;
    };

    public int[] shortestPath;
    int targetIndex = 0;
    List<Target> validTargetAnimals;
    Dictionary<Animal, Animal> blockTarget;
    Tile targetTile;

    bool canTrace { get { return ownAnimal != null && ownAnimal.animalData.skills.Contains("Trace") && ownAnimal.canAction; } }

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        GameManager.instance.OnSelectedAnimal += OnSelectedAnimal;
        GameManager.instance.OnSetTargetAnimal += OnSetTargetAnimal;
        GameManager.instance.OnAnimalBlocked += OnAnimalBlocked;
        //TileChunk.instance.OnClickedTile += OnClickedTile;
        Init();
    }

    public void Init()
    {
        validTargetAnimals = new List<Target>();
        blockTarget = new Dictionary<Animal, Animal>();
    }

    int[] x = { 1, -1, 0, 0 };
    int[] y = { 0, 0, 1, -1 };

    // 시야에 보여야 한다.
    // 이 동물로 향하는 경로가 존재해야 한다.
    bool CheckValidTargetAndMakeTargetStruct(Animal ownAnimal, Animal targetAnimal)
    {
        if (targetAnimal.ownedTile == null)
            return false;
        if (targetAnimal.photonView.isMine || !targetAnimal.isVisible)
            return false;

        int tileX = targetAnimal.ownedTile.TileIndex_X;
        int tileY = targetAnimal.ownedTile.TileIndex_Y;
        bool isValidTarget = false;
        Target target;
        target.tile = new Tile();
        for (int i = 0; i < 4; i++)
        {
            Tile tile = TileChunk.instance.tiles[tileX + x[i], tileY + y[i]];
            if (tile.owningAnimal != null && tile.owningAnimal.isVisible)
                continue;
            ownAnimal.isTracing = true;
            int[] pathToTarget = TileChunk.instance.GetShortestPathForMovement(ownAnimal, tile._tileIndex, RANGE);
            ownAnimal.isTracing = false;

            if (pathToTarget != null)
            {
                isValidTarget = true;
                break;
            }
        }
        if (isValidTarget)
        {
            target.animal = targetAnimal;
            validTargetAnimals.Add(target);
        }
        return isValidTarget;
    }

    // 내가 선택할 수 있는 타일을 모두 구해서 Vectorline으로 보여준다.
    void GetAvailableTargets(Animal ownAnimal)
    {
        if (ownAnimal == null)
            return;

        ownAnimal.curremainMovement = RANGE;

        validTargetAnimals.Clear();

        foreach (var animal in GameManager.instance.allAnimals)
        {
            CheckValidTargetAndMakeTargetStruct(ownAnimal, animal);
        }
        foreach (var hunter in GameManager.instance.allHunters)
        {
            CheckValidTargetAndMakeTargetStruct(ownAnimal, hunter);
        }

        ownAnimal.curremainMovement = ownAnimal.animalData.mobility;
    }

    //void OnClickedTile(Tile clickedTile)
    //{
    //    if (targetAnimal == null)
    //        return;

    //    foreach (var target in targetableEnemyAnimal)
    //    {
    //        if (target.animal == targetAnimal)
    //        {
    //            if(targetAnimal.ownedTile == clickedTile)
    //            {
    //                return;
    //            }
    //            foreach (var tile in target.tiles)
    //            {
    //                if(clickedTile == tile)
    //                {
    //                    targetTile = clickedTile;

    //                    TileChunk.instance.lineGreen.Resize(0);
    //                    TileChunk.instance.AddVerticesIntoLinePointsByScale(tile, 0.9f, TileChunk.instance.lineMagenta);
    //                    TileChunk.instance.lineMagenta.Draw3D();
    //                    TileChunk.instance.lineGreen.Draw3D();
    //                    ownAnimal.curremainMovement = RANGE;
    //                    shortestPath = TileChunk.instance.GetShortestPathForMovement(ownAnimal, targetTile._tileIndex);
    //                    TileChunk.instance.DisplayNavigationPath(ownAnimal, tile);
    //                    ownAnimal.curremainMovement = ownAnimal.animalData.mobility;
    //                    return;
    //                }
    //            }
    //            return;
    //        }
    //    }
    //}

    protected virtual void SwitchState()
    {
        if (canTrace)
        {
            GetAvailableTargets(ownAnimal);
            state = ownAnimal.curRemainSkillCoolTime <= 0 && validTargetAnimals.Count > 0 ? CommandState.ACTIVE : CommandState.DISABLE;
        }
        else
        {
            state = CommandState.NONE;
        }
    }

    void OnFinishedMove(Animal animal)
    {
        SwitchState();
    }

    void OnChangedCanAction(bool canAction)
    {
        SwitchState();
    }

    // 이 동물이 선택되면 스킬버튼을 띄워야 한다.
    void OnSelectedAnimal(Animal animal)
    {
        ownAnimal = animal;

        SwitchState();
    }

    void OnSetTargetAnimal(Animal animal)
    {
        if (animal == null)
            return;

        if (state == CommandState.READY || state == CommandState.ACTIVATED)
        {
            targetAnimal = null;
            foreach (var target in validTargetAnimals)
            {
                if (target.animal == animal)
                {
                    targetAnimal = animal;
                    FindBestRouteAndTile(ownAnimal, targetAnimal);
                    UIManager.instance.OpenConfirmUI(this);
                    break;
                }
            }
        }
    }

    public override void OnClickButton()
    {
        UIManager.instance.curCommand = this;
        GetAvailableTargets(ownAnimal);

        targetIndex++;  // 타겟 리스트에서 자동으로 타겟설정
        if (validTargetAnimals.Count <= targetIndex)
        {
            targetIndex = 0;
        }

        targetAnimal = validTargetAnimals[targetIndex].animal;
        FindBestRouteAndTile(ownAnimal, targetAnimal);

        vectorline.Resize(0);
        foreach (var target in validTargetAnimals)
        {
            TileChunk.instance.AddVerticesIntoLinePointsByScale(target.animal.ownedTile, 1f, vectorline);
        }
        vectorline.Draw3D();

        if (validTargetAnimals.Count > 0)
        {
            state = CommandState.ACTIVATED;
        }
        else
        {
            state = CommandState.READY;
        }

        UIManager.instance.OpenConfirmUI(this);
    }

    // trace도중 move함수에서 상대에게 막혔을때 호출됨
    void OnAnimalBlocked(Animal animal, Animal targetAnimal)
    {
        if (animal.isTracing)
        {
            blockTarget.Add(animal, null);
            
            // 은신동물한테 막히면 타겟을 할 수 없고 그냥 맞기만 한다
            if (!targetAnimal.isVisible) blockTarget[animal] = null;
            else    blockTarget[animal] = targetAnimal;
        }
    }

    public override void Cancel()
    {
        SwitchState();

        TileChunk.instance.UndisplayNavigationPath();
        vectorline.Resize(0);
        vectorline.Draw3D();
        
        targetAnimal = null;
    }

    // 타겟으로 가는 최단경로를 구함
    public void FindBestRouteAndTile(Animal ownAnimal, Animal targetAnimal)
    {
        ownAnimal.curremainMovement = RANGE;
        Tile targetAnimalTile = targetAnimal.ownedTile;
        int tileX = targetAnimalTile.TileIndex_X;
        int tileY = targetAnimalTile.TileIndex_Y;
        shortestPath = null;

        for (int i = 0; i < 4; i++)
        {
            if (!TileChunk.instance.IsValidTileIndex(tileX + x[i], tileY + y[i])) continue;
            Tile tile = TileChunk.instance.tiles[tileX + x[i], tileY + y[i]];
            ownAnimal.isTracing = true;
            int[] pathToTarget = TileChunk.instance.GetShortestPathForMovement(ownAnimal, tile._tileIndex, ownAnimal.curremainMovement);
            ownAnimal.isTracing = false;
            if (pathToTarget == null) continue;

            if (shortestPath == null) shortestPath = pathToTarget;

            if (shortestPath[0] > pathToTarget[0])
                shortestPath = pathToTarget;
        }

        int tileindex = shortestPath[1];
        tileX = tileindex % TileChunk.instance.tileSizeHeight;
        tileY = tileindex / TileChunk.instance.tileSizeHeight;
        targetTile = TileChunk.instance.tiles[tileX, tileY];
        TileChunk.instance.DisplayNavigationPath(ownAnimal, targetTile, targetAnimal.ownedTile, shortestPath);

        ownAnimal.curremainMovement = ownAnimal.animalData.mobility;
    }

    public override void Do()
    {
        ownAnimal.canAction = false;
        vectorline.Resize(0);
        vectorline.Draw3D();
        GameManager.instance.OnAnimalDidAction(ownAnimal);
        StartCoroutine(Trace(ownAnimal, targetAnimal, shortestPath));
        
        state = CommandState.NONE;
        
        targetAnimal = null;
    }

    public override void Ready()
    {

    }

    protected override void OnStateChanged(CommandState changedState)
    {
        base.OnStateChanged(changedState);

        switch (changedState)
        {
            case CommandState.NONE:
                {
                }
                break;
            case CommandState.DISABLE:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(ownAnimal.curRemainSkillCoolTime > 0);
                    commandButton.cooltimeLabel.text = ownAnimal.curRemainSkillCoolTime.ToString();
                }
                break;
            case CommandState.READY:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
            case CommandState.ACTIVATED:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
            case CommandState.ACTIVE:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
        }
    }

    public IEnumerator Trace(Animal animal, Animal targetAnimal, int[] shortestPath)
    {
        animal.isTracing = true;
        UIManager.instance.ShowHUDText(I2.Loc.ScriptLocalization.Get(GetType().Name + "_name"), Color.yellow, animal.transform.position + Animal.DAMAGE_TEXT_POS_FACTOR);
        yield return new WaitForSeconds(0.5f);

        animal.audioSource.PlayOneShot(traceSound, 0.4f);
        animal.movingAnimSpeed = SPEED;

        yield return MoveIfCanMove(animal, shortestPath);

        // 나를 막은 동물이 존재한다면, 
        if (blockTarget.ContainsKey(animal))
        {
            // null인상황은 이 동물이 은신인 상태이다. 이 시점은 isVisible이 이미 true로 변한 상황이기 때문에 
            // onAnimalBlocked에서 은신의 유무를 isVisible로 판단하지못하고, blockTarget[animal]을 null 로 기록한것으로 확인함
            // 은신이 아닌 경우 반격을 하고 은신일경우 공격을 못한다
            if(blockTarget[animal] != null)  animal.Attack(blockTarget[animal], AttackCommand.HitResult.HIT, 5);

            // 키를 제거해줌 (키가 존재한다는 것이 animal이 어떤동물에게 막혔다는걸 표현하기 위함임, 사용끝나면 키를 제거해줌)
            blockTarget.Remove(animal);
        }
        // 나를 막은 동물 없이 원래 타겟에게 공격할때
        else
            animal.Attack(targetAnimal, AttackCommand.HitResult.HIT, 5);

        animal.movingAnimSpeed = 1;
        animal.curremainMovement = 0;

        TileChunk.instance.UndisplayNavigationPath();

        animal.isTracing = false;
        yield return new WaitForSeconds(1f);
    }

    public IEnumerator MoveIfCanMove(Animal animal, int[] path)
    {
        /// 경로에 적 동물의 대기 공격 스킬이 있는지 체크 
        bool areThereEnemiesAttackSkills;
        ExitGames.Client.Photon.Hashtable attackedQueue;
        int changedDestinationPathIndex;

        animal.CheckEnemiesAttackSkillsInPath(ref path, out areThereEnemiesAttackSkills, out attackedQueue, out changedDestinationPathIndex);
        //if(attackedQueue.ContainsValue)
        /// 체크된게 있으면 돌리고, 체크되는게 없다면 일반 Move를 호출한다 
        IEnumerator moveCoroutine = null;
        if (areThereEnemiesAttackSkills)
        {
            moveCoroutine = animal.StartAttackedQueue(attackedQueue);
        }
        else
        {
            moveCoroutine = animal.Move(path, changedDestinationPathIndex);
        }

       // animal.StartTrackedCoroutine(moveCoroutine);

        return moveCoroutine;
    }

    public void GetTargetsAndRoute(Animal animal, ref List<Target> targetAnimals)
    {
        validTargetAnimals.Clear();
        for (int i = 0; i < GameManager.instance.enemyAnimals.Count; i++)
        {
            CheckValidTargetAndMakeTargetStruct(animal, GameManager.instance.enemyAnimals[i]);
        }
        targetAnimals = validTargetAnimals;
    }
}
