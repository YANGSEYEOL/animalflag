﻿using UnityEngine;
using System.Collections;

public class DefendCommand : Command
{
    GameManager gameManager;
    TileChunk tileChunk;
    UIManager uiManager;

    Animal _animal;
    Animal animal
    {
        get { return _animal; }
        set
        {
            if (_animal != null)
            {
                _animal.OnChangedCanAction -= OnChangedCanAction;
            }

            _animal = value;

            if (value != null)
            {
                _animal.OnChangedCanAction += OnChangedCanAction;
            }
        }
    }

    public UIButton defendButton;



    private void Start()
    {
        gameManager = GameManager.instance;
        gameManager.OnSelectedAnimal += OnSelectedAnimal;
        gameManager.OnChangedPlayerTurn += OnChangedPlayerTurn;

        tileChunk = TileChunk.instance;

        uiManager = UIManager.instance;

        state = CommandState.NONE;
    }

    private void OnChangedPlayerTurn(string turnUserId)
    {
        animal = null;
    }

    private void OnSelectedAnimal(Animal selectedAnimal)
    {
        animal = selectedAnimal;

        state = animal != null && animal.canAction ? CommandState.ACTIVE : CommandState.NONE;
    }

    public void OnClickDefendButton()
    {
        uiManager.OpenConfirmUI(this);
    }

    public void OnChangedCanAction(bool canAction)
    {
        state = animal != null && animal.canAction ? CommandState.ACTIVE : CommandState.NONE;
    }

    protected override void OnStateChanged(CommandState changedState)
    {
        switch (changedState)
        {
            case CommandState.NONE:
                {
                    defendButton.gameObject.SetActive(false);
                }
                break;
            case CommandState.ACTIVE:
                {
                    defendButton.gameObject.SetActive(true);
                }
                break;
            case CommandState.READY:
                {
                }
                break;
        }
    }

    public override void Ready()
    {
    }

    public override void Do()
    {
        state = CommandState.NONE;
        GameManager.instance.OnAnimalDidAction(animal);
        //animal.Defend();
    }

    public override void Cancel()
    {
        state = animal != null && animal.canAction ? CommandState.ACTIVE : CommandState.NONE;
    }
}
