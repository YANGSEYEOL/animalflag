﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using I2.Loc;

// 사정거리가 엄청 길어진다. 시야에 들어오는 동물을 하나 좇아 물을 수 있다. 녹턴 궁 같이 
public class EscapeCommand : Command
{
    public static EscapeCommand instance;

    const int RANGE = 3;

    [SerializeField]
    AudioClip runSound;

    Animal _ownAnimal;
    Animal ownAnimal
    {
        get { return _ownAnimal; }
        set
        {
            if (_ownAnimal != null)
            {
                _ownAnimal.OnFinishedMove -= OnFinishedMove;
            }
            _ownAnimal = value;
            if (_ownAnimal != null)
            {
                _ownAnimal.OnFinishedMove += OnFinishedMove;
            }
        }
    }

    Tile _targetTile;
    Tile targetTile
    {
        get
        {
            return _targetTile;
        }
        set
        {
            if (_targetTile != null)
            {
                TileChunk.instance.lineCyan.Resize(0);
                TileChunk.instance.lineCyan.Draw3D();
            }

            _targetTile = value;

            if (_targetTile != null)
            {
                TileChunk.instance.lineCyan.Resize(0);
                TileChunk.instance.AddVerticesIntoLinePointsByScale(_targetTile, 1.0f, TileChunk.instance.lineCyan);
                TileChunk.instance.lineCyan.Draw3D();
            }
        }
    }

    public Dictionary<Animal, int[]> pathsToTargetTiles;

    bool canEscape { get { return ownAnimal != null && ownAnimal.animalData.skills.Contains("Escape") && ownAnimal.canAction; } }

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        GameManager.instance.OnSelectedAnimal += OnSelectedAnimal;
        TileChunk.instance.OnClickedTile += OnClickedTile;
        Init();
    }

    public void Init()
    {
        pathsToTargetTiles = new Dictionary<Animal, int[]>();
    }
    
    protected virtual void SwitchState()
    {
        if (canEscape)
        {
            state = ownAnimal.curRemainSkillCoolTime <= 0 ? CommandState.ACTIVE : CommandState.DISABLE;
        }
        else
        {
            state = CommandState.NONE;
        }
    }

    void OnClickedTile(Tile clickedTile)
    {
        if (state != CommandState.READY)
            return;
        
        if (TileChunk.instance.IsTileValidForMove(clickedTile))
        {
            targetTile = clickedTile;
            TileChunk.instance.DisplayNavigationPath(ownAnimal, clickedTile);
        }

        UIManager.instance.OpenConfirmUI(this);
    }

    void OnFinishedMove(Animal animal)
    {
        SwitchState();
    }

    void OnChangedCanAction(bool canAction)
    {
        SwitchState();
    }

    // 이 동물이 선택되면 스킬버튼을 띄워야 한다.
    void OnSelectedAnimal(Animal animal)
    {
        ownAnimal = animal;

        SwitchState();
    }
    
    public override void OnClickButton()
    {
        UIManager.instance.curCommand = this;

        state = CommandState.READY;

        // 도망갈수있는 타일의 범위 보여줌
        TileChunk.instance.DisplayAllPathTilesOf(ownAnimal, ownAnimal.animalData.mobility , Color.green);
    }

    public override void Cancel()
    {
        targetTile = null;
        SwitchState();
    }

    public override void Do()
    {
        ownAnimal.canAction = false;
        TileChunk.instance.lineGreen.Resize(0);
        TileChunk.instance.lineGreen.Draw3D();
        GameManager.instance.OnAnimalDidAction(ownAnimal);

        int[] path = TileChunk.instance.GetShortestPathForMovement(ownAnimal, targetTile._tileIndex, ownAnimal.curremainMovement);
        
        // 타겟타일에 넣는다.
        pathsToTargetTiles[ownAnimal] = path;

        StartCoroutine(ReadyEscape(ownAnimal, targetTile));

        state = CommandState.NONE;
    }

    public override void Ready()
    {

    }

    protected override void OnStateChanged(CommandState changedState)
    {
        base.OnStateChanged(changedState);

        switch (changedState)
        {
            case CommandState.NONE:
                {
                    targetTile = null;
                }
                break;
            case CommandState.DISABLE:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(ownAnimal.curRemainSkillCoolTime > 0);
                    commandButton.cooltimeLabel.text = ownAnimal.curRemainSkillCoolTime.ToString();
                }
                break;
            case CommandState.READY:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
            case CommandState.ACTIVATED:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
            case CommandState.ACTIVE:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
        }
    }

    public IEnumerator ReadyEscape(Animal animal, Tile targetTile)
    {
        UIManager.instance.ShowHUDText(I2.Loc.ScriptLocalization.Get(GetType().Name + "_hud"), Color.yellow, animal.transform.position + Animal.DAMAGE_TEXT_POS_FACTOR);

        yield return new WaitForSeconds(1f);
    }

    // 상대방에게 공격을 회피하고 도망가는 동작
    public IEnumerator Escape(Animal animal)
    {
        UIManager.instance.ShowHUDText(I2.Loc.ScriptLocalization.Get(GetType().Name + "_name"), Color.yellow, animal.transform.position + Animal.DAMAGE_TEXT_POS_FACTOR);

        //yield return new WaitForSeconds(1f);
        animal.audioSource.PlayOneShot(runSound, 0.5f);
        

        // 미리 정해놓은 경로로 도망가기
        int curMovement = animal.curremainMovement;
        
        MoveIfCanMove(animal, pathsToTargetTiles[animal]);
        pathsToTargetTiles.Remove(animal);
        yield break;
    }

    public IEnumerator MoveIfCanMove(Animal animal, int[] path)
    {
        /// 경로에 적 동물의 대기 공격 스킬이 있는지 체크 
        bool areThereEnemiesAttackSkills;
        ExitGames.Client.Photon.Hashtable attackedQueue;
        int changedDestinationPathIndex;

        animal.CheckEnemiesAttackSkillsInPath(ref path, out areThereEnemiesAttackSkills, out attackedQueue, out changedDestinationPathIndex);

        /// 체크된게 있으면 돌리고, 체크되는게 없다면 일반 Move를 호출한다 
        IEnumerator moveCoroutine = null;
        if ( areThereEnemiesAttackSkills )
        {
            moveCoroutine = animal.StartAttackedQueue(attackedQueue);
        }
        else
        {
            moveCoroutine = animal.Move(path, changedDestinationPathIndex);
        }

        animal.StartTrackedCoroutine(moveCoroutine);

        return moveCoroutine;
    }

}
