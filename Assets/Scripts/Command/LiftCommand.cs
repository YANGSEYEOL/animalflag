﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
// 장전해놓으면 다음턴에 빈 타일로 돌진한다. 가는 경로에 있는 모든 동물들은 1턴 스턴에걸린다.
public class LiftCommand : Command {
    readonly int range = 1;
    public enum stage : int { NONE = 0, LIFTUP };
    readonly int DAMAGE = 5;
    stage curStage;

    [SerializeField]
    AudioClip stompSound;

    Animal ownAnimal;

    string description;
    string skillName;

    Animal targetAnimal;
    Tile tileToPutDown;
    Tile[] tilesLiftable;

    [SerializeField]
    UIButton skillButton;
    [SerializeField]
    UISprite skillSprite;

    void Start()
    {
        GameManager.instance.OnSelectedAnimal += OnSelectedAnimal;
        GameManager.instance.OnSetTargetAnimal += OnSetTargetAnimal;
        TileChunk.instance.OnClickedTile += OnClickedTile;
        Init();
    }

    public void Init()
    {
        InitActionName();
        InitSkillDescription();
        tilesLiftable = new Tile[range*4];
    }

    void InitActionName()
    {
        skillName = "Lift";
    }

    void InitSkillDescription()
    {
        description = "This is Thrust";
    }

    // 내가 선택할 수 있는 타일을 모두 구해서 Vectorline으로 보여준다.
    public void GetRangeTiles()
    {
        if (ownAnimal == null)
            return;

        int tileX = ownAnimal.ownedTile.TileIndex_X;
        int tileY = ownAnimal.ownedTile.TileIndex_Y;
        List<int> x = new List<int>();
        List<int> y = new List<int>();
        int a = range;
        int b = 0;
        for (int i = 0; i < range * 4; i++)
        {            
            x.Add(a); x.Add(b);
            y.Add(b); y.Add(a);
            a--;
            if (a == 0)
                a--;
        }

        TileChunk.instance.lineGreen.Resize(0);
        for (int i = 0; i < range * 4; i++)
        {
            if (TileChunk.instance.IsValidTileIndex(tileX + x[i], tileY + y[i]))
            {
                tilesLiftable[i] = TileChunk.instance.tiles[tileX + x[i], tileY + y[i]];
                TileChunk.instance.AddVerticesIntoLinePointsByScale(tilesLiftable[i], 0.9f, TileChunk.instance.lineGreen);
            }
        }
        TileChunk.instance.lineGreen.Draw3D();
    }

    // 이 동물이 선택되면 스킬버튼을 띄워야 한다.
    void OnSelectedAnimal(Animal animal)
    {
        if (animal == null)
            return;
    
        if (animal.animalData.skills.Contains("Lift") && animal.photonView.isMine)
        {
            ownAnimal = animal;
            skillButton.isEnabled = ownAnimal.canAction;
            skillSprite.color = skillButton.isEnabled ? Color.white : Color.grey;
            skillButton.gameObject.SetActive(true);

        }
        else
        {
            ownAnimal = null;
            skillButton.gameObject.SetActive(false);
        }
    }

    void OnSetTargetAnimal(Animal animal)
    {
        if (animal == null)
            return;
        // NONE : 스킬버튼을 누르지 않은상황, ACTIVE : 시전동물을 정한 상황(스킬버튼을 누른 상황), READY : 타겟동물을 정한 상황
        if (state == CommandState.ACTIVE)
        {
            targetAnimal = animal;
            curStage = stage.LIFTUP;
            TileChunk.instance.lineMagenta.Resize(0);
            TileChunk.instance.lineMagenta.Draw3D();
            TileChunk.instance.AddVerticesIntoLinePointsByScale(targetAnimal.ownedTile, 0.9f, TileChunk.instance.lineMagenta);
            TileChunk.instance.lineMagenta.Draw3D();

            state = CommandState.READY;
        }
    }

    void OnClickedTile(Tile clickedTile)
    {
        if (state == CommandState.NONE)
            return;

        if(curStage == stage.LIFTUP)               // 현재 들어올릴 동물을 선택한 상태인가?
        {
            if (CheckProperTileToPutDown(clickedTile))   // 현재 선택한 타일에 동물을 내려놓을 수 있는가?
            {
                TileChunk.instance.AddVerticesIntoLinePointsByScale(clickedTile, 0.9f, TileChunk.instance.lineMagenta);
                TileChunk.instance.lineMagenta.Draw3D();

                tileToPutDown = clickedTile;
                UIManager.instance.OpenConfirmUI(this);
            }
        }
    }

    public void OnClickSkillButton()
    {
        TileChunk.instance.lineMoveableRange.Resize(0);
        TileChunk.instance.lineMoveableRange.Draw3D();
        TileChunk.instance.UndisplayNavigationPath();
        GetRangeTiles();
        state = CommandState.ACTIVE;
    }

    void OnChangedCurCommand(Command newCurCommand)
    {
        if (newCurCommand == this)
        {
            GetRangeTiles();
            state = CommandState.ACTIVE;
        }
        else
        {
            state = CommandState.NONE;
        }
    }

    bool CheckProperTileToLiftUp(Tile clickedTile)
    {
        for (int i = 0; i < range * 4; i++)
        {
            if (tilesLiftable[i] == null) continue;
            if (tilesLiftable[i]._tileIndex == clickedTile._tileIndex &&
                clickedTile.owningAnimal != null)
                return true;
        }
        return false;
    }

    bool CheckProperTileToPutDown(Tile clickedTile)
    {
        for (int i = 0; i < range*4; i++)
        {
            if (tilesLiftable[i] == null) continue;
            if (tilesLiftable[i]._tileIndex == clickedTile._tileIndex &&
                clickedTile.owningAnimal == null)
                return true;
        }
        return false;
    }

    public override void Cancel()
    {
        skillButton.gameObject.SetActive(true);
        skillButton.isEnabled = ownAnimal.canAction;
        skillSprite.color = skillButton.isEnabled ? Color.white : Color.grey;
        state = CommandState.NONE;
    }

    IEnumerator LiftUpAndPutDown()
    {
        AnimationClip wriggleAnimationClip = null;
        foreach (var animationClip in targetAnimal.animator.runtimeAnimatorController.animationClips)
        {
            if (animationClip.name == "wriggle")
            {
                wriggleAnimationClip = animationClip;

                break;
            }
        }

        /// 회전 
        var lookVector = targetAnimal.transform.position - ownAnimal.ownedTile.position;
        var lookRotation = Quaternion.LookRotation(lookVector);
        ownAnimal.transform.rotation = lookRotation;

        // 상대방을 들어서 내려놓는 로직 실행
        targetAnimal.animator.SetTrigger("wriggleTrigger");
        ownAnimal.animator.SetTrigger("LiftUpTrigger");

        /// 카운터 연출
        yield return new WaitForSeconds(wriggleAnimationClip.length);
        ownAnimal.animator.SetTrigger("GetDownTrigger");

        /// 회전 
        lookVector = tileToPutDown.position - ownAnimal.ownedTile.position;
        lookRotation = Quaternion.LookRotation(lookVector);
        ownAnimal.transform.rotation = lookRotation;

        var moveToTileCoroutine = targetAnimal.MoveTo(targetAnimal.GetRealPositionByTile(tileToPutDown), "drop");
        while (moveToTileCoroutine.MoveNext()) yield return moveToTileCoroutine.Current;
        AudioClip atkSound = GameManager.instance.attackSound_critical[Random.Range(0, GameManager.instance.attackSound_critical.Length)];


        targetAnimal.curHealth -= DAMAGE;
        StartCoroutine(targetAnimal.Damaged(DAMAGE, AttackCommand.HitResult.CRITICAL));
        ownAnimal.audioSource.PlayOneShot(atkSound, 0.4f);
        AttackEffect.instance.Play(ownAnimal.transform.position, AttackCommand.HitResult.CRITICAL);
        UIManager.instance.ShowHUDText("GRAZE", Color.white, targetAnimal.transform.position + Animal.DAMAGE_TEXT_POS_FACTOR);
        UIManager.instance.ShowHUDText(DAMAGE.ToString(), Color.grey, targetAnimal.transform.position + Animal.DAMAGE_TEXT_POS_FACTOR);
        targetAnimal.ownedTile = tileToPutDown;
        state = CommandState.NONE;
    }
    // state를 다 처음으로 돌려놓고 
    public override void Do()
    {
        ownAnimal.canAction = false;
        curStage = stage.NONE;
        GameManager.instance.OnAnimalDidAction(ownAnimal);
        StartCoroutine(LiftUpAndPutDown());
    }

    public override void Ready()
    {

    }

    protected override void OnStateChanged(CommandState changedState)
    {
        switch (changedState)
        {
            case CommandState.NONE:
                {
                    TileChunk.instance.lineGreen.Resize(0);
                    TileChunk.instance.lineGreen.Draw();

                    TileChunk.instance.lineMagenta.Resize(0);
                    TileChunk.instance.lineMagenta.Draw();
                }
                break;
            case CommandState.ACTIVE:
                {
                }
                break;
            case CommandState.READY:
                {

                }
                break;
        }
    }
}
