﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour
{
    public static CameraShake instance;  
    float duration = 1f;
    float magnitude = 1f;   
    float elapsed = 0f;


    void Awake()
    {        
        instance = this;	
    }


    public void Shake(float duration, float magnitude)   
    {
        this.duration = duration;       
        this.magnitude = magnitude;     
        elapsed = 0f;       
        StartCoroutine(ShakeCoroutine());   
    }


    bool isShaking = false;
    IEnumerator ShakeCoroutine()
    {
        /// 새롭게 셰이크가 들어오면 elapsed 초기화하고 다시 돌려야 한다     

        if (isShaking) yield break;

        isShaking = true;

        float shakeDirection = -1f;
        while (elapsed < duration)
        {
            float x = shakeDirection * magnitude;
            float z = shakeDirection * magnitude;
            shakeDirection *= -1f;

            Vector3 originPos = transform.position;
            transform.position = new Vector3(x + originPos.x, originPos.y, z + originPos.z);
            elapsed += Time.deltaTime;

            yield return null;
        }

        isShaking = false;
    }
}
