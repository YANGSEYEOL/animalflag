﻿using Photon;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

using ExitGames.Client.Photon;

public class ChickenFarmConnect : PunBehaviour
{
    public UIInput InputField;

    public string UserId;

	string previousRoomPlayerPrefKey = "Chicken:PreviousRoom";
    string previousRoom;

	const string NickNamePlayerPrefsKey = "NickName";

    [SerializeField]
    bool autoJoinRoom = false;

	void Start()
	{
		InputField.value = PlayerPrefs.HasKey(NickNamePlayerPrefsKey) ? PlayerPrefs.GetString(NickNamePlayerPrefsKey) : "";

        // 로그 레벨 풀로 채움 
        PhotonNetwork.logLevel = PhotonLogLevel.Full;

        // 플레이어 생성 동물들. 플레이어가 나가도 삭제되지 않도록 함 
        PhotonNetwork.autoCleanUpPlayerObjects = false;

#if UNITY_EDITOR
        // 매번 Connect 버튼 누르는거 귀찮아 죽겠네
        StartCoroutine(AutoConnect());
#endif
    }

    IEnumerator AutoConnect()
    {
        /// 모든 GameObejct들의 Start()가 호출된 이후에 실행되는 것을 보장한다 
        /// 기다리지 않고 호출하면 Start()가 호출되지 않은 GameObject들에 NullReferenceException 연타를 맛보게된다
        yield return new WaitForEndOfFrame();
        InputField.value = UserId;
        ApplyUserIdAndConnect();
    }

	// Connect UI 버튼을 누르면 호출됨
    public void ApplyUserIdAndConnect()
    {
        // 닉네임이 중복되어도 상관없으려나 
        // InputField에 글자가 없으면 DemoNick으로 기본 설정된다
		string nickName = "DemoNick";
        if (this.InputField != null && !string.IsNullOrEmpty(this.InputField.value))
        {
            nickName = this.InputField.value;
			PlayerPrefs.SetString(NickNamePlayerPrefsKey,nickName);
        }
        else
        {
            Debug.LogError("닉네임 제대로 설정하라거");
        }

        if (PhotonNetwork.AuthValues == null)
        {
            PhotonNetwork.AuthValues = new AuthenticationValues();
        }

		/// To be able to ReJoin any room, you need to use UserIDs!
        /// 방에 재입장하려면 UserId를 사용한다는군
        /// 닉네임을 UserId와 같게 처리하기 때문에 중복될 수 있다
        /// UserId 중복이 일어나면 중복나는 사람들을 하나로 인식한다 
        /// 내가 만약 이걸 세팅 안해주면 알아서 값을 배정해주나?
		PhotonNetwork.AuthValues.UserId = nickName;

		Debug.Log("Nickname: " + nickName + " userID: " + this.UserId,this);

        PhotonNetwork.playerName = nickName;


        string connectVersion;
        
        // OS X UnityEditor에서는 Application.version이 UnityEditor.version이 반환됨
        // 에디터 플레이모드에서도 포톤 서버 접속 버전이 게임 버전이 되도록 함
    #if UNITY_STANDALONE_OSX
        connectVersion = "1.0";
        //connectVersion = UnityEditor.PlayerSettings.bundleVersion;  
    #else
        connectVersion = Application.version;
    #endif
        PhotonNetwork.ConnectUsingSettings(connectVersion);
        Debug.Log("Connect Version : " + connectVersion);
        

        // this way we can force timeouts by pausing the client (in editor)
        PhotonHandler.StopFallbackSendAckThread();
    }

    public void OnClickReConnectAndRejoin()
    {
        // Reconnect는 이해하겠다만, Rejoin까지 가능하게 한다구?
        // 내부 코드를 보니까 타임아웃은 났지만, 앱이 살아있을 때만 가능한 처리이다
        // 앱 메모리상에 남아있는 RoomParamsCache값을 이용해서 rejoin하는 원리 
        PhotonNetwork.ReconnectAndRejoin();
        PhotonHandler.StopFallbackSendAckThread();  // this is used in the demo to timeout in background!
    }


    public override void OnConnectedToMaster()
    {
        // after connect 

        // 서버 접속을 했는데 이전 룸 기록이 있다면, 룸 이름 가져오기
        if (PlayerPrefs.HasKey(previousRoomPlayerPrefKey))
		{
			Debug.Log("getting previous room from prefs: ");
			this.previousRoom = PlayerPrefs.GetString(previousRoomPlayerPrefKey);
			PlayerPrefs.DeleteKey(previousRoomPlayerPrefKey); // we don't keep this, it was only for initial recovery
		}

        // after timeout: re-join "old" room (if one is known)
        if (!string.IsNullOrEmpty(this.previousRoom) && !PhotonNetwork.offlineMode)
        {
            Debug.Log("ReJoining previous room: " + this.previousRoom);
            PhotonNetwork.ReJoinRoom(this.previousRoom);
            // we only will try to re-join once. if this fails, we will get into a random/new room
            this.previousRoom = null;       
        }
        else
        {
            if (autoJoinRoom)
            {
#if UNITY_EDITOR
                CreateRoom();
                //PhotonNetwork.JoinRandomRoom();
#else
                // else: join a random room
                //PhotonNetwork.JoinRandomRoom();
                CreateRoom();
#endif
            }
            else
            {
                BGMPlayer.instance.Play(BGMPlayer.BGM.LOBBY);
            }
        }
    }

    private void CreateRoom()
    {
        // important! MAX Player 2, TTL 20 sec 
        PhotonNetwork.CreateRoom(null, new RoomOptions() { MaxPlayers = 2, PlayerTtl = 0000, PublishUserId = true }, null);
    }

    public override void OnJoinedLobby()
    {
        OnConnectedToMaster(); // this way, it does not matter if we join a lobby or not
    }

    public override void OnPhotonRandomJoinFailed(object[] codeAndMsg)
    {
		Debug.Log("OnPhotonRandomJoinFailed");

        CreateRoom();
    }

    public override void OnJoinedRoom()
    {
		Debug.Log("Joined room: " + PhotonNetwork.room.Name);

		// save room name for that use when next connection 
        this.previousRoom = PhotonNetwork.room.Name;
		PlayerPrefs.SetString(previousRoomPlayerPrefKey,this.previousRoom);

    }

    public override void OnPhotonJoinRoomFailed(object[] codeAndMsg)
	{
		Debug.Log ("OnPhotonJoinRoomFailed");
		this.previousRoom = null;
		PlayerPrefs.DeleteKey (previousRoomPlayerPrefKey);

        // rejoin에 실패를 하면 자동으로 랜덤방에 접속을 하더라 
        // 딱히 그걸 호출하는 코드가 이 함수안에는 전개되지 않아서 의아했다 
        // 알고보니 이렇게 방 접속 실패를 하면, 서버 연결을 강제로 끊고 다시 재접속을 하더라 
        // (GameEnteredOnGameServer -> DisconnectToReconnect)
        // 다시 재연결이 되면서 이벤트 함수인 OnConnectedToMaster가 재호출된다 
    }

    public override void OnConnectionFail(DisconnectCause cause)
    {
        Debug.Log("Disconnected due to: " + cause + ". this.previousRoom: " + this.previousRoom);
    }
	
	public override void OnPhotonPlayerActivityChanged(PhotonPlayer otherPlayer)
	{
		Debug.Log("OnPhotonPlayerActivityChanged() for "+otherPlayer.NickName+" IsInactive: "+otherPlayer.IsInactive);
	}

}
