﻿using System;
using System.Collections;
using Photon;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

using ExitGames.Client.Photon;

// 포톤서버는 각플레이어에게 플레이어아이디를 부여한다. 플레이어 아이디는 1부터 시작한다.
// 이 게임에서는 actual number를 신경쓰지않는다.
// 이 게임에서는 플레이어0 과 플레이어 1을 쓴다., 그래서 어떻게든 플레이어는 각 플레이어 넘버를 파악해야한다.

public class RpsCore : PunBehaviour, IPunTurnManagerCallbacks
{

	[SerializeField]
	private RectTransform ConnectUiView;

	[SerializeField]
	private RectTransform GameUiView;

	[SerializeField]
	private CanvasGroup ButtonCanvasGroup;

	[SerializeField]
	private RectTransform TimerFillImage;

    [SerializeField]
    private Text TurnText;

    [SerializeField]
    private Text TimeText;

    [SerializeField]
    private Text RemotePlayerText;

    [SerializeField]
    private Text LocalPlayerText;
    
    [SerializeField]
    private Image WinOrLossImage;


    [SerializeField]
    private Image localSelectionImage;
    public Hand localSelection;

    [SerializeField]
    private Image remoteSelectionImage;
    public Hand remoteSelection;


    [SerializeField]
    private Sprite SelectedRock;

    [SerializeField]
    private Sprite SelectedPaper;

    [SerializeField]
    private Sprite SelectedScissors;

    [SerializeField]
    private Sprite SpriteWin;

    [SerializeField]
    private Sprite SpriteLose;

    [SerializeField]
    private Sprite SpriteDraw;


    [SerializeField]
    private RectTransform DisconnectedPanel;

    private ResultType result;

    private PunTurnManager turnManager;

    public Hand randomHand;    // used to show remote player's "hand" while local player didn't select anything

	// keep track of when we show the results to handle game logic.
	private bool IsShowingResults;
	
    public enum Hand
    {
        None = 0,
        Rock,
        Paper,
        Scissors
    }

    public enum ResultType
    {
        None = 0,
        Draw,
        LocalWin,
        LocalLoss
    }

    public void Start()
    {
		this.turnManager = this.gameObject.AddComponent<PunTurnManager>();
		// TurnManagerListener를 등록해서 TurnManaget의 이벤트를 받는다
        this.turnManager.TurnManagerListener = this;
        this.turnManager.TurnDuration = 5f;
       
		// 내가 선택한 가위바위보 이미지와 상대방이 선택한 가위바위보 이미지
		// 둘다 deactive
        this.localSelectionImage.gameObject.SetActive(false);
        this.remoteSelectionImage.gameObject.SetActive(false);
        this.StartCoroutine("CycleRemoteHandCoroutine");

		RefreshUIViews();
    }

    public void Update()
    {
		// 업데이트에 더럽게 코드가 많구만... 맘에 안듬 
		
		// Check if we are out of context, which means we likely got back to the demo hub.
		if (this.DisconnectedPanel ==null)
		{
			Destroy(this.gameObject);
		}

        // for debugging, it's useful to have a few actions tied to keys:
        if (Input.GetKeyUp(KeyCode.L))
        {
            PhotonNetwork.LeaveRoom();
        }
        if (Input.GetKeyUp(KeyCode.C))
        {
            PhotonNetwork.ConnectUsingSettings(null);
            PhotonHandler.StopFallbackSendAckThread();
        }

        // 굉장히 위험한 코드가 여기 있네 
        // 아예 중간에서 return을 때려버리네
        if ( ! PhotonNetwork.inRoom)
        {
			return;
		}

		// disable the "reconnect panel" if PUN is connected or connecting
        // GetActive()를 체크할 필요는 없음. false 상태일 때 false를 호출한다고 재처리가 되진 않음. 무시됨
        // 이런 코드는 이벤트 함수에서 바꿔야지 Update에 있으면 쓰나... 그럼 이벤트 함수를 왜 쓰나 
		if (PhotonNetwork.connected && this.DisconnectedPanel.gameObject.GetActive())
		{
			this.DisconnectedPanel.gameObject.SetActive(false);
		}
		if (!PhotonNetwork.connected && !PhotonNetwork.connecting && !this.DisconnectedPanel.gameObject.GetActive())
		{
			this.DisconnectedPanel.gameObject.SetActive(true);
		}


		if (PhotonNetwork.room.PlayerCount>1)
		{
            // 또 한번 더러운 코드 발견 
            // 이건 위에 있는 inRoom 체크보다 더 더럽다 
			if (this.turnManager.IsTimeOver)
			{
				return;
			}

			/*
			// check if we ran out of time, in which case we loose
			if (turnEnd<0f && !IsShowingResults)
			{
					Debug.Log("Calling OnTurnCompleted with turnEnd ="+turnEnd);
					OnTurnCompleted(-1);
					return;
			}
		    */


            // 턴 텍스트
            if (this.TurnText != null)
            {
                this.TurnText.text = this.turnManager.Turn.ToString();
            }

            // 타임 텍스트
			if (this.turnManager.Turn > 0 && this.TimeText != null && ! IsShowingResults)
            {
				this.TimeText.text = this.turnManager.RemainingSecondsInTurn.ToString("F1") + " SECONDS";

                // 타임 게이지바 표현 
				TimerFillImage.anchorMax = new Vector2(1f- this.turnManager.RemainingSecondsInTurn/this.turnManager.TurnDuration,1f);
            }

            
		}

		this.UpdatePlayerTexts();

        // show local player's selected hand
        Sprite selected = SelectionToSprite(this.localSelection);
        if (selected != null)
        {
            this.localSelectionImage.gameObject.SetActive(true);
            this.localSelectionImage.sprite = selected;
        }

        // remote player's selection is only shown, when the turn is complete (finished by both)
        // 둘다 선택이 끝났을 때, remote의 것을 보여준다 
        if (this.turnManager.IsCompletedByAll)
        {
            // 플레이어 둘다 선택을 끝마쳤다 해도 이 코드는 타임업되면 동작하지 않는다. 왜냐?
            // 위에 isOver체크 때문에 return이 되기 때문이다
            // 둘다 시간내에 선택을 했다면 문제없이 동작이 되겠지만, 상대방은 선택을 했고, 나는 타임업까지 가만히 둔 상태라면 이곳을 처리하지 못함
            selected = SelectionToSprite(this.remoteSelection);

            if (selected != null)
            {
                this.remoteSelectionImage.color = new Color(1,1,1,1);
                this.remoteSelectionImage.sprite = selected;
            }
        }
        else
        {
            // isGameStart 같은 변수로 체크하는게 아니라 플레이어 숫자로 버튼 선택 여부를 결정하네
            // 직관적이지 못하다 
			ButtonCanvasGroup.interactable = PhotonNetwork.room.PlayerCount > 1;

            // 상대방이 안 들어왔을 때를 Count < 2로 표현하네 
            // 가독성 안 좋다 
            if (PhotonNetwork.room.PlayerCount < 2)
            {
                this.remoteSelectionImage.color = new Color(1, 1, 1, 0);
            }

            // if the turn is not completed by all, we use a random image for the remote hand
            // 이 코드가 얼마나 가독성이 안좋은지를 또 알려준다
            // Turn > 0 이라면 게임이 시작됬다는걸 의미한다
            // 만약 턴이 시작됐을 때 따로 코루틴을 돌려서 체크 했다면 애초에 Turn > 0를 할 필요가 없어진다
            // Update안에 있으니까 넣을 수 밖에 없어지는거지. 확실히 하기 위해서 
            // IsCompletedByAll 체크는 방금 위에서 체크해서 false일 때만 여길 들어오는데 왜 또 체크하는거야 
            else if (this.turnManager.Turn > 0 && !this.turnManager.IsCompletedByAll)
            {
                // alpha of the remote hand is used as indicator if the remote player "is active" and "made a turn"
                PhotonPlayer remote = PhotonNetwork.player.GetNext();

                // 변수명도 마음에 안든다 직관적이지 못함 
                // 기본 알파값
                float alpha = 0.5f;
                if (this.turnManager.GetPlayerFinishedTurn(remote))
                {
                    // 선택을 끝마쳤을 때 알파값
                    alpha = 1;
                }
                if (remote != null && remote.IsInactive)
                {
                    // 타임아웃 됐을 때 알파값
                    alpha = 0.1f;
                }

                this.remoteSelectionImage.color = new Color(1, 1, 1, alpha);
                this.remoteSelectionImage.sprite = SelectionToSprite(randomHand);
            }
        }

    }

    #region TurnManager Callbacks

    /// <summary>Called when a turn begins (Master Client set a new Turn number).</summary>
    public void OnTurnBegins(int turn)
    {
        Debug.Log("OnTurnBegins() turn: "+ turn);
        this.localSelection = Hand.None;
        this.remoteSelection = Hand.None;

        this.WinOrLossImage.gameObject.SetActive(false);

        this.localSelectionImage.gameObject.SetActive(false);
        this.remoteSelectionImage.gameObject.SetActive(true);

		IsShowingResults = false;
		ButtonCanvasGroup.interactable = true;
    }

    // 인터페이스 재구현
    // 
    public void OnTurnCompleted(int obj)
    {
        Debug.Log("OnTurnCompleted: " + obj);

        this.CalculateWinAndLoss();
        this.UpdateScores();
        this.OnEndTurn();
    }

    // 인터페이스 재구현
    // when a player moved (but did not finish the turn)
    public void OnPlayerMove(PhotonPlayer photonPlayer, int turn, object move)
    {
        Debug.Log("OnPlayerMove: " + photonPlayer + " turn: " + turn + " action: " + move);
        throw new NotImplementedException();
    }

    // when a player made the last/final move in a turn
    public void OnPlayerFinished(PhotonPlayer photonPlayer, int turn, object move)
    {
        Debug.Log("OnTurnFinished: " + photonPlayer + " turn: " + turn + " action: " + move);

        if (photonPlayer.IsLocal)
        {
            this.localSelection = (Hand)(byte)move;
        }
        else
        {
            this.remoteSelection = (Hand)(byte)move;
        }
    }

    public void OnTurnTimeEnds(int obj)
    {
        // 타임업처리는 TurnManager가 그냥 알려주기만 하는군
        // 턴을 끝낼지 말지에 대한 처리는 우리가 직접 하는군
		if (!IsShowingResults)
		{
			Debug.Log("OnTurnTimeEnds: Calling OnTurnCompleted");
			OnTurnCompleted(-1);
		}
	}

    private void UpdateScores()
    {
        // 로컬 플레이어 스코어만 갱신해서 올리네
        // 커스텀 프로퍼티다 보니 플레이어 중 한명만 스코어를 올려주면 나머지 애들한테도 푸시되서 갱신됨
        if (this.result == ResultType.LocalWin)
        {
            PhotonNetwork.player.AddScore(1);   // this is an extension method for PhotonPlayer. you can see it's implementation
        }
    }

    #endregion

    #region Core Gameplay Methods

    
    /// <summary>Call to start the turn (only the Master Client will send this).</summary>
    public void StartTurn()
    {
        // 마스터 클라이언트가 시작 사인을 한다 
        if (PhotonNetwork.isMasterClient)
        {
            this.turnManager.BeginTurn();
        }
    }
	
    // 가위바위보 선택을 MakeTurn이라고 부르네
    // 함수 이름 노직관적 
    public void MakeTurn(Hand selection)
    {
        this.turnManager.SendMove((byte)selection, true);
    }
	
    // On 이라는 이벤트 함수가 너무 많으니까 정신이 없네
    // 이벤트 함수는 외부에서 호출해주는 걸 의미하는건데
    // 여기는 같은 클래스 내부에서 함수 호출하는 것도 On을 붙이니 헷갈림 
    // 이벤트 함수를 override한 것도 아닌데 말이지
    public void OnEndTurn()
    {
        this.StartCoroutine("ShowResultsBeginNextTurnCoroutine");
    }

    // 코루틴을 쓰긴 쓰네...
    // Update()에 있는거 좀 빼서 코루틴으로 만들어놓지
    public IEnumerator ShowResultsBeginNextTurnCoroutine()
    {
		ButtonCanvasGroup.interactable = false;
		IsShowingResults = true;
       // yield return new WaitForSeconds(1.5f);

        // 가위바위보 결과에 따라 Win,Lose,Draw 이미지 표시 해주고 새로운 턴 시작 
        // 이 함수는 CalculateWinAndLoss() 결과에 의존한다
        // 그러므로, 이 함수 첫 시작에 CalculateWinAndLoss()를 넣는게 맞다 
        if (this.result == ResultType.Draw)
        {
            this.WinOrLossImage.sprite = this.SpriteDraw;
        }
        else
        {
            this.WinOrLossImage.sprite = this.result == ResultType.LocalWin ? this.SpriteWin : SpriteLose;
        }
        this.WinOrLossImage.gameObject.SetActive(true);

        yield return new WaitForSeconds(2.0f);

        this.StartTurn();
    }
    
    
    public void EndGame()
    {
		Debug.Log("EndGame");
    }

    private void CalculateWinAndLoss()
    {
        this.result = ResultType.Draw;
        if (this.localSelection == this.remoteSelection)
        {
            return;
        }

		if (this.localSelection == Hand.None)
		{
			this.result = ResultType.LocalLoss;
			return;
		}

		if (this.remoteSelection == Hand.None)
		{
			this.result = ResultType.LocalWin;
		}
        
        if (this.localSelection == Hand.Rock)
        {
            this.result = (this.remoteSelection == Hand.Scissors) ? ResultType.LocalWin : ResultType.LocalLoss;
        }
        if (this.localSelection == Hand.Paper)
        {
            this.result = (this.remoteSelection == Hand.Rock) ? ResultType.LocalWin : ResultType.LocalLoss;
        }

        if (this.localSelection == Hand.Scissors)
        {
            this.result = (this.remoteSelection == Hand.Paper) ? ResultType.LocalWin : ResultType.LocalLoss;
        }
    }

    private Sprite SelectionToSprite(Hand hand)
    {
        switch (hand)
        {
            case Hand.None:
                break;
            case Hand.Rock:
                return this.SelectedRock;
            case Hand.Paper:
                return this.SelectedPaper;
            case Hand.Scissors:
                return this.SelectedScissors;
        }

        return null;
    }

    private void UpdatePlayerTexts()
    {
        // 플레이어들의 이름, 스코어 띄우는 함수 

        // GetNext()로 다음 플레이어를 가져올 수 있네
        PhotonPlayer remote = PhotonNetwork.player.GetNext();
        PhotonPlayer local = PhotonNetwork.player;

        if (remote != null)
        {
            // should be this format: "name        00"
            this.RemotePlayerText.text = remote.NickName + "        " + remote.GetScore().ToString("D2");
        }
        else
        {
            // 상대방이 없다면 상대방 기다리고 있다고 띄우기 
			TimerFillImage.anchorMax = new Vector2(0f,1f);
			this.TimeText.text = "";
            this.RemotePlayerText.text = "waiting for another player        00";
        }
        
        if (local != null)
        {
            // should be this format: "YOU   00"
            this.LocalPlayerText.text = "YOU   " + local.GetScore().ToString("D2");
        }
    }

    public IEnumerator CycleRemoteHandCoroutine()
    {
        while (true)
        {
            // cycle through available images
            this.randomHand = (Hand)Random.Range(1, 4);
            yield return new WaitForSeconds(0.5f);
        }
    }

    #endregion

    // 손동작을 하나 냈을때 호출하는 함수들 MakeTurn(가위, 바위, 보)이런식으로 시작함.
    #region Handling Of Buttons

    public void OnClickRock()
    {
        this.MakeTurn(Hand.Rock);
    }

    public void OnClickPaper()
    {
       this.MakeTurn(Hand.Paper);
    }

    public void OnClickScissors()
    {
        this.MakeTurn(Hand.Scissors);
    }

    public void OnClickConnect()
    {
        PhotonNetwork.ConnectUsingSettings(null);
        PhotonHandler.StopFallbackSendAckThread();  // this is used in the demo to timeout in background!
    }
    
    public void OnClickReConnectAndRejoin()
    {
        // Reconnect는 이해하겠다만, Rejoin까지 가능하게 한다구?
        // 내부 코드를 보니까 타임아웃은 났지만, 앱이 살아있을 때만 가능한 처리이다
        // 앱 메모리상에 남아있는 RoomParamsCache값을 이용해서 rejoin하는 원리 
        PhotonNetwork.ReconnectAndRejoin();
        PhotonHandler.StopFallbackSendAckThread();  // this is used in the demo to timeout in background!
    }

    #endregion


	void RefreshUIViews()
	{
		// RedFillImage의 AnchorMax를 왼쪽으로 땡겨서 타이머 게이지바 UI를 왼쪽으로 돌려놓는다
		TimerFillImage.anchorMax = new Vector2(0f,1f);

		// 방에 접속해있는지 아닌지에 따라 접속 UI, 게임UI 스위칭
		// 위에 코드에 비해 양반
		ConnectUiView.gameObject.SetActive(!PhotonNetwork.inRoom);
		GameUiView.gameObject.SetActive(PhotonNetwork.inRoom);

		// 방에 들어와있고, 상대방이 있어야만 가위바위보 선택 가능
		ButtonCanvasGroup.interactable = PhotonNetwork.room!=null?PhotonNetwork.room.PlayerCount > 1:false;
	}
	

    public override void OnLeftRoom()
    {
        Debug.Log("OnLeftRoom()");



		RefreshUIViews();
    }

    public override void OnJoinedRoom()
    {
		RefreshUIViews();

        if (PhotonNetwork.room.PlayerCount == 2)
        {
            if (this.turnManager.Turn == 0)
            {
                // when the room has two players, start the first turn (later on, joining players won't trigger a turn)
                // 턴이 시작되지 않은 상태일 때만 StartTurn()을 호출하네 
                // Update()에서 UI에 대한 실시간 처리는 하고 있기 때문에 중간에 플레이어가 나가면 waiting player가 뜰꺼다
                // 하지만 게임을 종료하거나 퍼징하는 처리를 하고 있지 않기 때문에 타이머는 계속 돌고 턴은 계속 진행된다 
                // 나간 유저가 재입장을 하거나, 새로운 유저가 들어온다면 진행되고 있는 턴을 이어간다 
                this.StartTurn();
            }
        }
        else
        {
            Debug.Log("Waiting for another player");
        }
    }

    // 플레이어 접속시
    public override void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
    {
		Debug.Log("Other player arrived");

        if (PhotonNetwork.room.PlayerCount == 2)
        {
            if (this.turnManager.Turn == 0)
            {
                // when the room has two players, start the first turn (later on, joining players won't trigger a turn)
                this.StartTurn();
            }
        }
    }


    public override void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
    {
		Debug.Log("Other player disconnected! "+otherPlayer.ToStringFull());
    }


    public override void OnConnectionFail(DisconnectCause cause)
    {
        this.DisconnectedPanel.gameObject.SetActive(true);
    }

}
