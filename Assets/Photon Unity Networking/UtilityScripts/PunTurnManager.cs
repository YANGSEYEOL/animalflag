﻿// ----------------------------------------------------------------------------
// <copyright file="PunTurnManager.cs" company="Exit Games GmbH">
//   PhotonNetwork Framework for Unity - Copyright (C) 2016 Exit Games GmbH
// </copyright>
// <summary>
//  Manager for Turn Based games, using PUN
// </summary>
// <author>developer@exitgames.com</author>
// ----------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.Collections;
using ExitGames.Client.Photon;
using Photon;
using UnityEngine;
using Hashtable = ExitGames.Client.Photon.Hashtable;

/// <summary>
/// Pun turnBased Game manager.
/// Provides an Interface (IPunTurnManagerCallbacks) for the typical turn flow and logic, between players
/// Provides Extensions for PhotonPlayer, Room and RoomInfo to feature dedicated api for TurnBased Needs
/// </summary>
public class PunTurnManager : PunBehaviour
{
	/// <summary>
	/// Wraps accessing the "turn" custom properties of a room.
	/// </summary>
	/// <value>The turn index</value>
    public int Turn
    {
        get { return PhotonNetwork.room.GetTurn(); }
        private set { PhotonNetwork.room.SetTurn(value, true); }
    }

	public bool IsPaused
	{
		get { return PhotonNetwork.room.IsPaused(); }
	}


	/// <summary>
	/// The duration of the turn in seconds.
	/// </summary>
    public float TurnDuration = 20f;

	

	// 사실 Property가 나오게 된 이유는, 내부에만 쓸 수 있는 변수들을 캡슐화하면서도 쉽게 접근해서 사용할 수 있도록 하기 위함아니었던가 
	// 근데 마치 Property를 함수처럼 사용해서 이런 코딩 스타일을 어떻게 받아들여야할지 모르겠다


	/// <summary>
	/// Gets the elapsed time in the current turn in seconds
	/// </summary>
	/// <value>The elapsed time in the turn.</value>
	public float ElapsedTimeInTurn
	{
		get { return ((float)(PhotonNetwork.ServerTimestamp - PhotonNetwork.room.GetTurnStart()))/1000.0f; }
	}


	/// <summary>
	/// Gets the remaining seconds for the current turn. Ranges from 0 to TurnDuration
	/// </summary>
	/// <value>The remaining seconds fo the current turn</value>
	public float RemainingSecondsInTurn
	{
		get { return Mathf.Max(0f,this.TurnDuration - this.ElapsedTimeInTurn); }
	}


	/// <summary>
	/// Gets a value indicating whether the turn is completed by all. 
	/// </summary>
	/// <value><c>true</c> if this turn is completed by all; otherwise, <c>false</c>.</value>
    public bool IsCompletedByAll
    {   
        get 
		{ 
			if(PhotonNetwork.room != null && Turn > 0)
			{
				/// 한명의 플레이어라도 턴 차례가 안왔다면 return false;
				foreach(var player in PhotonNetwork.playerList)
				{
					if(player.GetFinishedTurn() != Turn)
					{
						return false;
					}
				}
				return true;
			}
			else
			{
				return false;
			}
		}
    }

	/// <summary>
	/// Gets a value indicating whether the current turn is finished by me.
	/// </summary>
	/// <value><c>true</c> if the current turn is finished by me; otherwise, <c>false</c>.</value>
    public bool IsFinishedByMe
    {
		get { return PhotonNetwork.player.GetFinishedTurn() == Turn; }
    }

	/// <summary>
	/// Gets a value indicating whether the current turn is over. That is the ElapsedTimeinTurn is greater or equal to the TurnDuration
	/// </summary>
	/// <value><c>true</c> if the current turn is over; otherwise, <c>false</c>.</value>
    public bool IsTimeOver
    {
		get { return this.RemainingSecondsInTurn <= 0f; }
    }

	/// <summary>
	/// The turn manager listener. Set this to your own script instance to catch Callbacks
	/// </summary>
    public IPunTurnManagerCallbacks TurnManagerListener;


	/// <summary>
	/// The turn manager event offset event message byte. Used internaly for defining data in Room Custom Properties
	/// </summary>
    public const byte TurnManagerEventOffset = 0;
	/// <summary>
	/// The Move event message byte. Used internaly for saving data in Room Custom Properties
	/// </summary>
    public const byte EvMove = 1 + TurnManagerEventOffset;
	/// <summary>
	/// The Final Move event message byte. Used internaly for saving data in Room Custom Properties
	/// </summary>
    public const byte EvFinalMove = 2 + TurnManagerEventOffset;

	public const byte EvForceFinishPlayer = 3 + TurnManagerEventOffset;


	#region MonoBehaviour CallBack
	/// <summary>
	/// Register for Event Call from PhotonNetwork.
	/// </summary>
    void Start()
    {
		// "=" is so dangerous. correct to use "+="
        PhotonNetwork.OnEventCall += OnEvent;
    }


	bool isTickingTurnTimer = false;
	IEnumerator TurnTimer()
	{
		if(isTickingTurnTimer)
		{
			yield break;
		}
		isTickingTurnTimer = true;

		while(true)
		{
			if(IsPaused)
			{
				break;
			}
			if(Turn > 0 && IsTimeOver)
			{
				/// OnTurnTimeEnds 보다 먼저 선행되어야 함 
				/// 새로운 타이머를 호출하려할 때 이 값이 true로 되어있으면 바로 취소되기 때문 
				isTickingTurnTimer = false;

				TurnManagerListener.OnTurnTimeEnds(Turn);
				break;
			}
			yield return null;
		}
		isTickingTurnTimer = false;
	}


	#endregion


	/// <summary>
	/// Tells the TurnManager to begins a new turn.
	/// </summary>
    public void BeginTurn()
    {
        Turn = this.Turn + 1; // note: this will set a property in the room, which is available to the other players.
    }

    /// <summary>
	/// Call to send an action. Optionally finish the turn, too.
	/// The move object can be anything. Try to optimize though and only send the strict minimum set of information to define the turn move.
	/// </summary>
    /// <param name="move"></param>
    /// <param name="finished"></param>
    public void SendMove(object move, bool finished)
    {
		// 이미 내가 끝나 있는 상태인데 SendMove가 들어오면 그냥 return을 보내는 군
		// 자신이 끝나있는 상태면 SendMove가 들어오면 안되는건가보군
		// 이 함수는 내가 Move를 했을 때 호출하는 함수군
        if (IsFinishedByMe)
        {
            UnityEngine.Debug.LogWarning("Can't SendMove. Turn is finished by this player.");
            return;
        }

        // along with the actual move, we have to send which turn this move belongs to
        Hashtable moveHt = new Hashtable();
        moveHt.Add("turn", Turn);
        moveHt.Add("move", move);


		// 한 턴에 한번만 움직일 수 있는 경우도 있을테지만, 여러번 움직일 수 있는 경우도 있겠지
		// 그렇기 때문에 finished 매개변수가 들어가있는 것일테고
		// 일단 SendMove가 일어날 때마다 RaiseEvent는 항상 보낸다, 진짜 finished될 때만 턴을 끝낸다  
		// 턴의 마지막 움직임일 경우,  EvFinalMove를 보낸다는 것에 유의할 것
        byte evCode = (finished) ? EvFinalMove : EvMove;
        PhotonNetwork.RaiseEvent(evCode, moveHt, true, new RaiseEventOptions() { /*CachingOption = EventCaching.AddToRoomCache*/ });
        if (finished)
        {
            PhotonNetwork.player.SetFinishedTurn(Turn);
        }

        // the server won't send the event back to the origin (by default). to get the event, call it locally
        // (note: the order of events might be mixed up as we do this locally)
		// RPC는 타겟 설정에 따라 자신에게도 즉각 로컬 호출이 되도록 만들 수 있는데 
		// RaiseEvent는 자신에게는 호출이 되지 않나보구나
        OnEvent(evCode, moveHt, PhotonNetwork.player.ID);
    }


	/// 이 함수의 목적은 다른 플레이어들에게 이 플레이어가 턴이 종료되었다는 것을 알리는 것
	/// 딱 이것 뿐임. 대단하게 무언가를 하는건 없음 
	public void ForceFinishPlayer(PhotonPlayer finishPlayer)
	{
		finishPlayer.SetFinishedTurn(Turn);
	}

	/// <summary>
	/// Gets if the player finished the current turn.
	/// </summary>
	/// <returns><c>true</c>, if player finished the current turn, <c>false</c> otherwise.</returns>
	/// <param name="player">The Player to check for</param>
	/// 해당하는 플레이어의 턴이 끝나있는 상태인지를 확인하는 함수 
    public bool GetPlayerFinishedTurn(PhotonPlayer player)
    {
		return player.GetFinishedTurn() == Turn;
    }

	public void Pause()
	{
		PhotonNetwork.room.Pause((int)(ElapsedTimeInTurn * 1000f));
	}

	public void Resume()
	{
		PhotonNetwork.room.Resume();
	}

	#region Callbacks

	/// <summary>
	/// Called by PhotonNetwork.OnEventCall registration
	/// </summary>
	/// <param name="eventCode">Event code.</param>
	/// <param name="content">Content.</param>
	/// <param name="senderId">Sender identifier.</param>
    public void OnEvent(byte eventCode, object content, int senderId)
    {
        PhotonPlayer sender = PhotonPlayer.Find(senderId);
        switch (eventCode)
        {
            case EvMove:
            {
                Hashtable evTable = content as Hashtable;
                int turn = (int)evTable["turn"];
                object move = evTable["move"];
                this.TurnManagerListener.OnPlayerMove(sender, turn, move);

                break;
            }
            case EvFinalMove:
            {
                Hashtable evTable = content as Hashtable;
                int turn = (int)evTable["turn"];
                object move = evTable["move"];

                if (turn == this.Turn)
                {
                    this.TurnManagerListener.OnPlayerFinished(sender, turn, move);
                }

                if (IsCompletedByAll)
                {
                    this.TurnManagerListener.OnTurnCompleted(this.Turn);
                }
                break;
            }
			case EvForceFinishPlayer:
			{
				Hashtable evTable = content as Hashtable;
                int turn = (int)evTable["turn"];
                int finishPlayerId = (int)evTable["forceFinishPlayerID"];

				Debug.Log("Force Finished Player Id : " + finishPlayerId);

				PhotonPlayer forceFinishPlayer = PhotonPlayer.Find(finishPlayerId);
				if (turn == this.Turn)
                {
                    this.TurnManagerListener.OnPlayerFinished(forceFinishPlayer, turn, null);
                }

                if (IsCompletedByAll)
                {
                    this.TurnManagerListener.OnTurnCompleted(this.Turn);
                }

				break;
			}	
        }
    }

	/// <summary>
	/// Called by PhotonNetwork
	/// </summary>
	/// <param name="propertiesThatChanged">Properties that changed.</param>
    public override void OnPhotonCustomRoomPropertiesChanged(Hashtable propertiesThatChanged)
    {
     	//   Debug.Log("OnPhotonCustomRoomPropertiesChanged: "+propertiesThatChanged.ToStringFull());

		// 직접 Turn을 변경하는 코드는 BeginTurn() 외에는 없다 
		// 즉, 아래 코드는 BeginTurn()이 호출되어야만 처리된다
        if (propertiesThatChanged.ContainsKey(TurnExtensions.TurnPropKey))
        {
            TurnManagerListener.OnTurnBegins(Turn);
        }
		/// 플레이어 턴 교체될 때
		else if(propertiesThatChanged.ContainsKey(TurnExtensions.WhoIsTurnPropKey))
		{
			/// 턴 타이머가 돌아가고 있다면 새로 호출하는게 아니고 그냥 둬야 함 
			StartCoroutine(TurnTimer());
		}
		/// Pause or Resume 될 때
		else if(propertiesThatChanged.ContainsKey(TurnExtensions.IsPausedKey))
		{
			/// Resume될 때 
			if((bool)propertiesThatChanged[TurnExtensions.IsPausedKey] == false)
			{
				StartCoroutine(TurnTimer());
			}
		}
		else if(propertiesThatChanged.ContainsKey(TurnExtensions.FinishedTurnPropKey))
		{
			int finishedPlayerId = (int)propertiesThatChanged[TurnExtensions.FinishedTurnPropKey];
			PhotonPlayer forceFinishedPlayer = PhotonPlayer.Find(finishedPlayerId);
			if (forceFinishedPlayer.GetFinishedTurn() == Turn)
			{
				TurnManagerListener.OnPlayerFinished(forceFinishedPlayer, Turn, null);
			}

			if (IsCompletedByAll)
			{
				TurnManagerListener.OnTurnCompleted(Turn);
			}
		}
    }

	#endregion
}


public interface IPunTurnManagerCallbacks
{
	/// <summary>
	/// Called the turn begins event.
	/// </summary>
	/// <param name="turn">Turn Index</param>
    void OnTurnBegins(int turn);

	/// <summary>
	/// Called when a turn is completed (finished by all players)
	/// </summary>
	/// <param name="turn">Turn Index</param>
    void OnTurnCompleted(int turn);

	/// <summary>
	/// Called when a player moved (but did not finish the turn)
	/// </summary>
	/// <param name="player">Player reference</param>
	/// <param name="turn">Turn Index</param>
	/// <param name="move">Move Object data</param>
    void OnPlayerMove(PhotonPlayer player, int turn, object move);

	/// <summary>
	/// When a player finishes a turn (includes the action/move of that player)
	/// </summary>
	/// <param name="player">Player reference</param>
	/// <param name="turn">Turn index</param>
	/// <param name="move">Move Object data</param>
    void OnPlayerFinished(PhotonPlayer player, int turn, object move);


	/// <summary>
	/// Called when a turn completes due to a time constraint (timeout for a turn)
	/// </summary>
	/// <param name="turn">Turn index</param>
    void OnTurnTimeEnds(int turn);
}

// 포톤 라이브러리에 있는 클래스들을 확장하는 익스텐션 메소드 클래스 
// 아래에 정의돼있는 함수들은 전부 CustomProperties만 수정하는 함수들이다 
public static class TurnExtensions
{
	/// <summary>
	/// currently ongoing turn number
	/// </summary>
    public static readonly string TurnPropKey = "Turn";

	/// <summary>
	/// start (server) time for currently ongoing turn (used to calculate end)
	/// </summary>
    public static readonly string TurnStartPropKey = "TStart";

	/// <summary>
	/// Finished Turn of Actor (followed by number)
	/// </summary>
    public static readonly string FinishedTurnPropKey = "FToA";

	public static readonly string IsPausedKey = "IsPaused";

	/// 게임이 Pause 될 때 남은 턴 시간을 저장해둔 후, 게임이 Resume이 되어야 할 때 남은 턴 시간을 다시 맞춰주기 위함
	/// 게임이 Pause 된다고 해도 타임스탬프는 계속 흘러가기 때문
	public static readonly string CapturedElapsedTimeByPausingKey = "CapturedElapsedTimeByPausing";

	public static readonly string WhoIsTurnPropKey = "WhoIsTurn";
	

	/// <summary>
	/// Sets the turn.
	/// </summary>
	/// <param name="room">Room reference</param>
	/// <param name="turn">Turn index</param>
	/// <param name="setStartTime">If set to <c>true</c> set start time.</param>
    public static void SetTurn(this Room room, int turn, bool setStartTime = false)
    {
        if (room == null || room.CustomProperties == null)
        {
            return;
        }

        Hashtable turnProps = new Hashtable();
        turnProps[TurnPropKey] = turn;
        if (setStartTime)
        {
			// 시작 시간도 같이 설정해놓는다
            turnProps[TurnStartPropKey] = PhotonNetwork.ServerTimestamp;
        }

        room.SetCustomProperties(turnProps);
    }

	/// <summary>
	/// Gets the current turn from a RoomInfo
	/// </summary>
	/// <returns>The turn index </returns>
	/// <param name="room">RoomInfo reference</param>
    public static int GetTurn(this RoomInfo room)
    {
        if (room == null || room.CustomProperties == null || !room.CustomProperties.ContainsKey(TurnPropKey))
        {
            return 0;
        }

        return (int)room.CustomProperties[TurnPropKey];
    }

	public static void SetTurnStart(this Room room, int time)
    {
        if (room == null || room.CustomProperties == null)
        {
            return;
        }

        Hashtable turnProps = new Hashtable();
		turnProps[TurnStartPropKey] = time;

        room.SetCustomProperties(turnProps);
    }


	/// <summary>
	/// Returns the start time when the turn began. This can be used to calculate how long it's going on.
	/// </summary>
	/// <returns>The turn start.</returns>
	/// <param name="room">Room.</param>
    public static int GetTurnStart(this RoomInfo room)
    {
        if (room == null || room.CustomProperties == null || !room.CustomProperties.ContainsKey(TurnStartPropKey))
        {
            return 0;
        }

        return (int)room.CustomProperties[TurnStartPropKey];
    }

	/// <summary>
	/// gets the player's finished turn (from the ROOM properties)
	/// </summary>
	/// <returns>The finished turn index</returns>
	/// <param name="player">Player reference</param>
    public static int GetFinishedTurn(this PhotonPlayer player)
    {
		string propKey = FinishedTurnPropKey + player.UserId;

        Room room = PhotonNetwork.room;
        if (room == null || room.CustomProperties == null || !room.CustomProperties.ContainsKey(propKey))
        {
            return 0;
        }
        
        return (int)room.CustomProperties[propKey];
    }

	/// <summary>
	/// Sets the player's finished turn (in the ROOM properties)
	/// </summary>
	/// <param name="player">Player Reference</param>
	/// <param name="turn">Turn Index</param>
    public static void SetFinishedTurn(this PhotonPlayer player, int turn)
    {
        Room room = PhotonNetwork.room;
        if (room == null || room.CustomProperties == null)
        {
            return;
        }

        string propKey = FinishedTurnPropKey + player.UserId;
        Hashtable finishedTurnProp = new Hashtable();
        finishedTurnProp[propKey] = turn;

		/// OnCustomProperties에서 누구 턴이 종료되었는지 빠르게 알아내기 위함
		finishedTurnProp[FinishedTurnPropKey] = player.ID;

        room.SetCustomProperties(finishedTurnProp);
    }


	public static void Pause(this Room room, int turnElapsedTime)
    {
        if (room == null || room.CustomProperties == null)
        {
            return;
        }

		Hashtable pauseProp = new Hashtable();
        pauseProp[IsPausedKey] = true;
		pauseProp[CapturedElapsedTimeByPausingKey] = turnElapsedTime;

		room.SetCustomProperties(pauseProp);
    }

	public static void Resume(this Room room)
    {
        if (room == null || room.CustomProperties == null || !room.CustomProperties.ContainsKey(CapturedElapsedTimeByPausingKey))
        {
            return;
        }

		Hashtable resumeProp = new Hashtable();
        resumeProp[IsPausedKey] = false;

		int expectedTurnStartTime = PhotonNetwork.ServerTimestamp - (int)room.CustomProperties[CapturedElapsedTimeByPausingKey];
		resumeProp[TurnStartPropKey] = expectedTurnStartTime;

		room.SetCustomProperties(resumeProp);
    }

	public static bool IsPaused(this RoomInfo room)
	{
		if (room == null || room.CustomProperties == null || !room.CustomProperties.ContainsKey(IsPausedKey))
        {
            return false;
        }

		return (bool)room.CustomProperties[IsPausedKey];
	}

	public static void SetWhoIsTurn(this Room room, PhotonPlayer player)
    {
        if (room == null || room.CustomProperties == null)
        {
            return;
        }

        Hashtable whoIsTurnProps = new Hashtable();
		whoIsTurnProps[WhoIsTurnPropKey] = player.UserId;

		// 턴이 다른 플레이어에게 넘어갈 때마다 타이머 리셋 
		whoIsTurnProps[TurnStartPropKey] = PhotonNetwork.ServerTimestamp;

        room.SetCustomProperties(whoIsTurnProps);
    }

	/// return : 플레이어 UserId
    public static string GetWhoIsTurn(this RoomInfo room)
    {
		if (room == null || room.CustomProperties == null || !room.CustomProperties.ContainsKey(WhoIsTurnPropKey))
		{
			return "None";
		}

		return (string)room.CustomProperties[WhoIsTurnPropKey];
    }
}